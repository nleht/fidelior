#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 23 14:47:15 2022

@author: nle003
"""

import numpy as np
import matplotlib as mpl
from matplotlib import pyplot as plt
import fidelior as fdo
from fidelior import end, half

Nx = 5
Ny = 4

x_ = np.arange(Nx+1)
y_ = np.arange(Ny+1)

box = fdo.Box((Nx, Ny))

x, y = box.ndgrid(x_, y_)[0:end, 0:end]
#x1, y1 = box.ndgrid(x_, y_)[half:end-half, -half:end-1-half]

mpl.rcParams["xtick.major.size"] = 0
mpl.rcParams["ytick.major.size"] = 0
plt.rc('axes', linewidth=0)

plt.figure(1, figsize=(Nx, Ny))
plt.clf()
plt.plot(x._arr, y._arr, 'ko-')
plt.plot(x._arr.T, y._arr.T, 'k-')
d = .07
plt.xlim(-d, Nx+d)
plt.ylim(-d, Ny+d)
plt.gca().set_aspect('equal')

xticks = list(np.arange(Nx+1))
xlabels = [str(y) for y in np.arange(Nx+1)]
xlabels[-1] += '/end'
plt.xticks(ticks=np.arange(Nx+1), labels=xlabels)
yticks = list(np.arange(Ny+1))
ylabels = [str(y) for y in np.arange(Ny+1)]
ylabels[-1] += '/end'
# add a few more
yticks.insert(1,1/2)
ylabels.insert(1,'half')
yticks.insert(-2, Ny-1/2)
ylabels.insert(-2, 'end-half')
plt.yticks(ticks=yticks, labels=ylabels)

plt.title('box = Box((5, 4))')
plt.text(Nx+d, Ny+d, 'vertex\n(node)', ha='left', va='center', color='b')
plt.text(Nx-0.5, Ny-0.5, 'cell', ha='center', va='center', color='b')
plt.text(Nx-0.5, Ny, 'edge\n(facet)', ha='center', va='center', color='b')
plt.text(Nx, Ny-0.5, 'edge\n(facet)', va='center', ha='center', rotation=90, color='b')

plt.plot([0, 0], [1/2, Ny-1/2], 'ro')
plt.xlabel('1st index')
plt.ylabel('2nd index', labelpad=-30)

plt.rcdefaults()
