Basic features of FIDELIOR
=======

The best environment to use with _FIDELIOR_ is [Spyder](https://www.spyder-ide.org/) or [IPython](https://ipython.org/).

Contents
- [Discretized functions](#discretized-functions)
- [Finite-difference operators](#finite-difference-operators)
- [Unknown discretized functions](#unknown-discretized-functions)
- [Constraints (boundary conditions)](#constraints-boundary-conditions)
- [Discretized equations and their solution](#discretized-equations-and-their-solution)
- [A few words about implementation (advanced)](#a-few-words-about-implementation-advanced)
- [Where to go next?](#where-to-go-next)

## Discretized functions

Finite-difference (FD) schemes usually operate on N-dimensional arrays which represent discretized values of continuous functions on a rectangular grid. (Unfortunately, irregular meshes are usually too complicated to be handled by the FD method -- in this case, try FEM or finite-volume methods. However, see [Example 6](/examples#6-mesh-refinement) for a combination of a coarse and a fine rectangular mesh.) The grid is represented by a `Box` object which describes a rectangular region (in an arbitrary number of dimensions) with the given number of grid cells along each axis. For example, in 2D, to create a grid of 10 cells along _x_-axis and 15 cells along _y_-axis, we do:
```python
In [1]: import fidelior as fdo
In [2]: box = fdo.Box((10,15))
```
Values of functions on such a grid are represented as 2D arrays. We want to allow the discretized functions to have extra values outside the `box`, in the so-called **ghost cells**. The NumPy `ndarray` is not very convenient for storing the values of disretized functions, because indexing always starts with zero, and one would have to keep track of the offset if there are ghost cells before the beginning of the grid. We also want to keep track of ghost cells after the end of the grid. In _FIDELIOR_, this is done by using arrays with customized indexing, which are created in the following way:
```python
In [3]: from fidelior import end, half
In [4]: u = box.zeros()[-1:end+1, -2:end+3]     # allocated array
In [5]: f = box[-half:end+half, half:end-half]  # unallocated array
```
Discretized function `u` is defined on the **nodes** of the grid, which is represented by integer indices. It has 1 ghost node below zero and 1 ghost node above the end on _x_ axis, and 2 ghost nodes below zero and 3 ghost nodes above the end on _y_ axis. The extent of `u` which has the information on the number of ghost nodes along each axis (**spans**) is stored as a tuple of slices in `u.spans`.

Discretized function `f` is defined on the **cells** of the grid, which is represented by half-integer indices. It has 1 ghost cell below zero and 1 ghost cell above the end on _x_ axis, and no ghost cells along _y_ axis.

Objects `u`, `f` belong to class `ExtArray`, and we will call them **extended arrays**. Extended array `u` is allocated and initialized to zeros, which is done by the method `.zeros(dtype='double')`. Its values may be accessed in a way similar to usual Python rules, e.g., `u[-1:3,:]`, `u[0, end]`, etc. Note, however, one **very important difference**: indices are **inclusive** (unlike Python but like in MATLAB), so that, e.g., index `-1:3` means `[-1,0,1,2,3]`. The "keyword" `end` represents the node right after the last cell, i.e, the upper edge of the grid. The node at the lower edge of the grid has index `0`. Because of the ghost nodes, we can (unlike MATLAB) access nodes above `end` by using `end+d`, where `d` is an offset, and below `0`, by using negative indices.

The indices are illustrated in the Figure below. For details on the terminology of space discretization (_vertex_, _node_, _cell_, etc.), see [The FEniCS Book (2012)](https://fenicsproject.org/book/), page 186.

| Indices in a _Box_ and basic terms |
| --- |
| ![FIDELIOR Box](figures/box.png) |


Array `f` (as given above) is unallocated and has to be **assigned** first like this:
```python
In [6]: f
Out[6]: ExtArray<>[-½:10+½, ½:15-½]        # does not contain anything, indicated by "<>"
In [7]: import numpy
In [8]: f.assign(numpy.random.rand(*f.nt)) # assign random values; use copy=True keyword to copy the input
Out[8]: ExtArray<float64>[-½:10+½, ½:15-½] # contains elements of type "float64"
```
As one might guess, `f.nt` is the total shape of the numerical array. After allocation of `f`, we can access its elements in a similar manner, e.g., `f[-half:1+half, end-1-half]`. The values at half-integer indices, like _f_<sub>k+1/2,j+1/2</sub>, are accessed by `f[k+half, j+half]`. Again, indices are **inclusive**. Index `half` represents the first cell, `end-half` represents the last cell, and we can access ghost cells with negative indices (e.g., `-1-half`, `-2+half`) and indices above `end` (e.g., `end+1+half`).

Basic numerical operations (overloaded NumPy functions) may be applied to the extended arrays, e.g. `fdo.exp(f)` etc. The extended arrays can also be added, subtracted, multiplied etc. The spans for binary operations do not have to coincide, but "on-node" arrays may be added only to "on-node" arrays etc, e.g., if the span along x-axis is half-integer in one of the operands, it must be half-integer also in the other operand. If the spans are different, the spans of the result are clipped to the smallest spans of the operands. E.g., if the operands had spans `[0:end+1]` and `[-1:end]`, the span of the result will be `[0:end]`. To assign value with smaller spans, use `update` method. For example, to update `f` with a smaller on-cell extended array `v`, we can write `f.update(v)`.

New numeric or empty extended arrays, with the same `box` and spans, may be created in the following ways:
```python
u_copy = u.copy()      # copies the data, too
b = u.as_zeros('bool') # array of 'False'; same as box.zeros('bool')[u.spans]
e = u.as_empty()       # same as box[u.spans]
```

When taking a subarray, the result is `numpy.ndarray`:
```python
In [9]: u[0:end, 0:end]
Out[9]:
array([[0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.],
       ...,
       [0., 0., 0., ..., 0., 0., 0.],
       [0., 0., 0., ..., 0., 0., 0.]])
```
A step >1 can be specified, i.e., `u[0:end:2, 0:end:3]`. The current version of _FIDELIOR_ does not allow negative steps.

To get another `ExtArray`, one may use the following syntax:
```python
In [9]: u.restrict[0:end, 0:end]
Out[9]: ExtArray<float64>[0:10+0, 0:15+0]
```
However, this is very rarely needed and probably can be avoided altogether.

## Finite-difference operators

The syntax for the shift operator along one or more axes, e.g., taking _u_<sub>k+1,j+1/2</sub>, is `u.at((1, half))`. Shifting by a half-integer number is possible. The boundaries shift, too, and the resulting array will have different spans:
```python
In [9]: u.at((1, half))
Out[9]: ExtArray<float64>[-2:10+0, -2½:15+2½] # the spans are [-2:end+0, -2-half:end+2+half]
```
Shifting only in a single axis has a shortcut notation, e.g., a shifted 3D array `v.at((0,0,half))` may be written as `v.shift(half, axis=2)`. For 1D arrays, the `axis` argument may be skipped.

Other operators may be represented as a linear combination of such shifts. The most used operators are:
* The central difference operator: `z.shift(half, axis) - z.shift(-half, axis)`. It is predefined in _FIDELIOR_ as `fdo.diff(z, axis)`.
* The averaging operator: `(z.shift(half, axis) + z.shift(-half, axis))/2`. It is predefined in _FIDELIOR_ as `fdo.aver(z, axis)`.

As we mentioned above, in binary operations on extended arrays the span of the result is clipped to the intersection of the spans of the operands. Because of this, the results of `fdo.diff` and `fdo.aver` will be shorter by 1 in the corresponding axis.

To approximate a derivative, the difference must be divided by Δx. In the case of non-uniform grids, Δx may be obtained from node coordinates. There is a _FIDELIOR_ way to specify node coordinates as extended arrays:
```python
x_, y_ = ... # specify node coordinates as 1D NumPy arrays
x, y = box.ndgrid(x_, y_)[0:end, 0:end] # create extended arrays for node coordinates
# Variable Δx and Δy, as extended arrays
dx = fdo.diff(x, 0)
dy = fdo.diff(y, 1)
# Gradient of u
dudx = fdo.diff(u, 0)/dx
dudy = fdo.diff(u, 1)/dy
```

## Unknown discretized functions

Unknown discretized functions are represented by linear operators. We will call them **symbolic extended arrays** or just **symbolic arrays** (if there is no confusion with arrays described in [this section](#a-few-words-about-implementation-advanced)). To make a distinction, the previously described arrays may be called **numeric (extended) arrays**. The symbolic arrays are set up in the following manner:
```python
In [9]: u_sym = box.sym('u')[u.spans] # has same size (spans) as u
# or simply
In [9]: u_sym = u.as_sym('u')
```
The unknowns are given names, in this case, `'u'`.

We can apply differential operators to the symbolic arrays:
```python
In [10]: dudy_sym = fdo.diff(u_sym, axis=1) # apply finite difference
```
Since `u_sym` is an operator, `dudy_sym` is also an operator and may be applied to numeric extended arrays:
```python
In [11]: dudy_sym
Out[11]: ExtArray<Operator(u)>[-1:10+1, -1½:15+2½]
In [12]: dudy = dudy_sym.apply(u)
# Or just
In [12]: dudy = dudy_sym(u)
# The result
In [13]: dudy
Out[13]: ExtArray<float64>[-1:10+1, -1½:15+2½] # contains fdo.diff(u, axis=1)
```

The ways to create numeric arrays, described above in section [_Discretized functions_](#discretized-functions), also work if the input is symbolic, e.g., `new_array = u_sym.as_zeros('int')`. There is another, more advanced, way to create extended arrays:
```python
v = box.allocate_as(u)[half:end-half, half:end-half]
```
This will "allocate" space of the given size tied to the specified `box`. This `box` may be different from `u.box`, i.e., the `Box` associated with `u`. If `u` is symbolic, this "space" may be assigned to operators which are derived from `u`. If the input `u` is numeric, this is just empty space of the same `dtype` as `u`. This functionality is useful, e.g., in subroutines which may operate either on numeric or symbolic arrays. There is an example of the use of `.allocate_as` method in [Example 4](/examples/#4-advection-in-1d-using-dg-method):
```python
du = some_box.allocate_as(u)[some_spans]
tmp = some_linear_operations_on_u # an ExtArray
# Assign part of 'du'
du[:, some_index] = tmp[...]
# NOTE: tmp is ExtArray while du[:, some_index] is ndarray,
# so we must take tmp[...] before assigning
```
Every element of `du` _must be assigned_ to something before it may be used further. This holds for both numeric and symbolic arrays.

One can check whether an extended array is numeric or symbolic by checking boolean fields `u.is_num` and `u.is_sym`.

## Constraints (boundary conditions)

We can set arbitrary linear constraints on the symbolic arrays:
```python
In [14]: (u_sym[0, end] == 1.5).constraint('corner')
```
Then, when we solve an equation for `u_sym`, the solution will take into account that the result must satisfy `u[0, end]==1.5`. Constraints must have names (here, `'corner'`). This will make it easy to replace them -- just provide a different constraint with the same name.

A more complicated example:
```python
In [14]: ((dudy_sym[:, half] == 3.5) & (u_sym[:, end] == 7.0)).constraint('Neumann bottom and Dirichlet top')
```
This sets the derivative, represented as a finite difference (see section [_Unknown discretized functions_](#unknown-discretized-functions)), to be equal to 3.5 at the bottom boundary of the simulation domain (where _y_ index is `half` -- remember that the difference is defined on _cells_, i.e, half-way between nodes for node arrays), and the function itself to be equal to 7.0 at the top boundary. This example also demonstrates that the results of `==` operation (which belong to class `Equality`) may be combined by using `&` operator.

## Discretized equations and their solution

The discretized equations, similarly to constraints, are also represented as equalities. E.g.,
```python
In [15]: equation = Equation(div(E_sym) == rho)
In [16]: phi = equation.solve()
# Or simply
In [15]: phi = (div(E_sym) == rho).solve()
```
Here, `E_sym = grad(-phi_sym)` is the negative gradient of `phi_sym`, and `rho` and `u` are numeric extended arrays. The differential operators `grad` and `div` may be defined (e.g., for Δx=1, Δy=1) as
```python
def grad(u):
    return (fdo.diff(u, 0), fdo.diff(u, 1))
def div(A):
    return sum(fdo.diff(Ai, axis=i) for i, Ai in enumerate(A))
```

There is no qualitative difference between equations and constraints, i.e., equations to be solved may be represented as constraints. If there are no independent variables left after setting all the constraints, an **empty equation** is solved:
```python
In [15]: (div(E_sym) == rho).constraint('Gauss law')
In [16]: phi = fdo.empty_equality(phy_sym).solve()
```

The solution is done in SciPy; _FIDELIOR_ does not provide any algorithms for the solution of linear equations.

## A few words about implementation (advanced)

The actual data of a numeric extended array `u` is stored as a `numpy.ndarray` in the field `u._arr`. In the case of a symbolic extended array, the information that is stored in this field has type `FieldOperator`, which behaves similarly to a `numpy.ndarray`. E.g., the indexing conventions are _the same as in NumPy_, and objects of this type may undergo simple linear operations, such as addition, subtraction, taking an element or a subset, and multiplication by a number or a numeric array. Multiplication of two symbolic arrays is not linear and thus is not allowed. These "stripped" **symbolic arrays** are created in the following way, by specifying the "name" and the array shape:
```python
In [17]: f_sym = fdo.ndarray_sym('f', (3, 4))
In [18]: f_sym.shape
Out[18]: (3, 4)
```
The total number of elements in array of shape of `f_sym` is 3x4=12. This corresponds to 12 unknown variables, which is the number of columns in matrix `f_sym.M`. We can **derive** other `FieldOperator`s from `f_sym`, which may have a different shape, e.g.:
```python
In [19]: g_sym = f_sym[:, -1]
```
As if we were operating on NumPy arrays, `g_sym` is the last column of `f_sym`, with `g_sym.shape==(3,)`. The number of unknowns is still 12, but the number of elements in `g_sym` is equal to the number of rows in `g.M`, i.e. `g_sym.M.shape==(3, 12)`. When operator `g_sym` acts on a NumPy array (`numpy.ndarray`) `f` of the same shape as `f_sym`, it extracts the last column, exactly as its definition suggests:
```python
In [20]: f = numpy.random.randn(3, 4)
In [21]: g_sym.apply(f) # or simply g_sym(f)
Out[21]: array([ 0.05992443, -0.36946073,  0.27777805])
```
The "original" symbolic array `f` is, of course, also an operator. As one might guess, it is the _identity operator_ represented by a unit matrix. The operators may have an inhomogeneous part: e.g., we may have `h_sym = f_sym[:, -1] + 1`. The inhomogeneous part is stored in vector `h_sym.V`. In this example, `h_sym.V == array([1., 1., 1.])`.

The derived operators should still act on the same system of unknown variables, which is tracked by the **reference** field `g_sym.ref` of type `FieldRef`. For all derived operators, this field is _identical_ to the original `f_sym.ref`, i.e., operation `is` taken between them returns `True`. The reference must be the same for operators participating in binary operations. It is possible to have several _different_ extended arrays which use the same system of variables, and thus to share the reference -- see [Example 5](/examples#5-multiple-unknown-functions) on how this is done by using an `EACollection` (extended-array collection) object. The different extended arrays may even belong to different `Box`es, see [Example 6](/examples#6-mesh-refinement). The trick in the implementation of `EACollection` is to point the `._arr` field of each participating extended array to a portion of a 1D array.

The syntax for setting constraints on "stripped" symbolic arrays and solving equations is the same as described above for symbolic _extended_ arrays. Information about constraints set in the code must be available to all "derived" operators. Therefore, this information is also stored in the reference, in subfield `ref._bc`, in a form of a Python `dict`. A constraint is represented as a `FieldOperator` that gives zero when acting on a NumPy array that satisfies that constraint. When an `Equation` object is created, all the constraints are collected together into a solvable (square-matrix) linear system.

See [the test example](/examples/example0_basics.py) of these basic features.

## Where to go next?

See the [examples](/examples).

