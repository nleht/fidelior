#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 20 10:57:52 2021

Implicit and explicity methods for discontinuous Galerkin (dG) for advection, in 1D with constant V.

The basis functions are Legendre polynomials (orthogonal).

Various time-stepping methods are tried: explicit and implicit Runge-Kutta of orders up to 6.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
from scipy import special as spec
import matplotlib.pyplot as plt

import fidelior as fdo
from fidelior import end, half, Box, ConstraintEnforcer, Equation

# This package has some tools for calculating method stability and plotting stability regions
from dG_RK_tools import find_max_cfl, butcher_pq

import time
t_start = time.time()

####################################################################################################
#%% Setup
# Preferences

# Create an animation
make_movie = False
# To make a GIF, do:
# convert -delay 20 -loop 0 __example4_movie__/img00*[02468].png __tmp_example4_movie__/movie.gif
# convert -limit memory 1 -limit map 1  __example4_movie__/movie.gif -fuzz 10% -layers \
#    Optimize __tmp_example4_movie__/optimized.gif
# Insert a 5-second delay before repeating the loop
# convert __example4_movie__/optimized.gif \( +clone -set delay 500 \) +swap +delete  example4.gif

use_max_V = False # use theoretically calculated maximum stable velocity
V_default = 0.4  # if not the maximum, then this
# Tested up to NDG==6
NDG = 6 # order of the discontinuous Galerkin method
# Approximate the function u with Legendre polynomials P_n(xi) of orders n=0..NDG-1
# on each element with variable xi = (x-xc)/(dx/2) lying in interval [-1, 1]
algorithm = 'upstream' # 'upstream' or 'symmetric'
piecewise_const = False
use_constraint_enforcer = False # False is simpler, True involves automatic enforcement
rand_coef = .75 # Randomly non-uniform grid, =0 for uniform

RK = 'Fully implicit' # 'Theta' or 'Foust 4.1' or 'Fully implicit' or ... Recommend same order as dG
# Choose a sub-method
if RK=='Theta':
    # info for Theta method
    Theta = 0.51
elif RK=='Diag-implicit':
    # info for Diag-implicit method
    DI='Butcher 4'
elif RK=='Fully implicit':
    # Info for fully implicit method
    FI = 'GL6'

def get_str(RK):
    if RK=='Fully implicit':
        return RK+'('+FI+')'
    elif RK=='Diag-implicit':
        return RK+'('+DI+')';
    elif RK=='Theta':
        return RK+'('+str(Theta)+')'
    else:
        return RK

#%% Info for general methods
if RK=='Diag-implicit':
    # These are not always stable up to CFL=1
    # info for Diag-implicit method
    if DI=='Crouzeix 3':
        # Crouzeix's two-stage, 3rd order Diagonally Implicit Runge–Kutta method:
        DI_A = np.array([[1/2+np.sqrt(3)/6, 0], [-np.sqrt(3)/3, 1/2+np.sqrt(3)/6]])
        DI_b = np.array([1/2, 1/2])
    elif DI=='Butcher 4':
        # Butcher's three-stage, fourth-order method
        # not A-stable, B-stable or L-stable
        DI_A = np.array([[0,0,0],[1/4,1/4,0],[0,1,0]])
        DI_b = np.array([1/6,2/3,1/6])
    else:
        raise Exception('Unknown diag-implicit method')
    DI_N = len(DI_b)
    assert DI_A.shape==(DI_N,DI_N)
elif RK=='Fully implicit':
    # These are stable up to CFL=1
    # Info for fully implicit method (Gauss-Legendre)
    if FI=='GL4':
        # Gauss-Legendre 4th order
        FI_A = np.array([[1/4, 1/4-np.sqrt(3)/6], [1/4+np.sqrt(3)/6, 1/4]])
        FI_b = np.array([1/2, 1/2])
        FI_blow = np.array([1/2+np.sqrt(3)/2, 1/2-np.sqrt(3)/2])
    elif FI=='GL6':
        # Gauss-Legendre 6th order
        s15 = np.sqrt(15)
        FI_A = np.array([[5/36, 2/9-s15/15, 5/36-s15/30],
                         [5/36+s15/24, 2/9, 5/36-s15/24],
                         [5/36+s15/30, 2/9+s15/15, 5/36]])
        FI_b = np.array([5/18, 4/9, 5/18])
        FI_blow = np.array([-5/6, 8/3, -5/6])
    FI_N = len(FI_b)
    assert FI_A.shape==(FI_N,FI_N)

#%% Determine the max stable CFL
if RK=='Foust 4.1':
    p = 1/np.array([1,1,2,6,24,200])
    q = [1]
elif RK=='RK4':
    p = 1/np.array([1,1,2,6,24])
    q = [1]
elif RK=='Shu 3':
    p = 1/np.array([1,1,2,6])
    q = [1]
elif RK=='Mid-point' or RK=='Shu 2':
    p = 1/np.array([1,1,2])
    q = [1]
elif RK=='Diag-implicit':
    p, q = butcher_pq(DI_A, DI_b)
elif RK=='Fully implicit':
    # These should give Vmax = 1
    p, q = butcher_pq(FI_A, FI_b)
else:
    if RK=='Implicit mid-point' or RK=='Trapezoidal rule':
        Theta = 0.5
    elif RK=='Euler':
        Theta = 0
    elif RK=='Implicit Euler':
        Theta = 1
    else:
        Theta = 0
    p = np.array([1,1-Theta])
    q = np.array([1,-Theta])
    
Vmax = find_max_cfl(NDG, p, q, algorithm)*(1-rand_coef)
print('Vmax =', Vmax)

#%% We can use Vmax, or specify our own velocity
if use_max_V:
    V = Vmax # also CFL coefficient since dx=dt=1
else:
    V = V_default
dt = 1
# Good combination: NDG=4, RK='Shu 3', V=0.1

N = 50
L = N # fix grid step at 1
ncells = (N,)

dx_ = rand_coef*(2*np.random.rand(N)-1)+1 # uniformly distributed in [1-rand_coef, 1+rand_coef]
dx_ = dx_*L/np.sum(dx_)
x_ = np.hstack((0, np.cumsum(dx_))) - L/2

box = Box((N,))
x_nodes = box[0:end].assign(x_)
x_centers = fdo.aver(x_nodes) #, axis=0)
dx = fdo.diff(x_nodes)

def get_waveform(t, x):
    w = np.zeros(x.shape)
    xs = ((x - V*t + L/2) % L) - L/2
    i1 = (xs > 0) & (xs < L/4)
    w[i1] = 2*xs[i1]
    i2 = np.abs(xs+L/4)<L/8
    w[i2] = (L/2)*np.sqrt(1-((xs[i2]+L/4)/(L/8))**2)
    return w

################################################################################
#%% Discontinuous Galerkin using Legendre Polynomials
boxx = Box((N, NDG-1))
# NDG fields: u is symbolic, u[*]v are numeric
u = boxx.sym('u')[-half:end+half, 0:end]
((u[end+half, :] == u[half, :]) & (u[-half, :] == u[end-half,:])).constraint('periodic')

# Boundary conditions for the numeric fields
if use_constraint_enforcer:
    # A more complicated way to enforce bc
    _dep = u.as_zeros('bool') # initialized to False
    _dep[-half, :] = True; _dep[end+half, :] = True
    _periodic_bc = ConstraintEnforcer(u, _dep)
    def apply_periodic_bc(uv):
        _periodic_bc.enforce(uv)
else:
    # simpler: explicitly
    def apply_periodic_bc(uv):
        uv[end+half, :] = uv[half, :]
        uv[-half, :] = uv[end-half,:]

#%% Fill out the initial values
uv = u.as_zeros()
u1v = u.as_zeros()
u2v = u.as_zeros()
u3v = u.as_zeros()

uini = boxx.zeros()[-half:end+half, 0:end]

xb = x_nodes[0:end]
xc = x_centers[half:end-half] # (xb[1:]+xb[:-1])/2

NDGp = NDG # minimum is NDG, more for nicer pictures
quadrature = 'Gauss' # only Gauss works well
if quadrature=='Gauss':
    xi1, w1 = spec.orthogonal.p_roots(NDGp)
elif quadrature=='uniform':
    xi1b = np.linspace(-1,1,NDGp+1)
    xi1 = (xi1b[1:]+xi1b[:-1])/2
    w1 = np.diff(xi1b)
elif quadrature=='Chebyshev':
    xi1, w1 = spec.orthogonal.c_roots(NDGp)
    xi1 /= 2 # it was in [-2, 2] interval
    w1 /= np.pi


xi = np.tile(xi1, N)
xdetailc = np.kron(dx_/2, xi1) + np.repeat(xc, NDGp)
dxdetail = np.kron(dx_/2, w1) # NOT diff(xdetail)
ii = np.repeat(np.arange(N), NDGp)
istop = NDGp*(1+np.arange(N))-1

uinidetail = get_waveform(0, xdetailc)

for kdg in range(NDG):
    # Integrate uini*legendre(xi) over each interval using Gauss-Legendre quatrature
    tmp = uinidetail*dxdetail*spec.legendre(kdg)(xi)
    uinteg = np.diff(np.hstack((0,np.cumsum(tmp)[istop])))
    udg = uinteg*(2*kdg+1)/dx_
    if kdg==0 or not piecewise_const:
        uv[half:end-half, kdg] += udg
apply_periodic_bc(uv)

def get_udetail(uv):
    "Values of u at xdetailc"
    udetail = 0
    for kdg in range(NDG):
        udg = uv[half:end-half, kdg]
        udetail += spec.legendre(kdg)(xi)*udg[ii]
    return udetail

if False:
    plt.figure(1)
    plt.clf()
    plt.plot(xb, get_waveform(0, xb),'o')
    plt.plot(xdetailc, get_waveform(0,xdetailc))
    plt.plot(xdetailc, get_udetail(uv), '.-')

#%% The differential operators
def get_deriv(u, l):
    r = list(range(l-1,-1,-2))
    if len(r)==0:
        return 0
    else:
        return box[-half:end+half].assign( sum(u[:, m] for m in r) )

def du_upstream(u):
    "u is now an ExtendedArray, and this works for symbolic arrays, too"
    # If lower-dimensional slices would be implemented, this would not be necessary
    uep = box[-half:end-half].assign(
        sum(u[-half:end-half, l] for l in range(NDG)) )
    uem = uep.at((-1,)) # has span half:end+half
    du = boxx.allocate_as(u)[half:end-half, 0:end] # allocate float64 or symbolic
    for l in range(NDG):
        tmp = ( -(uep-uem*(-1)**l) + get_deriv(u, l)*2 )*V/dx*(2*l+1)
        du[:, l] = tmp[:]
    return du

def du_downstream(u):
    "Not debugged!"
    # If lower-dimensional slices would be implemented, this would not be necessary
    uem = box[half:end+half].assign(
        sum(u[half:end+half, l] for l in range(NDG)) )
    uep = uem.at((1,)) # has span -half:end-half
    du = boxx.allocate_as(u)[half:end-half,0:end]
    for l in range(NDG):
        tmp = ( -(uep-uem*(-1)**l) + get_deriv(u, l)*2 )*V/dx*(2*l+1)
        du[:,l] = tmp[:]
    return du

def du_symmetric(u):
    "u is now an ExtendedArray, and this works for symbolic arrays, too"
    uep = box[-half:end-half].assign(
        sum(u[half:end+half,l]*(-1)**l for l in range(NDG))/2 )
    uep += sum(u[-half:end-half,l] for l in range(NDG))/2
    uem = uep.at((-1,))
    du = boxx.allocate_as(u)[half:end-half,0:end]
    for l in range(NDG):
        tmp = ( -(uep-uem*(-1)**l) + get_deriv(u, l)*2 )*V/dx*(2*l+1)
        #s = span(du,0) # half:end-half or just :
        du[:,l] = tmp[:]
    return du

if algorithm=='upstream':
    du = du_upstream
    dua = du_downstream
elif algorithm=='symmetric':
    du = du_symmetric
    dua = du_symmetric
elif algorithm=='downstream':
    du = du_downstream
    dua = du_upstream
else:
    raise Exception('Unknown dG algorithm')

#%% Implicit method solvers
i_indep = (slice(half, end-half), slice(None)) # index for independent part in u
euler_equation = Equation(u-du(u) == np.nan)
midpoint_equation = Equation(u-du(u)/2 == np.nan)
if RK=='Theta':
    theta_equation = Equation((u-du(u)*Theta)[i_indep] == np.nan)
euleradj_equation = Equation(u+dua(u) == np.nan)

# Crouzeix's two-stage, 3rd order Diagonally Implicit Runge–Kutta method:
if RK=='Diag-implicit':
    # g[i] == k[i]*h from the definition of RK
    DI_g = [u.as_zeros() for i in range(DI_N)]
    DI_equations = []
    for i in range(DI_N):
        a = DI_A[i,i]
        if a==0:
            DI_equations.append(None)
        else:
            DI_equations.append(Equation(u - du(u)*a == np.nan))
    def DI_next(uv):
        global DI_g
        for i in range(DI_N):
            tmp = uv + sum(DI_g[j]*DI_A[i,j] for j in range(i))
            if DI_equations[i] is not None:
                DI_equations[i].update_rhs(None,  du(tmp))
                DI_g[i] = DI_equations[i].solve()
            else:
                DI_g[i].update( du(tmp) )
                apply_periodic_bc(DI_g[i])
        uv += sum(DI_g[i]*DI_b[i] for i in range(DI_N))
        return uv

if RK=='Fully implicit':
    boxxx = Box((N, NDG-1, FI_N-1))
    # We need a special definition for g
    FI_g = boxxx.sym('FI_g')[-half:end+half, 0:end, 0:end]
    ((FI_g[end+half,:,:] == FI_g[half,:,:]) &
     (FI_g[-half,:,:] == FI_g[end-half,:,:])).constraint('periodic')
    # The operator which needs to be inverted
    OPER = boxxx.allocate_as(FI_g)[half:end-half, 0:end, 0:end]
    duu_stor = OPER.as_zeros()
    g_tmp = [boxx[-half:end+half, 0:end] for i in range(FI_N)]
    for j in range(FI_N):
        g_tmp[j].assign(FI_g[:,:,j])
    for i in range(FI_N):
        OPER_i = g_tmp[i] - sum(du(g_tmp[j])*FI_A[i,j] for j in range(FI_N))
        OPER[:,:,i] = OPER_i[:,:]
    FI_equation = Equation(OPER == np.nan) # fdo.Solver(FI_g, OPER)
    def FI_next(uv):
        tmp = du(uv)
        for i in range(FI_N):
            duu_stor[:,:,i] = tmp[:,:]
        FI_equation.update_rhs(None, duu_stor)
        FI_g = FI_equation.solve() #_full(FI_g, duu_stor)
        uv[:,:] = uv[:,:] + sum(FI_g[:,:,i]*FI_b[i] for i in range(FI_N))
        apply_periodic_bc(uv)
        return uv

#%%
print('Time for setup =', time.time()-t_start)
tot_turns = 10
Nt = np.int(np.ceil(tot_turns*L/(V*dt)))
print('Wait for',dt*Nt*V/L,'turns')
t = 0
zmin,zmax = (-.2*L,0.7*L)
fig = plt.figure(100,figsize=(10,5))
if make_movie:
    # Create an animation
    kplot = 0
for kt in range(Nt+1):
    # Runge-Kutta from Chi-Wang Shu ENO/WENO lectures (page 386)
    # Other RK methods from https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods
    if RK=='Euler':
        # First order explicit RK method
        uv += du(uv)
    elif RK=='Shu 2':
        # Second order explicit RK method
        u1v.update(uv + du(uv))
        apply_periodic_bc(u1v)
        uv.update(uv/2 + u1v/2 + du(u1v)/2)
    elif RK=='Mid-point':
        # Very similar to Shu 2
        u1v.update(uv + du(uv)/2)
        apply_periodic_bc(u1v)
        uv += du(u1v)
    elif RK=='Shu 3':
        # Same as SSPRK3 from Wikipedia
        u1v.update(uv + du(uv))
        apply_periodic_bc(u1v)
        u2v.update((3/4)*uv + (1/4)*u1v + (1/4)*du(u1v))
        apply_periodic_bc(u2v)
        uv.update((1/3)*uv + (2/3)*u2v + (2/3)*du(u2v))
    elif RK=='Kutta 3':
        k1 = du(uv)
        u1v.update(uv + k1/2)
        apply_periodic_bc(u1v)
        k2 = du(u1v)
        u2v.update(uv - k1 + 2*k2)
        apply_periodic_bc(u2v)
        uv += (k1 + 4*k2 + du(u2v))/6
    elif RK=='RK4':
        # Classical Runge-Kutta, 4th order
        k1 = du(uv)
        u1v.update(uv + k1/2)
        apply_periodic_bc(u1v)
        k2 = du(u1v)
        u2v.update(uv + k2/2)
        apply_periodic_bc(u2v)
        k3 = du(u2v)
        u3v.update(uv + k3)
        apply_periodic_bc(u3v)
        uv += (k1 + 2*k2 + 2*k3 + du(u3v))/6
    elif RK=='Shu 4':
        # Something wrong with this one
        u1v.update(uv + du(uv)/2)
        apply_periodic_bc(u1v)
        u2v.update( (649/1600)*uv - (10890423/25193600)*dua(uv)
                   + (951/1600)*u1v + (5000/7873)*du(u1v) )
        apply_periodic_bc(u2v)
        u3v.update( (53989/2500000)*uv - (102261/5000000)*dua(uv) + (4806213/20000000)*u1v \
            - (5121/20000)*dua(u1v) + (23619/32000)*u2v + (7873/10000)*du(u2v) )
        apply_periodic_bc(u3v)
        uv.update( (1/5)*uv + (1/10)*du(uv) + (6127/30000)*u1v + (1/6)*du(u1v) + (7873/30000)*u2v \
            + (1/3)*u3v + (1/6)*du(u3v) )
    elif RK=='Foust 4.1':
        # From Forrest's dissertation: the low-storage RK4
        # It is also more stable than the 'classical' RK4
        aa = [0, -567301805773/1357537059087, -2404267990393/2016746695238,
              -3550918686646/2091501179385, -1275806237668/842570457699]
        bb = [1432997174477/9575080441755, 5161836677717/13612068292357,
              1720146321549/2090206949498, 3134564353537/4481467310338,
              2277821191437/14882151754819]
        cc = [0, 1432997174477/9575080441755, 2526269341429/6820363962896,
              2006345519317/3224310063776, 2802321613138/2924317926251]
        u1v = u.as_zeros()
        u2v = uv
        # k is u1, p is u2 in formula (3.48) in Foust dissertation
        for irk in range(5):
            u1v.update(aa[irk]*u1v + du(u2v))
            apply_periodic_bc(u1v) # gives a bit cleaner boundary crossing
            u2v += bb[irk]*u1v
        uv = u2v
    elif RK=='Fully implicit':
        uv = FI_next(uv)
    elif RK=='Diag-implicit':
        uv = DI_next(uv)
    elif RK=='Implicit Euler':
        euler_equation.update_rhs(None, uv[half:end-half,:])
        uv = euler_equation.solve()
    elif RK=='Implicit mid-point':
        # Same as Theta = 0.5
        # Much better than Shu 2
        midpoint_equation.update_rhs(None, du(uv)[i_indep])
        uv += midpoint_equation.solve()
    elif RK=='Trapezoidal rule':
        # Same as implicit mid-point
        midpoint_equation.update_rhs(None, (uv+du(uv)/2)[i_indep])
        uv = midpoint_equation.solve()
    elif RK=='Theta':
        theta_equation.update_rhs(None, (uv+du(uv)*(1-Theta))[i_indep])
        uv = theta_equation.solve()
    elif RK=='Adjoint Euler':
        # Always unstable
        uv.update(uv + dua(uv))
    elif RK=='McCormack':
        # McCormack, only works for stencil0=[(0,)] (CIR)
        u1v.update(uv + du(uv))
        apply_periodic_bc(u1v)
        uv.update(uv/2 + u1v/2 + dua(u1v)/2)
    elif RK=='Adjoint implicit Euler':
        # Interestingly, stable for CFL>2 (????)
        euleradj_equation.update_rhs(None, uv[i_indep])
        uv = euleradj_equation.solve()
    elif RK=='Central':
        # Always unstable
        uv.update( uv + du(uv)/2 + dua(uv)/2 )
    else:
        raise Exception('Unknown method')
    apply_periodic_bc(uv)
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0 or kt==Nt:
        if not plt.fignum_exists(fig.number):
            print('Figure closed, exiting ...')
            sys.exit()
        fig = plt.figure(fig.number)
        fig.clear() # plt.clf()
        plt.plot(xdetailc, get_udetail(uv))
        plt.plot(xc, uv[half:end-half,0],'o-')
        # Plot the exact value
        plt.plot(xc, get_waveform(t, xc),'k',alpha=0.3)
        plt.grid(True)
        plt.ylim(zmin,zmax)
        plt.xlim(-L/2,L/2)
        title = 'V={:.3g}'.format(V)+', method='+get_str(RK) + ', '
        title += 'DG'+str(NDG) + ' ' + algorithm + ': n = {:1.3f}'.format(V*t/L)
        plt.title(title)
        if make_movie:
            plt.savefig('figures/__example4_movie__/img{:05d}.png'.format(kplot))
            kplot += 1
        plt.pause(.1)
        #fig.waitforbuttonpress() # progress by clicking mouse in Figure 100
        #break
        
