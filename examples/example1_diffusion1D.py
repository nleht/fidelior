#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 14:51:49 2022

Implicit Crank–Nicolson method for solving a diffusion equation in 1D.
This method is also known as trapezoidal rule, or implicit mid-point.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import time
import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end, half, Box, Equation

do_plot = True # plot during the process? Set False to plot only the final
# There is more than one way to do it: 'optimized' is much faster; 'not optimized' is
# somewhat simpler to understand. See the implementation below.
optimized = True
# Works well on non-smooth grids! Here we have a random-size cell grid.
random_coef = 0.9 # set to zero for a uniform grid; must be 0 <= random_coef < 1

#%% Set up the calculation domain
N = 100 # number of cells
box = Box((N,))
a = 0; b = 1 # the calculation domain [a,b]
# We can use an arbitrary non-uniform grid
d0 = random_coef*(2*np.random.rand(N)-1)+1
dx_ = d0*(b-a)/np.sum(d0)
x_ = np.hstack((0, np.cumsum(dx_))) # node coordinates
x = box[0:end].assign(x_)
dx = fdo.diff(x)
dxc = fdo.aver(dx)

#%% Set up f
f = box.sym('f')[0:end] # symbolic array
def left_edge(t):
    return np.cos(2*np.pi*t)
(f[0] == left_edge(0)).constraint('left') # we could use any RHS because it will be reset anyway
(f[end] == 0).constraint('right') # the fixed bc
fv = f.as_zeros() # numerical value, same size as f, initialized to zeros
# fv[...] = 0 # initial value is zero by default
fv[0] = left_edge(0) # The initial boundary condition

#%% Set up diffusion
D = 0.1
dt = 0.01
# It pays to pre-calculate the operators in advance, if not optimized
def laplacian(u):
    return fdo.diff(fdo.diff(u)/dx)/dxc

# For faster solution, set up the operator in the equation in advance.
# This is the implicit method called 'Crank–Nicolson method',
# 'trapezoidal rule' or 'implicit mid-point method'
#equation = Equation(f - laplacian(f)*D*dt/2 == fv + laplacian(fv)*D*dt/2)
# The LHS operator is now set up. We will change only the RHS later.
# Note that "RHS" does not have to be all literally on the right-hand side of the equation.
# What is understood by RHS in FIDELIOR is all the _known_ functions (in this case, containing fv)
# We could have written, equivalently,
equation = Equation(f - fv == dt*D*laplacian(f + fv)/2)

#%% Time cycle: Solve diffusion
# The equation is fprev == fv, where fprev is f - Laplacian(f)*D*dt
T = 100
t = 0
if do_plot:
    fig = plt.figure(1)
    fig.clear()
    ax = fig.add_subplot()
else:
    tstart = time.time()
while t <= T:
    # Solve with the implicit BCs
    if not optimized:
        # Slower but easiest to program/understand
        (f[0] == left_edge(t+dt)).constraint('left') # at the next time moment
        fv = (f - laplacian(f)*D*dt/2 == fv + laplacian(fv)*D*dt/2).solve()
    else:
        # Much faster: do not spend time setting up the equation
        equation.update_rhs('left', left_edge(t+dt))     # update one of the constraints
        equation.update_rhs(None, fv + laplacian(fv)*D*dt/2)   # update the main equation
        fv = equation.solve() # or fv = fdo.solve(equation)
    t += dt
    if do_plot:
        if not plt.fignum_exists(1):
            print('Figure is closed, exiting')
            break
        ax.clear()
        ax.plot(x[0:end], fv[0:end],'.-')
        ax.grid(True)
        ax.set_xlabel('x')
        ax.set_ylabel('f(x, t)')
        ax.set_title('t = {:3.3f}'.format(t))
        ax.set_ylim(-1.1, 1.1)
        ax.set_xlim(0, 1)
        plt.pause(.01)

if not do_plot:
    print('Time to', ('' if optimized else 'set up and'),'invert', int(np.floor(T/dt)),
          N, 'x', N, 'sparse matrices =', time.time()-tstart, 'sec')
    # Plot the results (if we did not plot them before)
    plt.figure(1)
    plt.clf()
    plt.plot(x[0:end], fv[0:end],'.-')
    plt.grid(True)
    plt.xlabel('x')
    plt.ylabel('f(x, t)')
    plt.title('t = {:3.3f}'.format(t))
