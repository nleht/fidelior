#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 17 15:55:13 2022

We demonstrate features of TRUMP by solving a discretized Poisson equation

    Δ(φ)=-ρ

for φ with given ρ, in 3D in cartesian coordinates.

Direction references: x is to the right, y is into rear, z is up.

While doing this, we abuse the feature of Python3 which allows Greek characters
to be used in variable names.

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import time
import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib import cm
import fidelior as fdo
from fidelior import end, half

# Set up
Nx = 25; Ny = 25; Nz = 25;
# On my desktop computer, the typical solving times are for N = Nx = Ny = Nz:
# N = 25: T = 2--3 sec
# N = 35: T = 25--30 sec
# N = 50: T = 250--300 sec
# It looks like it is O(N**6) = O(Ntot**2) or even slower with higher N.
# Calculation time depends on CPU cache size.
box = fdo.Box((Nx, Ny, Nz))
dx = 1/Nx; dy = 1/Ny; dz = 1/Nz
x = np.linspace(0, 1, Nx+1)
y = np.linspace(0, 1, Ny+1)
z = np.linspace(0, 1, Nz+1)

ρ = box[1:end-1, 1:end-1, 1:end-1]
# The charge density ρ does not need to be defined at the boundary, so the
# negative number of ghost cells is specified (we cut off one layer)
print('Setting ρ=1 in the domain [(0--1) x (0--1) x (0--1)]')
ρ.assign(np.ones(ρ.nt))
# note that ρ[0,...] or ρ[end,...] whould give an error because the boundary is cut off.
# FIDELIOR uses MATLAB-style indexing, not Python-style! In particular, the upper
# boundary ('stop' of the slice) truly _is_ the last element, not the element after the last
# as it would be in Python indexing. Also, negative indices in ExtArrays
# mean exactly what they mean - ghost cells below the lower boundary.
φ = box.sym('φ')[0:end, 0:end, 0:end]
# Laplacian of φ
Δφ = fdo.diff(fdo.diff(φ,0),0)/dx**2 + fdo.diff(fdo.diff(φ,1),1)/dy**2 +\
        fdo.diff(fdo.diff(φ,2),2)/dz**2
print('Setting φ=0 on the right edge, bottom, top, front and back')
((φ[end,:,:] == 0) & \
 # -- right facet
 (φ[1:end-1,0,:] == 0) & \
 # -- front
 (φ[1:end-1,end,:] == 0) & \
 # -- back
 (φ[1:end-1,1:end-1,0] == 0) & \
 # -- bottom
 (φ[1:end-1,1:end-1,end] == 0) \
 # -- top
 ).constraint('fixed')
# -- We cannot use φ[:,0,:] = 0 here because it would conflict/redefine the previous BC

# Solution #1
print('\n*** Solution #1: φ=0 on the left facet ***')
t0 = time.time()
print('Setting φ=0 on the left facet')
(φ[0,:,:] == 0).constraint('left')
print('Solving Δφ=-ρ ...', flush=True)
φ1 = (Δφ == -ρ).solve() # BC are automatically included
print('Solving time:', time.time()-t0, flush=True)
print('Error =', fdo.max(abs(Δφ(φ1)+ρ)))

# Solution #2
print('\n*** Solution #2: φ=1 on the left facet ***')
t0 = time.time()
print('Updating the BC to φ=1 on the left facet')
(φ[0,:,:] == 1).constraint('left') # constraint is replaced with the new one
print('Solving Δφ=-ρ ...', flush=True)
φ2 = (Δφ == -ρ).solve() # the solver already knows about the new BC
print('Solving time:', time.time()-t0, flush=True)
print('Error =', fdo.max(abs(Δφ(φ2)+ρ)))

# Solution #3
print('\n*** Solution #3: ∂φ/∂n=0 on the left facet ***')
t0 = time.time()
print('Updating the BC to ∂φ/∂n=0 on the left facet')
(φ[0,:,:] == φ[1,:,:]).constraint('left') # Neumann B.C. on the left
print('Solving Δφ=-ρ ...', flush=True)
φ3 = (Δφ == -ρ).solve() # the solver already knows about the new BC
print('Solving time:', time.time()-t0, flush=True)
print('Error =', fdo.max(abs(Δφ(φ3)+ρ)))


#%%#################################################################################################
# Plot solution #1
# print('Please close the figure window to continue') # if not using IPython
X, Y = np.meshgrid(x, y, indexing='ij') # MATLAB convention of reversing the first two indices

def plot_3D(fignum, φ, title):
    version = mpl.__version__
    fig = plt.figure(fignum, figsize=(6,5))
    fig.clear()
    if version <= '3.4.0':
        ax = fig.gca(projection='3d') # now Matplotlib complains about this!
    else:
        ax = fig.add_subplot(projection='3d')
    vmin = fdo.min(φ)
    vmax = fdo.max(φ)
    levels = np.linspace(vmin, vmax, 11)
    for iz in range(Nz+1):
        ax.contour(X, Y, φ[:,:,iz], zdir='z', offset=z[iz], vmin=vmin, vmax=vmax, levels=levels,
                      cmap=mpl.cm.coolwarm, alpha=0.3)
    ax.set_xlabel('X'); ax.set_xlim(0, 1)
    ax.set_ylabel('Y'); ax.set_ylim(0, 1)
    ax.set_zlabel('Z'); ax.set_zlim(0, 1)
    
    if version <= '3.0.3':
        ax.set_aspect('equal') # does not work with newer matplotlib
        # See https://stackoverflow.com/a/31364297 for a possible solution
    elif version >= '3.3.1':
        ax.set_box_aspect([1,1,1])
    ax.set_title(title)
    
plot_3D(1, φ1, r'$\phi$ for $\rho=1$ and $\phi=0$ at the boundary')
plot_3D(2, φ2, r'$\phi$ for $\rho=1$ and $\phi=1$ on the left facet')
plot_3D(3, φ3, r'$\phi$ for $\rho=1$ and $\partial\phi/\partial n=0$ on the left facet')
plt.show()
