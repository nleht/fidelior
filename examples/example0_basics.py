#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 21:37:26 2022

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import numpy as np
import fidelior as fdo
from fidelior import half, end

#%% Run some tests
f = fdo.ndarray_sym('f', (5,))
# -- same as f = FieldOperator(FieldRef('f', (5,)), init=True)
g = f[3:5]
c = (f[0:2] == g) # an equality
c.constraint('a constraint') # set the constraint -- an equation is enforced
dep = np.zeros(f.shape, dtype=bool)
dep[3:5] = True
fval = np.array([1,2,3])
system = fdo.Equation(f[0:3] == fval).system
fsol = system.solve_system()
# Or just: tmp = (f[0:3] == np.array([1,2,3])).solve()
print('f =', fsol)

h = fdo.ndarray_sym('h', (2,3))
if True:
    (h[:,0]==1).constraint('left')
    (h[:,-1]==2).constraint('right')
else:
    ((h[:,0]==1) & (h[:,-1]==2)).constraint('both')
eq = (h[:,1]==5)
print('h =\n', eq.solve())
# Replacing the BC is easy!
(h[:,-1]==4).constraint('right')
print('h =\n', eq.solve())

#%% Enforcing the BC -- need to define which 
f2 = (f[~dep] == fval).solve()
print('f =', f2)
# Obtain the same but directly, plus enforce bc
f1 = np.zeros(f.shape)
f1[~dep] = fval
# Now, enforce bc

f_bc = fdo.ConstraintEnforcer(f)
f_bc.dep[3:5] = True
f_bc.freeze()
f_bc.enforce(f1)
# Or just
# ConstraintEnforcer(f, dep).enforce(f1)

#%% Check if it works for fields in multiple dimensions
if False:
    h_dep = np.ones(h.shape, dtype='bool')
    h_dep[:,1] = False
    h_bc = fdo.ConstraintEnforcer(h, h_dep)
else:
    # Equivalent
    h_bc = fdo.ConstraintEnforcer(h, True)
    h_bc.dep[:,1] = False
    h_bc.freeze()
h1 = np.zeros(h.shape)
h1[:,1] = 5
h_bc.enforce(h1)
print(h1)
for rhs in [1,2,3,4]:
    (h[:,-1]==rhs).constraint('right')
    h_bc.update().enforce(h1)
    print('explicit h =\n', h1)

#%% Extended Arrays
box = fdo.Box((5,5))
tmp = box[-half:end+1+half, -2:end+2] # Unallocated array
tmp._arr = np.random.rand(*tmp.nt)
tmp2 = box.zeros()[half:end+5+half, -3:end+1] # Allocated array
tmp2[...] = 4

s = box.sym('s')[-1:end, -1:end]
# Insufficient BC:
# ((s[-1,:]==s[end,:]) & (s[:,-1]==s[:,end])).constraint('periodic')
# Now, sufficient:
((s[-1,0:end]==s[end,0:end]) & (s[0:end,-1]==s[0:end,end]) & \
 (s[-1,-1]==s[end,end])).constraint('periodic')

sv = (s[0:end,0:end]==np.random.randn(6,6)).solve()
print('Periodic s =', sv, '=\n', sv._arr)
# We have not specified which elements are independent, so we can solve a different equation:
sv = (s[-1:end-1,-1:end-1]==np.random.randn(6,6)).solve()
print('Periodic s =', sv, '=\n', sv._arr)

#%% Enforcing BC
s1 = s.as_zeros()
s1[0:end,0:end] = np.random.randn(6,6)
dep = s.as_zeros('bool')
dep[...] = True
dep[0:end,0:end] = False
s_bc = fdo.ConstraintEnforcer(s, dep)
s_bc.enforce(s1)

#%% Shifts
ds = s.at((half,0))-s.at((-half,0))
print('ds(s1) =\n', ds(s1))
print('aver(s1) =\n', fdo.aver(s, 0)(s1))

#%% Solve a poisson equation
def laplacian(ea):
    return fdo.diff(fdo.diff(ea, 0), 0) + fdo.diff(fdo.diff(ea, 1), 1)
phi = box.sym('s')[0:end, 0:end]
((phi[0,:]==1) & (phi[end,:]==0) & (phi[1:end-1,0]==0) & \
 (phi[1:end-1,end]==0)).constraint('Dirichlet')

phi0 = (laplacian(phi)==0).solve()
print('phi0 =', phi0, '=\n', phi0._arr)
