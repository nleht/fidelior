#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 21 21:33:55 2021

All functions collected for convenient import

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import numpy as np
import matplotlib.pyplot as plt

def butcher_pq(A, b):
    "Extract the stability function R(z)=p(z)/q(z) from the Butcher tableau for an RK method."
    A = np.array(A)
    b = np.array(b)
    assert np.abs(np.sum(b)-1)<1e-12
    n = len(b)
    assert A.shape==(n,n)
    lamp, _ = np.linalg.eig(A - np.outer(np.ones((n,)),b))
    # p(z) == (1-lam1*z)*(1-lam2*z)*...*(1-lamn*z)
    p = np.trim_zeros(np.poly(lamp))
    lamq, _ = np.linalg.eig(A)
    q = np.trim_zeros(np.poly(lamq))
    return p,q

def make_R(p, q):
    "Create a callable function R(z)=p(z)/q(z)"
    p1d = np.poly1d(np.flip(np.array(p).astype(np.complex)))
    q1d = np.poly1d(np.flip(np.array(q).astype(np.complex)))
    return (lambda z: p1d(z)/q1d(z))

def plot_stability(p, q=[1], color='k'):
    "Plot the tability region boundary |R(z)|==1 for an RK method."
    tha = (np.arange(360)+0.5)*np.pi/180
    pc = np.array(p).astype(np.complex)
    qc = np.array(q).astype(np.complex)
    pn = len(pc)
    qn = len(qc)
    sn = max(pn, qn)
    r = np.zeros((len(tha),sn-1), dtype=np.complex)
    for ith, th in enumerate(tha):
        sc = np.zeros((sn,), dtype=np.complex)
        sc[:pn] = pc[:]
        sc[:qn] -= np.exp(1j*th)*qc[:]
        r[ith,:] = np.roots(np.flip(sc))
    plt.plot(np.real(r), np.imag(r), '.', color=color)

def plot_zscaled(L, color='k', algorithm='upstream', plot=True):
    """The scaled argument zscaled of the stability function for a dG method, defined as
        z = lambda*dt = -CFL*zscaled
    """
    larr = np.arange(L)
    imat, jmat = np.meshgrid(larr, larr, indexing='ij')
    Dmat = (imat+jmat) % 2
    Dmat[jmat>imat] = 0
    Emat = np.ones((L, L), dtype=np.int)
    Ematp = Emat - 2*(jmat % 2)
    Lmat = np.diag(2*larr+1)
    Smat = np.diag((-1)**larr)
    LSmat = Lmat @ Smat
    if algorithm=='upstream':
        z0 = Lmat @ (Emat-2*Dmat)
        zm = - LSmat @ Emat
        zp = 0
    elif algorithm=='symmetric':
        z0 = 0.5 * Lmat @ (Emat - Smat @ Ematp - 4*Dmat)
        zm = - 0.5 * LSmat @ Emat
        zp = 0.5 * Lmat @ Ematp
    elif algorithm=='downstream':
        # This one is rather useless
        z0 = - Lmat @ (Smat @ Ematp + 2*Dmat)
        zm = 0
        zp = Lmat @ Ematp
    else:
        raise ValueError('Unknown algorithm')
    tha = (np.arange(360)+0.5)*np.pi/180
    zs = np.zeros((len(tha),L), dtype=np.complex)
    for ith, th in enumerate(tha):
        zscaled = z0 + np.exp(-1j*th)*zm + np.exp(1j*th)*zp
        zs[ith,:],_ = np.linalg.eig(zscaled)
    if plot:
        return plt.plot(np.real(zs), np.imag(zs), '.', color=color)
    else:
        return zs

def find_max_cfl(L, p, q, algorithm):
    """Systematic determination of maximum CFL for given L, p and q"""
    zs = plot_zscaled(L, plot=False, algorithm=algorithm)
    R = make_R(p, q)
    bot, top = 0, 1
    while True:
        mid = (bot+top)/2
        tmp = np.max(np.abs(R(-mid*zs)))-1
        if tmp>1e-6:
            top = mid
        else:
            bot = mid
        if top-bot<1e-6:
            break
    return (bot+top)/2

def poly_ratio(p, q, n):
    "The ratio of two polynomials, up to order n"
    res = np.zeros((n+1,))
    q = np.array(q)
    assert q[0]!=0
    nq = len(q)-1
    rem = np.zeros((n+nq+1,))
    rem[0:len(p)] = p[:]
    for i in range(n+1):
        res[i] = rem[i]/q[0]
        rem[i:i+nq+1] -= res[i]*q
    return res

def exp_expansion(n):
    res = np.zeros((n+1,))
    term = 1
    for i in range(n+1):
        res[i] = term
        term /= (i+1)
    return res

def rk_order(p, q):
    "Compare the ratio of two polynomials to exp(z) expansion"
    # Could there be a better way?
    assert q[0]!=0
    nq = len(q)-1
    q = np.array(q)
    n = nq+len(p)
    rem = np.zeros((n+nq,))
    rem[0:len(p)] = p[:]
    term = 1
    i = 0
    while i<n:
        r = rem[i]/q[0]
        rem[i:i+nq+1] -= r*q
        if np.abs(r-term)>1e-12:
            break
        i += 1
        term /= i
    return i-1
