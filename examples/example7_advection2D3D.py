#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 12:42:46 2022

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

#%% Obligatory imports
import time
import numpy as np
from matplotlib import pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d
import fidelior as fdo
from fidelior import end, half

#%% Settings
# Create an animation
make_movie = False
# For instructions to make a GIF, see the text of Example 4 (dG method)
ndim = 3 # 2 or 3 dimensions
bc_choice = 'periodic' # (recommend) periodic or zero
flow_choice = 'rotation' # parallel or rotation
dudt_choice = 'conserv3' # central2 or deriv3 or (recommend) conserv3
timestep_choice = 'ode23' # trapezoidal2 or McCormack2 (has own dudt) or (recommend) ode23
errmin, errmax = 1e-4, 1e-2 # for embedded RK method
# dd == number of ghost nodes
if dudt_choice[-1]=='3': # 3rd-order (spatial) method
    dd = 2
elif dudt_choice[-1]=='2': # 2nd-order (spatial) method
    dd = 1

#%% Geometry and other basics
N = (100,)*ndim # do not set > 25 for ndim==3 and implicit methods (trapezoidal2)
L = (1,)*ndim
box = fdo.Box(N)
dr = tuple(np.array(L)/np.array(N))
r_ = tuple(np.linspace(-dr[a], dr[a]*(N[a]+1), N[a]+3) for a in range(ndim))
rav_ = tuple((x_[1:] + x_[:-1])/2 for x_ in r_)
r = box.ndgrid(*r_)[(slice(-1, end+1),)*ndim] # a tuple

def div(A):
    A = tuple(A)
    return sum(fdo.diff(A[a], a)/dr[a] for a in range(ndim))

uv = box.zeros()[(slice(-dd, end+dd),)*ndim]
u = uv.as_sym('u')

#%% Boundary conditions
if bc_choice == 'zero':
    if ndim==2:
        ((u[-dd:-1, :] == 0) & (u[end+1:end+dd, :] == 0) &
         (u[0:end, -dd:-1] == 0) & (u[0:end, end+1:end+dd] == 0)).constraint('zero')
        def explicit_bc(uv):
            "Faster but harder to change and more prone to bugs"
            uv[-dd:-1, :] = 0
            uv[end+1:end+dd, :] = 0
            uv[0:end, -dd:-1] = 0
            uv[0:end, end+1:end+dd] = 0
    elif ndim==3:
        ((u[-dd:-1, :, :] == 0) & (u[end+1:end+dd, :, :] == 0) &
         (u[0:end, -dd:-1, :] == 0) & (u[0:end, end+1:end+dd, :] == 0) &
         (u[0:end, 0:end, -dd:-1] == 0 & (u[0:end, 0:end, end+1:end+dd] == 0))).constraint('zero')
        def explicit_bc(uv):
            uv[-dd:-1, :, :] = 0
            uv[end+1:end+dd, :, :] = 0
            uv[0:end, -dd:-1, :] = 0
            uv[0:end, end+1:end+dd, :] = 0
            uv[0:end, 0:end, -dd:-1] = 0
            uv[0:end, 0:end, end+1:end+dd] = 0
    else:
        raise Exception('ndim')
elif bc_choice == 'periodic':
    if ndim==2:
        ((u[-dd:dd-1, :] == u[end-dd+1:end+dd, :]) &
         (u[dd:end+dd, -dd:dd-1] == u[dd:end+dd, end-dd+1:end+dd])).constraint('periodic')
        def explicit_bc(uv):
            "Faster but harder to change and more prone to bugs"
            uv[-dd:-1, :] = uv[end-dd+1:end, :]
            uv[end+1:end+dd, :] = uv[0:dd-1, :]
            uv[0:end, -dd:-1] = uv[0:end, end-dd+1:end]
            uv[0:end, end+1:end+dd] = uv[0:end, 0:dd-1]
    elif ndim==3:
        ((u[-dd:dd-1, :, :] == u[end-dd+1:end+dd, :, :]) &
         (u[dd:end+dd, -dd:dd-1, :] == u[dd:end+dd, end-dd+1:end+dd, :]) &
         (u[dd:end+dd, dd:end+dd, -dd:dd-1] == 
          u[dd:end+dd, dd:end+dd, end-dd+1:end+dd])).constraint('periodic')
        def explicit_bc(uv):
            "Faster but harder to change and more prone to bugs"
            uv[-dd:-1, :, :] = uv[end-dd+1:end, :, :]
            uv[end+1:end+dd, :, :] = uv[0:dd-1, :, :]
            uv[0:end, -dd:-1, :] = uv[0:end, end-dd+1:end, :]
            uv[0:end, end+1:end+dd, :] = uv[0:end, 0:dd-1, :]
            uv[0:end, 0:end, -dd:-1] = uv[0:end, 0:end, end-dd+1:end]
            uv[0:end, 0:end, end+1:end+dd] = uv[0:end, 0:end, 0:dd-1]
    else:
        raise Exception('ndim')
else:
    raise Exception('unknown')

def implicit_bc(uv):
    "About 2-3 times slower than explicit_bc but more universal and simpler"
    fdo.Equation((u-uv)[(slice(0, end),)*ndim] == 0).solve(out=uv)

apply_bc = explicit_bc # explicit_bc or implicit_bc

#%% Initial distribution
def initial_distribution():
    uini = r[0].as_zeros() # initialized to zeros
    if ndim==2:
        centers = [3*L[0]/4, L[1]/2]
        halfwidths = [L[0]/8, L[1]/4]
    elif ndim==3:
        centers = [3*L[0]/4, L[1]/2, L[2]/2]
        halfwidths = [L[0]/8, L[1]/4, L[2]/6]
    else:
        raise Exception('ndim')
    i = ~uini.as_zeros('bool') # True
    for k in range(ndim):
        i = i & (abs(r[k] - centers[k]) < halfwidths[k])
    uini[i] = 1
    return uini

uv.update( initial_distribution() )
# Smear it a bit
for k in range(0):
    for axis in range(ndim):
        uv.update(fdo.aver(fdo.aver(uv, axis), axis))
    apply_bc(uv)

#%% Velocity
dt = 1

def cross(a, b):
    if ndim==2:
        return (-b[1], b[0])
    elif ndim==3:
        "There is a builtin numpy.cross, but anyway"
        return (a[1]*fdo.aver(b[2], 1) - a[2]*fdo.aver(b[1], 2),
                a[2]*fdo.aver(b[0], 2) - a[0]*fdo.aver(b[2], 0),
                a[0]*fdo.aver(b[1], 0) - a[1]*fdo.aver(b[0], 1))
    else:
        raise Exception('ndim in cross')

if flow_choice == 'parallel':
    # Parallel translation
    if ndim==2:
        CFL = (.4, .3)
    elif ndim==3:
        CFL = (.3, .2, .4)
    V = tuple(np.array(CFL)*np.array(dr)/dt)
    Va = V
elif flow_choice == 'rotation':
    # Rotation
    alpha = 0.012 # angular velocity
    if ndim==2:
        rotaxis = 1
    elif ndim==3:
        th = np.pi/3
        ph = 0
        rotaxis = [np.sin(th)*np.cos(ph), np.sin(th)*np.sin(ph), np.cos(th)]
    else:
        raise Exception('ndim')
    CFL = alpha*dt/dr[0]*L[0]/2
    print('CFL0 =', CFL, '(to be adjusted)')
    rr = sum((r[a]-L[a]/2)**2 for a in range(ndim))
    psi = alpha*rr/2
    for axis in range(ndim):
        psi = fdo.aver(psi, axis)
    # Velocity represented as alpha cross grad(psi)
    # This way its divergence is zero (incompressible flow)
    gradpsi = tuple(fdo.diff(psi, a)/dr[a] for a in range(ndim))
    V = cross(rotaxis, gradpsi)
    # On the nodes
    Va = tuple(fdo.aver(V[axis], axis) for axis in range(ndim))
else:
    raise Exception('unknown flow_choice')

if timestep_choice=='ode23':
    dt = 10 # test how well the embedded method chooses dt

#%% The time derivative function
if dudt_choice=='central2':
    def dudt(u):
        "Central difference -- use only with implicit methods"
        result = -div(V[axis]*fdo.aver(u, axis) for axis in range(ndim))
        if dd>1:
            # We should not really use "restrict" because it leads to confusion.
            # However, we have too many ghost cells now and the result is too big!
            return result.restrict[(slice(0,end),)*ndim]
        else:
            return result
elif dudt_choice=='deriv3':
    def dudt(u):
        "Not conservative!"
        res = 0
        for axis, Vai in enumerate(Va):
            dudxip = +(u.shift(-2, axis)/6 - u.shift(-1, axis) + u/2 + u.shift( 1, axis)/3)
            dudxim = -(u.shift( 2, axis)/6 - u.shift( 1, axis) + u/2 + u.shift(-1, axis)/3)
            dudxi = ((Vai>=0)*dudxip + (Vai<0)*dudxim)/dr[axis]
            # dudxi = (dudxip+dudxim)/2/dr[axis]
            res = res - Vai*dudxi # Cannot use -= because it keeps the previous size!!!
        return res
elif dudt_choice=='conserv3':
    def dudt(u):
        "Conservative operator"
        # Face values ufx, ufy
        Vuf = []
        for axis, Vi in enumerate(V):
            ufp = -u.shift(-1-half, axis)/6 + u.shift(-half, axis)*5/6 + u.shift(half, axis)/3
            ufm = -u.shift(1+half, axis)/6 + u.shift(half, axis)*5/6 + u.shift(-half, axis)/3
            Vuf.append( Vi*((Vi>=0)*ufp + (Vi<0)*ufm) )
        return -div(Vuf)
else:
    raise Exception('unknown')

#%% Time stepping
# Theta method
# Theta method is a generalization of 1st-2nd order explicit/implicit methods
# For theta==.5, it is Crank-Nicolson/trapezoidal rule/implicit mid-point
# For theta==0, it is explicit (forward) Euler
# For theta==1, it is implicit (backward) Euler
theta = .5
equation = fdo.Equation( (u - uv)/dt == theta*dudt(u) + (1-theta)*dudt(uv) )

#%% Explicit RK method
# Popular MATLAB algorithms
# http://www.ece.northwestern.edu/local-apps/matlabhelp/techdoc/ref/ode45.html
g1 = uv.as_zeros()
g2 = uv.as_zeros()
g3 = uv.as_zeros()
g4 = uv.as_zeros()
update_g_bc = True
def ode23(uv, dudt, dt):
    """ODE23: Bogacki and Shampine embedded method
    https://en.wikipedia.org/wiki/Bogacki%E2%80%93Shampine_method
    """
    global g1, g2, g3, g4, recalculate
    if not recalculate:
        g1 = g4
    else:
        g1.update( dt*dudt(uv) )
        if update_g_bc:
            apply_bc(g1)
    # Zero change at the border is acceptable for intermediate calculations (?)
    g2.update(dt*dudt(uv + g1/2))
    if update_g_bc:
        apply_bc(g2)
    g3.update(dt*dudt(uv + g2*3/4))
    if update_g_bc:
        apply_bc(g3)
    y = uv + g1*2/9 + g2/3 + g3*4/9 # third order
    if not update_g_bc:
        apply_bc(y)
    g4.update(dt*dudt(y)) # store "internally"
    if update_g_bc:
        apply_bc(g4)
    z = uv + g1*7/24 + g2/4 + g3/3 + g4/8 # second order
    if not update_g_bc:
        apply_bc(z)
    return y, z

#%% 3D plot
def make_3dplot(fig, u, title, vmin, vmax):
    Nz = box.num_cells[2]
    x, y, z = r_[0][1:-1], r_[1][1:-1], r_[2][1:-1]
    X, Y = np.meshgrid(x, y, indexing='ij')
    version = mpl.__version__
    fig.clear()
    if version <= '3.4.0':
        ax = fig.gca(projection='3d') # now Matplotlib complains about this!
    else:
        ax = fig.add_subplot(projection='3d')
    levels = np.linspace(vmin, vmax, 11)
    for iz in range(Nz+1):
        cset = ax.contour(X, Y, u[0:end,0:end,iz], zdir='z', offset=z[iz],
                          vmin=vmin, vmax=vmax, levels=levels,
                          cmap=mpl.cm.coolwarm, alpha=0.6)
    ax.set_xlabel('X')
    ax.set_xlim(0, L[0])
    ax.set_ylabel('Y')
    ax.set_ylim(0, L[1])
    ax.set_zlabel('Z')
    ax.set_zlim(0, L[2])
    if version <= '3.0.3':
        ax.set_aspect('equal') # does not work with newer matplotlib
        # See https://stackoverflow.com/a/31364297 for a possible solution
    elif version >= '3.3.1':
        ax.set_box_aspect([1,1,1])
    ax.set_title(title)
    ax.set_title(title)
    fig.canvas.draw()

def str_array(a):
    return '[' + ', '.join('{:3.3f}'.format(a1) for a1 in a) + ']'

#%% The long cycle
fig = plt.figure(1, figsize=(6,6))

t = 0
t0 = time.time()
angle = 0
kplot = 0
for kt in range(10001): #int(np.floor(2*np.pi/alpha))):
    if not plt.fignum_exists(1):
        print('Exit')
        break
    if kt < 20 or (kt < 1000 and kt % 10 == 0) or kt % 100 == 0:
        title = 'kt = {:d}, t = {:3.3f}'.format(kt, t)
        if flow_choice=='parallel':
            title += ', D = ' + str_array(np.array(V)*t)
        elif flow_choice=='rotation':
            title += ', turns = {:3.3f}'.format(angle/(2*np.pi))
        if ndim==2:
            fig.clear()
            plt.pcolor(rav_[0], rav_[1], uv[0:end, 0:end].T)
            plt.gca().set_aspect('equal')
            plt.title(title)
        elif ndim==3:
            make_3dplot(fig, uv, title, -.2, 1.2)
        else:
            raise Exception('ndim')
        if make_movie:
            plt.savefig('figures/__example7_movie_{:d}D__/img{:05d}.png'.format(ndim,kplot))
            kplot += 1        
        plt.pause(.01)
    if timestep_choice=='ode23':
        recalculate = True
        changed = False
        while recalculate:
            uv3, uv2 = ode23(uv, dudt, dt)
            err = fdo.max(abs(uv3-uv2))/fdo.max(abs(uv))
            if err < errmin:
                dt *= 1.1
                print('Err = {:5.5f}: Increasing dt to {:5.5f}'.format(err, dt))
                changed = True
            elif err > errmax:
                dt /= 1.1
                print('Err = {:5.5f}: Decreasing dt to {:5.5f}'.format(err, dt))
                changed = True
            else:
                recalculate = False
                uv = uv3
                if changed:
                    print('*** Step {:d} completed, err = {:5.5f}'.format(kt, err))
    elif timestep_choice=='trapezoidal2':
        uv = equation.solve()
        equation.update_rhs(None, (1-theta)*dudt(uv) + uv/dt)
    elif timestep_choice=='McCormack2':
        # Does NOT use dudt!!!
        # uvi has smaller size!!!
        uv1 = uv - div(V[a]*fdo.upstream(uv, V[a], a) for a in range(ndim))*dt
        uv.update(
            (uv+uv1)/2 - div(V[a]*fdo.upstream(uv1, -V[a], a) for a in range(ndim))/2*dt)
        apply_bc(uv)
    t += dt
    if flow_choice=='rotation':
        angle +=  dt*alpha
print(time.time()-t0)
