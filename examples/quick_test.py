#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The quick test from the README file.
Author: Nikolai G. Lehtinen

This file is a part of "Examples" section of FIDELIOR package <https://gitlab.com/nleht/fidelior>.

This file (c) by Nikolai G. Lehtinen

This file is licensed under a
Creative Commons Attribution 4.0 International License.

You should have received a copy of the license along with this
work. If not, see <http://creativecommons.org/licenses/by/4.0/>.
"""

import numpy as np
from matplotlib import pyplot as  plt
import fidelior as fdo
from fidelior import end, half

# Set up the simple geometry
N = 100
x1 = np.arange(N+1)-N/2; y1 = x1
box = fdo.Box((N, N))
x, y = box.ndgrid(x1, y1)[0:end, 0:end]

# Charge density
x0 = 25; y0 = -25; w = 10;
rho = fdo.exp(-((x-x0)**2+(y-y0)**2)/(2*w**2))

# Unknown electrostatic potential
phi = box.sym('phi')[0:end, 0:end]

# These functions use 'fdo.diff(u, axis)' which is itself defined as
# def diff(u, axis): u.shift(half, axis) - u.shift(-half, axis)
def grad(u):
    return (fdo.diff(u, axis=0), fdo.diff(u, axis=1))
def div(A):
    return fdo.diff(A[0], axis=0) + fdo.diff(A[1], axis=1)

# Unknown electric field
E = grad(-phi)

# Boundary conditions (constraints with which the equation will be solved)
(phi[[0, end], :] == 0).constraint('Dirichlet') # on left and right edges
(E[1][1:end-1, [half, end-half]] == 1).constraint('Neumann') # Ey = 1 on top and bottom

# Set up and solve the equation
phiv = (div(E) == rho).solve()

# Plot and check the error
plt.pcolor(x1, y1, phiv[:,:].T, shading='auto')
plt.gca().set_aspect('equal')
print('Error =', fdo.max(fdo.abs(div(grad(-phiv))-rho)))
