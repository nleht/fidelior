#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 23:11:29 2020

Testing a nonuniform grid

@author: nleht
"""

import numpy as np
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end, half
from fidelior.plotting import UpdateablePcolor
fdo.set_global_debug_level(3)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

#%% First, test the uniform cylindrical geometry
Nr = 100
Nz = 200
ncells = (Nr, Nz)
Lr = 1
Lz = 2
rho_radius = 0.03
z_exact_zero = False
geoms = []
phis = []
rhos = []
phi_solvers = []

for uniform in [True, False]:
    if uniform:
        dr = Lr/Nr
        dz = Lz/Nz
    else:
        random_coef = 0.9
        dr = fdo.random_grid_delta(Lr, Nr, random_coef)
        if z_exact_zero:
            dz1 = fdo.random_grid_delta(Lz/2, Nz//2, random_coef)
            dz2 = fdo.random_grid_delta(Lz/2, Nz//2, random_coef)
            dz = np.hstack((dz1, dz2))
        else:
            dz = fdo.random_grid_delta(Lz, Nz, random_coef)
    gridr = fdo.Grid(num_cells=Nr, delta=dr, start=0)
    gridz = fdo.Grid(num_cells=Nz, delta=dz, start=-Lz/2)
    geom = fdo.GeometryCylindrical((gridr, gridz), ('r', 'z'), nls=(1,1), nus=(1,1))
    geoms.append(geom)
    phi = fdo.Field('phi', ncells, stags=(0,0), nls=(1,0), nus=(0,0))
    phis.append(phi)
    phi.bc.record('border')
    phi.bc[:,0] = 0 # ':' is equivalent to '0:end'
    phi.bc[:,end] = 0
    phi.bc[end,1:end-1] = 0 # phi.bc[end-1,1:end-1]
    phi.bc.record('axis')
    phi.bc[-1,:] = phi.bc[1,:] # symmetry around the axis
    phi.bc.freeze()
    rho =  fdo.Field('rho', ncells, stags=(0,0), nls=(0,-1), nus=(-1,-1))
    rhos.append(rho)
    d2u = gridr.r_n**2 + gridz.r_n**2
    rho.setv = np.exp(-d2u/2/rho_radius**2)
    phi_solver = fdo.solver(phi, -geom.laplacian(phi.symb))
    phi_solver.solve_full(phi, rho)
    phi_solvers.append(phi_solver)

#%% 2D plots
for uniform in [0,1]:
    if True:
        fig = UpdateablePcolor(100*(uniform+1))
        fig.plot(geoms[uniform], np.log10(phis[uniform] + 1e-8))
    else:
        re = geoms[uniform].grids[0].r_c[-half:end+half].flatten()
        ze = geoms[uniform].grids[1].r_c[-half:end+half].flatten()
        plt.figure(100*(uniform+1))
        plt.clf()
        plt.pcolor(re, ze, np.log10(phis[uniform][:,:].T + 1e-8))
        plt.gca().set_aspect('equal')
        plt.colorbar()

#%% On the horizontal slice trhough the middle
plt.figure(10)
plt.clf()
rarrs = [geom.grids[0].r_n[:].flatten() for geom in geoms]
zarrs = [geom.grids[1].r_n[:].flatten() for geom in geoms]
# Uniform
plt.plot(rarrs[0],phis[0][:,100],'.-')
# Interpolate
z_interp = np.zeros((Nr+1,))
r_interp = rarrs[1]
print('z=0 in nonuniform grid is at', geoms[1].dindex(0, 1))
phinu_interp = geoms[1].interpolate(phis[1], (r_interp, z_interp))
plt.plot(r_interp, phinu_interp, '.-')

#%% On the axis
plt.figure(11)
plt.clf()
plt.plot(zarrs[0], phis[0][0,:],'.-')
plt.plot(zarrs[1], phis[1][0,:],'.-')



