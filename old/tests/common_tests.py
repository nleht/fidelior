#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 16:58:41 2020

This file is part of TRUMP package. It contains testing some general-purpose utilities contained in file common.py

@author: nleht
"""

import numpy as np
import fidelior as fdo
from fidelior.common import make_indented_printable, customize, decustomize, register_customization_for

# Create test classes
class R: pass
make_indented_printable(R)
class A(R): pass
class B(R): pass
class C(R): pass
class D(R): pass
class E(R): pass

#%% Create multiplication operation for class A with results in B,C,D,E
import numbers
def Amul(t1,t2):
    if isinstance(t2,numbers.Number): return B()
    else: return C()
A.__mul__ = Amul
def Armul(t1,t2):
    if isinstance(t2,numbers.Number): return D()
    else: return E()
A.__rmul__ = Armul
register_customization_for(A)

#%% Demo for numbers
a = A()
print('2*a =',2*a)
print('a*2 =',a*2)

#%% Test these classes
vo = np.arange(1,6,dtype=np.float)
print('vo is a',vo.__class__.__name__)
print('a*vo =',a*vo) # gives C()
print('vo*a =',vo*a) # gives an array of D()'s
print('vo[0]*a =',vo[0]*a)
print('c(vo[0])*a =',customize(vo[0])*a)
v=customize(vo)
print('v is a',v.__class__.__name__)
print('a*v =',a*v) # gives C()
print('v*a =',v*a) # gives E(), not array of D()'s as with NumPy ndarray
   
#%% All normal operations should still work
print('v*v =',repr(v*v))

#%% Back to normal
print('v =',repr(v))
print('vo =',repr(vo),'=',repr(decustomize(v)))

#%% Acting on classes supporting subscripting fails for certain types
class F(R): pass
class Indexable(R):
    def __init__(self,l=1):
        self.l=l
    def __getitem__(self,k):
        if k<self.l and k>=-self.l:
            return self
        else:
            raise IndexError
    def __len__(self):
        return self.l
Indexable.__mul__ = Amul
Indexable.__rmul__ = Armul
register_customization_for(Indexable)

f=Indexable()
np_number = vo[0]
print(fdo.co.red('Weird result:'))
print('vo[0]*f =',np_number*f)
# - fails because vo[0] implements its own __mul__
print('f*vo[0] =',f*np_number)
print('vo[0] class is',np_number.__class__)
print('float(vo[0])*f =',np_number.item()*f)
