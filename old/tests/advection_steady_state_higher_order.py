#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 11:01:09 2020

Steady-state advection, using higher-order schemes.

Watch out, there must be a gauge condition along each streamline, otherwise there is a possibility
of the matrix to be singular!!!

See also: advection_steady_state_first_order.py

@author: nle003
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
import fidelior as fdo
from fidelior import end, half, span
from fidelior.plotting import UpdateablePcolor, ea_plot
import fidelior.automatic_schemes as AS
fdo.set_global_debug_level(0) # 0 is dangerous!
fdo.set_sparse(True) # was by default, but just in case
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()
    
def make_figure(geom, ea, fignum=None):
    fig = UpdateablePcolor(fignum=None)
    fig.plot(geom, ea)

#%% Setup the grid and velocity field: CCW for r<R1, CW for R1<r<R2, w=vmax/R2
# The unknown function f is calculated on a Nx x Ny array, at x,y coordinates
Lx = 1
Ly = 1
#Lx = 200; Ly=200; dx=1; dy=1
Nx = 500
Ny = 500
dx = Lx/Nx
dy = Ly/Ny
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2)
geom = fdo.GeometryCartesian((gridx,gridy), nls=(5,5), nus=(5,5))
x = geom.grids[0].r_n
y = geom.grids[1].r_n
xc = geom.grids[0].r_c
yc = geom.grids[1].r_c
# xpcolor = xc[-half:end+half].flatten()
# ypcolor = yc[-half:end+half].flatten()
# xec = x[:].flatten()
# yec = y[:].flatten()

vmax = 0.5
R = np.sqrt(Lx**2+Ly**2)/2
vangle=vmax/R # angular velocity

ncells = geom.ncells
psi = fdo.ExtendedArray(ncells, stags=(1,1), nls=(5,5), nus=(5,5))
psi.arr = np.full(psi.nts, fill_value=np.nan)
rc = np.sqrt(xc**2+yc**2)
psi.setv = -rc**2/2*vangle # at points (i-1/2,j-1/2)
psi = -rc**2/2*vangle
vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)
# Check if the divergence is zero at points (i,j)
print('max div v =', np.max(np.abs(geom.divergence([vx, vy]))), flush=True)
dt = 1/(np.max(vx)/dx+np.max(vy)/dy)

#%%
#vx = vx + 0.5*vmax

#%% Source
source = fdo.Field('source', ncells, stags=(0,0), nls=(-1,-1), nus=(-1,-1))
r_source = Lx/20
ip = (x+Lx/4)**2+y**2 < r_source**2
im = (x-Lx/4)**2+y**2 < r_source**2
source.setv = 0
source[ip] = 1
source[im] = -1
make_figure(geom, source)

####################################################################################################
#%% Third-order solution. Unfortunately, seems to be unstable 
from fidelior import automatic_schemes as AS
stencil = AS.odd_order_stencil(2, 2)
order = 3
#stencil, _, _ = AS._stencil_union(AS.odd_left_stencil(2, 2), AS.odd_right_stencil(2, 2))
scheme = AS.most_stable_scheme(geom, 3)
ORD3 = AS.all_directions_scheme(scheme)
d = AS.get_boundary_thickness(ORD3)-1
dens3o = fdo.Field('dens3o', ncells, stags=(0,0), nls=(d,d), nus=(d,d))

#%%
def basic_scheme(geom, order):
    "Slightly less stable, but should be OK for sum(vi)<~1"
    if order%2==1:
        n = (order+1)//2
        stencil = AS.odd_order_stencil(geom.ndim, n)
        scheme = AS.interpolating_scheme(geom, 2*n-1, stencil)
    else:
        n = order//2
        stencil = AS.even_order_stencil(geom.ndim, n)
        scheme = AS.interpolating_scheme(geom, 2*n, stencil)
    return AS.all_directions_scheme(scheme)

def stable_scheme(geom, order):
    "For vi<1, except order=1"
    return AS.all_directions_scheme(AS.most_stable_scheme(geom, order))

####################################################################################################
#%% Third-order solution. Unfortunately, seems to be unstable 
SCHEME = stable_scheme(geom, 3) # Main thing is here
d = AS.get_boundary_thickness(SCHEME)-1
dens = fdo.Field('dens', ncells, stags=(0,0), nls=(d,d), nus=(d,d))
tt1 = time.time()
dens.bc[-d:0,-d:end+d]=0
dens.bc[end:end+d,-d:end+d]=0
dens.bc[1:end-1,-d:0]=0
dens.bc[1:end-1,end:end+d] = 0
# Gauge
use_gauge = True
if use_gauge:
    dens.bc[Nx//2,Ny//2:end-1] = 0
tt2 = time.time()
dens.bc.freeze() # takes pretty long
tt3 = time.time()
print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)

#%% The previous point
#vxn = trump.n_upc(vx, vx, 0)
#vyn = trump.n_upc(vy, vy, 1)
# The exact position of the previous point
dt = min(dx/np.max(vx), dy/np.max(vy))/2
if False:
    dt = fdo.co.decustomize(dt)
if False:
    # Exact solution
    r = np.sqrt(x**2+y**2)
    th = np.arctan2(y, x)
    vxndt = r*(np.cos(th)-np.cos(th-vangle*dt))
    vyndt = r*(np.sin(th)-np.sin(th-vangle*dt))
else:
    # Not exact, but works for any vx, vy
    vxc = fdo.n_mean(vx, 0)
    vyc = fdo.n_mean(vy, 1)
    vxn = SCHEME.interpolate(vxc, (-vxc*dt, -vyc*dt))
    vyn = SCHEME.interpolate(vyc, (-vxc*dt, -vyc*dt))
    vxndt = (vxn + vxc)*dt/2
    vyndt = (vyn + vyc)*dt/2

#%% Solution
def smear(n):
    return fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(n,0),0),1),1)

def Oper(n):
    return (n - SCHEME.interpolate(n, (-vxndt, -vyndt)))/dt

dens_solver = fdo.solver(dens, Oper(dens.symb), ea_skip=dens.dep.view[1:end-1,1:end-1])
dens_solver.solve_full(dens, source)
make_figure(geom, dens)



#%% One-dimensional advection
# L = 2
# N = 16
# grid1d = fdo.Grid(num_cells=N, delta=L/N, start=-L/2)
# geom1d = fdo.GeometryCartesian((grid1d,), nls=(3,), nus=(3,))
# ncells1d = geom1d.ncells
# x1d = geom1d.grids[0].r_n
# xc1d = geom1d.grids[0].r_c


# dens1d = fdo.Field('dens1d', ncells1d, stags=(0,), nls=(0,), nus=(0,))
# #dens1d.bc[end]=dens1d.bc[0]
# # Gauge
# dens1d.bc[0] = 0
# dens1d.bc[end] = 0 # This value is getting ignored in the equations!
# dens1d.bc.freeze()

# v1d = fdo.ExtendedArray(ncells1d, stags=(1,), nls=(1,), nus=(1,))
# v1d.arr = np.ones(v1d.nts, dtype=np.double)

# def Oper1d(n):
#     return geom1d.divergence((v1d*fdo.c_upc(n, v1d),))

# #%%
# r_source = 0.1
# source1d = fdo.Field('source1d', ncells1d, stags=(0,), nls=(-1,), nus=(-1,))
# source1d.setv = 0
# source1d[np.abs(x1d+0.5)<r_source] = 1
# source1d[np.abs(x1d-0.5)<r_source] = -1

# # def Oper1d(n):
# #     nav = n.like(stags=(1,), nls=(0,), nus=(1,)) # copy, with different stags, nls, nus
# #     nav[half:end-half] = fdo.c_mean(n)[half:end-half]
# #     nav[end+half] = (n[0] + n[end])/2
# #     return geom1d.divergence((nav,))

# #%%
# #skip = dens1d.dep.view[0:end]
# oper = Oper1d(dens1d.symb)
# dens1d_solver = fdo.solver(dens1d, Oper1d(dens1d.symb))
# dens1d_solver.solve_full(dens1d, source1d)

