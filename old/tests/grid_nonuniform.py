#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 01:01:32 2020

@author: nleht
"""

import numpy as np
import fidelior as fdo
from fidelior import half, end
fdo.set_global_debug_level(5)
#from fidelior import extended_array
fdo.numpy_override.enter()


# def random_grid_delta(N, L, coef):
#     r"""x = randomize_Grid(uniform_grid, nl, nu)
#     A randomly-spaced grid"""
#     assert coef>=0 and coef<=1
#     d0 = coef*(2*np.random.rand(N)-1)+1
#     return d0*L/np.sum(d0)

nonuniform = False
d = fdo.random_grid_delta(5,1,0.1)
if nonuniform:
    gridx = fdo.Grid(len(d),delta=d,start=0,periodic=True)
    gridy = fdo.Grid(len(d),delta=d,start=0,periodic=True)
else:
    gridx = fdo.Grid(len(d),delta=1,start=0)
    gridy = fdo.Grid(len(d),delta=1,start=0)
gridx._arr(0,2,2)
gridx._arr(1,2,2)
geom = fdo.Geometry((gridx, gridy),nls=(2,4),nus=(3,3))
#gridx.add_Geometry(geom, 0, 2, 3)
#gridy.add_Geometry(geom, 1, 4, 3)
print(gridx.r_n)
print(gridy.r_n)

#%%
ncells = (gridx.num_cells, gridy.num_cells)
a = fdo.ExtendedArray(ncells, (0,0), (1,0), (1,2))
a.arr = np.random.randn(*a.nts)

#%%
xb = gridx.r_n.broadcast(ea=a)
yb = gridy.r_n.broadcast(ea=a)

#%% Another way to do it
xb_ = gridx.r_n.broadcast(stags=a.stags, nls=a.nls, nus=a.nus)
yb_ = gridy.r_n.broadcast(ea=gridx.r_n)

#%% Check it
print('error =', np.max(np.abs(xb_-xb)))

#%%
i = fdo.ea.intersection_span(a,gridx.r_n)

#%%
tmp1 = a + gridx.r_n

#%%
tmp2 = a + gridy.r_n

xy = gridx.r_n + gridy.r_n

#%%

a + a

#%% Broadcasting for symbolic arrays
b = fdo.Field('b', ncells, (0,0), (1,0), (1,2))

#%%
b + a

#%%
b.symb + a

#%%
b_oper = b.symb + gridx.r_n

#%%
def apply_operator(oper, f):
    tmp, err = oper.arr.action(f.arr.flat)
    g = f.copy()
    g.arr = tmp.reshape(f.arr.shape)
    return g, err

res, err = apply_operator(b_oper, a)
print((res-a).arr)