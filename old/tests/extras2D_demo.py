#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 11 14:58:27 2020

Like in "solver_demo2_poisson2d.py", we solve a discretized Poisson equation Δ(φ)=-ρ.
This time, we do it both in cartesian and cylindrical (set cyl=True) coordinates,
and also demonstrate some convenient functions in "extras2D" package.

In particular, the differential operator "divergence" is implemented there
for cylindrical coordinates, so we do not have to warry about re-implementing it.

@author: nle003
"""

import time
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import fidelior as fdo
from fidelior import end
from fidelior.plotting import UpdateablePcolor
fdo.set_sparse(True)
fdo.set_global_debug_level(1)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

cyl = True

def make_figure(fignum ,geom, f, title=''):
    plt.close(fignum)
    fig = UpdateablePcolor(fignum)
    fig.plot(geom, f, title=title)
    coors = 'rz' if isinstance(geom, fdo.geometry_cylindrical) else 'xy' # same as "cyl"
    fig.ax.set_xlabel(coors[0]); fig.ax.set_ylabel(coors[1])

#%%
#fdo.SET_SPARSE(True) # use sparse matrices for operators, this is default
N1=100
N2=2*N1
ncells = (N1, N2)
grid1 = fdo.Grid(num_cells=N1,delta=0.5/N1,start=0.0) # x or r, depending on "cyl"
grid2 = fdo.Grid(num_cells=N2,delta=1/N2,start=0.0) # y or z, depending on "cyl"
if cyl:
    geom = fdo.GeometryCylindrical((grid1,grid2),('r','z'),nls=(1,1),nus=(1,1))
else:
    geom = fdo.GeometryCartesian((grid1,grid2),nls=(1,1),nus=(1,1))
rho = fdo.Field('ρ',ncells,stags=(0,0),nls=(0,-1),nus=(-1,-1))
# The charge density ρ does not need to be defined at the boundary, so the
# negative number of ghost cells is specified (we cut off one layer)
print('Setting ρ=1 in the domain [',grid1.start,'--',grid1.stop,'x',\
    grid2.start,'--',grid2.stop,']')
rho[0:end-1,1:end-1] = 1
# note that ρ[0,...] or ρ[end,...] whould give an error because the boundary is cut off.
# TRUMP uses MATLAB-style indexing, not Python-style! In particular, the upper
# boundary ('stop' of the slice) truly _is_ the last element, not the element after the last
# as it would be in Python indexing. Also, negative indices in ExtendedArrays
# mean exactly what they mean - ghost cells below the lower boundary.
phi= fdo.Field('φ',ncells,stags=(0,0),nls=(1,0),nus=(0,0))
print('Setting φ=0 on the right edge, bottom and top')
phi.bc[:,end] = 0 # ':' is equivalent to '0:end'
phi.bc[-1,:] = phi.bc[1,:] # symmetry around the axis
# -- We cannot use φ.bc[0,:] = 0 here because it would conflict/redefine the previous BC
phi.bc[end,1:end-1] = 0
print('Setting φ=0 on the lower edge')
phi.bc.record('lower edge',is_variable=True)
phi.bc[:,0] = 0
print('Checking BC .. ',end='',flush=True)
t0 = time.time()
phi.bc.freeze() # This is necessary - checks BC consistency and prepares BC for solvers
print('done! t =',time.time()-t0,flush=True)
phi_solver = fdo.solver(phi, -geom.laplacian(phi.symb)) # Poisson equation with a given RHS = -Δ(φ)

#%% Plot #1
print('Solving Δφ=-ρ with φ=0 on the left edge .. ',end='',flush=True)
t0 = time.time()
phi_solver.solve_full(phi,rho) # BC are automatically included
print('done! t =',time.time()-t0,flush=True)
print('Error =',np.max(np.abs(geom.laplacian(phi)+rho).arr)) # Out: 5.00155472594e-13
make_figure(1+cyl*2, geom, phi, title=r'$\phi$ for $\rho=1$ and $\phi=0$ at the boundary')
#plt.savefig('fdo_fig_1.pdf')

#%% Plot #2
print('Updating the BC to φ=1 on the lower edge')
phi.bc.update('lower edge')
phi.bc[:,0] = 1
phi.bc.end_update() # optional
print('Solving Δφ=-ρ with φ=1 on the lower edge .. ',end='',flush=True)
t0 = time.time()
phi_solver.solve_full(phi,rho) # the solver already knows about the new BC
print('done! t =',time.time()-t0,flush=True)
print('Error =',np.max(np.abs(geom.laplacian(phi)+rho).arr)) # Out: 4.15134593368e-12
make_figure(2+cyl*2, geom, phi, title=r'$\phi$ for $\rho=1$, $\phi=1$ at lower edge')

print('MPL is interactive =',mpl.is_interactive())

if not mpl.is_interactive():
    plt.show()
