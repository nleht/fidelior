#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 20:50:27 2020

@author: nle003
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 14:58:27 2016

We demonstrate features of TRUMP by solving a discretized Poisson equation

    Δ(φ)=-ρ

for φ with given ρ, in 3D in cartesian coordinates.

Direction references: x is to the right, y is into rear, z is up.

While doing this, we abuse the feature of Python3 which allows Greek characters
to be used in variable names.

@author: nle003
"""

import time
import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib as mpl
import matplotlib.pyplot as plt
#from matplotlib import cm
import fidelior as fdo
from fidelior import co, end, half, field, solver, c_diff, n_diff
fdo.set_sparse(True) # use sparse matrices for operators, this is default
fdo.set_global_debug_level(0)
fdo.numpy_override.enter()
save_fig = False # set to True if you want PDF output of the figures
use_geometry_laplacian = True

def Δ(f, geom):
    "Laplacian"
    # c_dif acts on stag=0 ExtendedArray's ('central' difference), the result
    # is a stag=1 array. The opposite is for n_dif ('node' difference).
    if use_geometry_laplacian:
        # The built-in way to do it:
        return geom.laplacian(f)
    else:
        g = geom.grids
        return n_diff(c_diff(f,0),0)/g[0].delta**2 + n_diff(c_diff(f,1),1)/g[1].delta**2 +\
                n_diff(c_diff(f,2),2)/g[2].delta**2

# Set up
Nx = 25; Ny = 25; Nz = 25;
ncells = (Nx, Ny, Nz)
gridx = fdo.Grid(num_cells=Nx, delta=1/Nx, start=0.0)
gridy = fdo.Grid(num_cells=Ny, delta=1/Ny, start=0.0)
gridz = fdo.Grid(num_cells=Nz, delta=1/Nz, start=0.0)
# Create geometry with some wiggle room for the ghost cooridinate axcess
geom = fdo.GeometryCartesian((gridx, gridy, gridz), nls=(2,2,2), nus=(2,2,2))
# 'stags' is the staggering flag of the grid. '0' means the values are defined
# at nodes, while '1' would mean that the values are defined at centers of cells.
# 'nls' and 'nus' is the number of ghost cell layers on the bottom (lower) and
# the top (upper) boundaries of the array.
ρ = Field('ρ', ncells, stags=(0,0,0), nls=(-1,-1,-1), nus=(-1,-1,-1))
# The charge density ρ does not need to be defined at the boundary, so the
# negative number of ghost cells is specified (we cut off one layer)
print('Setting ρ=1 in the domain [(', gridx.start, '--', gridx.stop, ') x (',\
    gridy.start, '--', gridy.stop, ') x (', gridz.start, '--', gridz.stop, ')]')
ρ[1:end-1,1:end-1,1:end-1] = 1
# note that ρ[0,...] or ρ[end,...] whould give an error because the boundary is cut off.
# TRUMP uses MATLAB-style indexing, not Python-style! In particular, the upper
# boundary ('stop' of the slice) truly _is_ the last element, not the element after the last
# as it would be in Python indexing. Also, negative indices in ExtendedArrays
# mean exactly what they mean - ghost cells below the lower boundary.
φ = Field('φ', ncells, stags=(0,0,0), nls=(0,0,0), nus=(0,0,0))
# Variables for plotting
# xe = φ.pcolor_x(0); ye = φ.pcolor_x(1); ze = φ.pcolor_x(2)
# x = φ.xe(0); y = φ.xe(1); z = φ.xe(2)
xe = gridx.r_c[-half:end+half].flatten()
ye = gridy.r_c[-half:end+half].flatten()
ze = gridz.r_c[-half:end+half].flatten()
x = gridx.r_n[0:end].flatten()
y = gridy.r_n[0:end].flatten()
z = gridz.r_n[0:end].flatten()
X, Y = fdo.co.ndgrid(x, y)
print('Setting φ=0 on the right edge, bottom, top, front and back')
φ.bc[0,:,:] = φ.bc[1,:,:] # Neumann B.C. on the left
φ.bc[end,:,:] = 0 # Dirichlet B.C. on the right
φ.bc[1:end-1,0,:] = 0 # front
# -- We cannot use φ.bc[:,0,:] = 0 here because it would conflict/redefine the previous BC
φ.bc[1:end-1,end,:] = 0 # back
φ.bc[1:end-1,1:end-1,0] = 0 # bottom
φ.bc[1:end-1,1:end-1,end] = 0 # top

t0 = time.time()
φ.bc.freeze() # This is necessary - checks BC consistency and prepares BC for solvers
print(fdo.co.info('Freezing time:'),time.time()-t0,flush=True)
t0 = time.time()
Δφ = Δ(φ.symb, geom)
print(fdo.co.info('Symbolic operator time:'),time.time()-t0,flush=True)
t0 = time.time()
φ_solver = solver(φ, -Δφ) # Poisson equation with a given RHS = -Δ(φ)
print(fdo.co.info('Solver setup time:'),time.time()-t0,flush=True)
print('Solving Δφ=-ρ with φ=0 on the left edge ...')
t0 = time.time()
φ_solver.solve_full(φ,ρ) # BC are automatically included
print(fdo.co.info('Solving time:'),time.time()-t0,flush=True)
print('Error =',np.max(np.abs(Δ(φ, geom)+ρ).arr)) # Out: 5.00155472594e-13

# Exit the context
fdo.numpy_override.complete_exit()

####################################################################################################
#%%
# Plot solution #1
# xe, ye are just for 'pcolor', these are a not the points where the function is
# defined. To get the grid points, use φ.x(0) and φ.x(1)
# print('Please close the figure window to continue') # if not using IPython
fig1 = plt.figure(1,figsize=(6,5))
fig1.clear()
ax1 = fig1.gca(projection='3d')
vmin1 = np.min(φ.arr)
vmax1 = np.max(φ.arr)
levels1 = np.linspace(vmin1,vmax1,11)
for iz in range(Nz+1):
    cset1 = ax1.contour(X, Y, φ[:,:,iz], zdir='z', offset=z[iz],
                      vmin=vmin1, vmax=vmax1, levels=levels1,
                      cmap=mpl.cm.coolwarm, alpha=0.3)
ax1.set_xlabel('X')
ax1.set_xlim(0, 1)
ax1.set_ylabel('Y')
ax1.set_ylim(0, 1)
ax1.set_zlabel('Z')
ax1.set_zlim(0, 1)

if mpl.__version__ <= '3.0.3':
    ax1.set_aspect('equal') # does not work with newer matplotlib
# See https://stackoverflow.com/a/31364297 for a possible solution
ax1.set_title(r'$\phi$ for $\rho=1$ and $\partial\phi/\partial n=0$ on the left,' + '\n' + 
              r'$\phi=0$ at other faces')
if save_fig:
    fig1.savefig('poisson3d_fig_1.pdf')
