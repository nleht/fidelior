#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 12:24:56 2020

Solution of advective equation in 1D:

df/dt + div(v*f) = 0

using various linear schemes.

f in div(v*f) is represented as a "face value" of f

Literature:

Leonard [1979] - 3rd order upwind scheme in 1D (called QUICKEST)
Leonard [1991] - the flux-limiting algorithm in 1D (called ULTIMATE)

Addition on Nov 9, 2020: added the second-order simplified ENO scheme.

@author: nleht
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
import matplotlib.pyplot as plt

import fidelior as fdo
from fidelior import co, ea, end, half, span, \
    c_diff, n_diff, c_mean, n_mean, c_ups, n_ups, c_upc, n_upc, c_interp_ups, c_interp_upc
    
from fidelior.extended_arrays import _up1
from fidelior.plotting import ea_plot

fdo.set_global_debug_level(3) # 0 is dangerous!
fdo.set_sparse(True)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

# Instead of doing the context globally, just pull out a couple of functions
# with fdo.numpy_override:
#     np_maximum = np.maximum
#     np_minimum = np.minimum
#     np_sign = np.sign
#     np_abs = np.abs
#     np_cumsum = np.cumsum

#from fdo.extras1D import arrmin, arrmax, arrmin3, arrmax3
def maximum3(x,y,z):
    return np.maximum(x,np.maximum(y,z))

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

#%%
class AutoScheme:
    """Automatically generated scheme of N-th order, with stencil shifted by M from origin.
    Arbitrary stencils are also allowed."""
    def __init__(self, x, N=None, M=None, mask=None, stencil=None):
        if stencil is None:
            self.name = 'auto, N='+str(N)+', M='+str(M)
            self.stencil = np.array(range(M,M+N+1))
            if mask is not None:
                self.stencil = self.stencil[mask]
        else:
            self.stencil = np.unique(stencil) # also sorts
            self.name='auto, S='+str(self.stencil)
        self.order = len(self.stencil)-1
        self.x = x # the non-uniform grid
        self.polys = [self.make_poly(j) for j in self.stencil]
    # def find_poly_coef(self, k):
    #     """Find the matrix U that converts array :math:`\{ u_{i+k}, k \in S\}`
    #     into poly coefs.
        
    #     .. math:: U  (1, (x_{i+k}-x_i), (..)^2, (..)^3, ..)^T = (u(i+k1), u(i+k2), ... )^T
        
    #     The value of function u(x) calculated at u_k is
    #     U* (1, (x-x_k), (x-x_k)^2, ..) """
    #     dxk = self.x.shifted(k) - self.x
    #     for n in range(self.order):
    #         dxk**n
    #     #self.polys = [self.make_poly(j) for j in self.stencil]
    def make_poly(self, k):
        xk = self.x.shifted((k,)) # just for readability, no optimization
        def func(x):
            "Normalized so that = 1 at k"
            #p = None
            p = 1
            xx = self.x + x
            for i in self.stencil:
                if i != k:
                    xi = self.x.shifted((i,))
                    p = p*(xx-xi)/(xk-xi)
                    #if p is None:
                    #    p = (xx-xi)/(xk-xi)
                    #else:
                    #    p *= (xx-xi)/(xk-xi)
            return p
        return func
    def poly(self,k,x):
        return self.polys[np.searchsorted(self.stencil,k)](x)
    def next_value(self,f,c):
        # only scalar c
        fnext = 0
        for k in self.stencil:
            fnext = self.poly(k,-c)*f.shifted((k,)) + fnext # Try f += ... -- gives different size
        return fnext
    def get_coef(self,c):
        # Not used for calculations, only demo
        return np.array([self.poly(k, -c) for k in self.stencil])
    
#%%
def advection_next_value(geom, f, c, alg):
    """c has dimension of space, i.e. are actual velocity times dt.
    alg consists a 4-letter algorithm category + space + algorithm name."""
    global AUTO_SCHEMES
    assert isinstance(alg,str) and len(alg)>5
    # Extract the x
    #x = AUTO_SCHEMES['CIR'].x
    grid = geom.grids[0]
    x = grid.r_n
    xc = grid.r_c
    dx = grid.dr_c
    dx0 = grid.dr_n
    if alg=='ENO2 LW':
        # The essentially non-oscillatory scheme of Chi-Wang Shu, 2nd order
        V0 = 0
        V1 = f*dx0
        V0V1 = V1/dx0 # same as f
        V2 = V1 + V1.shifted((1,))
        V2V1 = V0V1.shifted((1,))
        V0V1V2 = (V2V1-V0V1)/(dx0 + dx0.shifted((1,)))
        # V0V1V2 = (f.shifted((1,))-f)/(dx0 + dx0.shifted((1,)))
        def V_poly(x):
            xm = xc.shifted((-half,))
            xp = xc.shifted((+half,))
            return f*(x-xm) + (f.shifted((1,))-f)/(dx0+dx0.shifted((1,)))*(x-xm)*(x-xp)
        upper = V_poly(xc.shifted((+half,))-c)-V_poly(xc.shifted((-half,)))
        #upper = V_poly(xc.shifted((+half,))-c)
        lower = (V_poly(xc.shifted((+half,)))-V_poly(xc.shifted((+half,))-c)).shifted((-1,))
        return (upper+lower)/dx0
    elif alg=='ENO2 WB':
        def V_poly_orig(x):
            xm = xc.shifted((-half,))
            xp = xc.shifted((+half,))
            return f*(x-xm) + (f.shifted((1,))-f)/(dx0+dx0.shifted((1,)))*(x-xm)*(x-xp)
        def V_poly(x):
            xm = xc.shifted((-1-half,))
            xp = xc.shifted((-half,))
            return f.shifted((-1,))*(x-xm) + (f-f.shifted((-1,)))/(dx0+dx0.shifted((-1,)))*(x-xm)*(x-xp)
            #return V_poly_orig(x).shifted((-1,))
        upper = V_poly(xc.shifted((+half,))-c)-V_poly(xc.shifted((-half,)))
        lower = (V_poly(xc.shifted((+half,)))-V_poly(xc.shifted((+half,))-c)).shifted((-1,))
        return (upper+lower)/dx0
    elif alg=='ENO2 simple':
        # Choose the best by choosing the minimal of V0V1V2
        # Amazingly enough, seems to work!
        V0V1V2_LW = (f.shifted((1,))-f)/(dx0 + dx0.shifted((1,)))
        V0V1V2_WB = (f-f.shifted((-1,)))/(dx0+dx0.shifted((-1,)))
        choose_lw = (np.abs(V0V1V2_LW)<np.abs(V0V1V2_WB))
        def V_poly_lw(x):
            xm = xc.shifted((-half,))
            xp = xc.shifted((+half,))
            return f*(x-xm) + (f.shifted((1,))-f)/(dx0+dx0.shifted((1,)))*(x-xm)*(x-xp)
        def V_poly_wb(x):
            xm = xc.shifted((-1-half,))
            xp = xc.shifted((-half,))
            return f.shifted((-1,))*(x-xm) + (f-f.shifted((-1,)))/(dx0+dx0.shifted((-1,)))*(x-xm)*(x-xp)
            #return V_poly_orig(x).shifted((-1,))
        def val(x, V_poly):
            upper = V_poly(xc.shifted((+half,))-c)-V_poly(xc.shifted((-half,)))
            lower = (V_poly(xc.shifted((+half,)))-V_poly(xc.shifted((+half,))-c)).shifted((-1,))
            return (upper+lower)/dx0
        val_lw = val(x, V_poly_lw)
        val_wb = val(x, V_poly_wb)
        val = val_lw*choose_lw + val_wb*(~choose_lw)
        return val
    elif alg[:5]=='AUTO ':
        return AUTO_SCHEMES[alg[5:]].next_value(f, c)
    elif alg=='COMB Fromm':
        return (AUTO_SCHEMES['Lax-Wendroff'].next_value(f, c) +
                AUTO_SCHEMES['Warming-Beam'].next_value(f, c))/2
        #elif alg in ['vanLeer','superbee','Sweby1.5']:
    elif alg[:5]=='GUES ':
        # Try to guess what flux_hi - flux_lo is
        # See https://en.wikipedia.org/wiki/Flux_limiter
        fnext_CIR = AUTO_SCHEMES['CIR'].next_value(f, c)
        df2 = AUTO_SCHEMES['Lax-Wendroff'].next_value(f, c) - fnext_CIR
        D = c_diff(x)
        dfdx = c_diff(f)/D
        D0 = n_mean(D)
        dflux2 = np.cumsum(df2*D0) # The flux delta, but with an arbitrary constant bias
        dflux2 -= np.median(dflux2.arr) # we guess the constant bias and remove it.
        dfdxu = n_ups(dfdx,-1.)
        dfdxd = n_ups(dfdx,1.)
        # r = dfd/dfu, do it carefully:
        r = dfdxd.copy_arr()
        ii = (dfdxu != 0)
        r[ii] /= dfdxu[ii]
        r[~ii] = np.inf # infinity
        if alg[5:]=='vanLeer':
            # Same as van Leer (1974), according to Sweby (1984)
            # It's a kind of magick!
            denom = np.abs(dfdxu)+np.abs(dfdxd)
            phi = np.sign(dfdxu)*dfdxd + np.abs(dfdxd)
            ii = (denom != 0)
            phi[ii] /= denom[ii]
        elif alg[5:]=='superbee':
            phi = maximum3(np.minimum(2*r,1),np.minimum(r,2),0)
        elif alg[5:]=='Sweby1.5':
            beta = 1.5
            phi = maximum3(np.minimum(r*beta,1),np.minimum(r,beta),0)
        elif alg[5:]=='LW-test':
            phi = f.copy()
            phi.alloc(np.double)
            phi.arr[:] = 1
        else:
            raise Exception('Internal error')
        # The first order flux fv1*v corrected
        # by adding second order flux dfv2*v with coefficient 0<phi<1
        # (shifted upwind with function 'c_upc')
        #return fnext_CIR + df2
        return fnext_CIR + n_diff(c_ups(phi, c)*dflux2)/D0
    elif alg[:5]=='DIFF ' or alg[:5]=='DIST ':
        # Home-grown flux limiter
        fnext = AUTO_SCHEMES[alg[5:]].next_value(f, c)
        # It cannot be outside the interval _up1(f) and f
        excess = fnext - np.maximum(_up1(f, c, 0), f)
        # If excess > 0, shift it down, but the point upstream has to be shifted up
        ihi = excess>0
        fnext[ihi]  -= excess[ihi]
        if alg[:5]=='DIST ':
            icommon = ea.common_index(ihi, fnext.shifted((-1,)), excess)
            fnext.shifted((-1,))[icommon] += excess[icommon]
        # Same if is too low, work with the new values already
        excess = fnext - np.minimum(_up1(f, c, 0), f)
        ilo = excess<0
        fnext[ilo]  -= excess[ilo] # increase
        if alg[:5]=='DIST ':
            icommon = ea.common_index(ilo, fnext.shifted((-1,)), excess)
            fnext.shifted((-1,))[icommon] += excess[icommon]            
        return fnext
    else:
        raise ValueError('Unknown alg: '+alg)

################################################################################
#%% Setup
V = 0.4
CFL = 0.9 # Courant–Friedrichs–Lewy condition, must be 0 < CFL=V*dt/dx < 1
num_smear = 0
rand_coef = 0.5
method_set = 5

# Setup the grid
L = 2
N = 300
ncells = (N,)
case = 'random grid'
if case == 'random grid':
    dx = fdo.random_grid_delta(L, N, rand_coef)
elif case == 'growing grid':
    # Growing grid
    dx = fdo.growing_grid_delta(L, N, L/N/100)
    N = len(dx)
elif case == 'wall grid':
    dx = np.hstack(( np.ones((N//2,)), 0.3*np.ones((N//2,)) ))
    dx = dx*L/np.sum(dx)
elif case == 'uniform grid':
    dx = 1/N
else:
    raise NotImplementedError('case not found')
grid = fdo.Grid(N, delta=dx, start=-L/2, periodic=True)
geom = fdo.Geometry((grid,), nls=(5,), nus=(5,))
x = grid.r_n
def waveform(t,f):
    f.setv = 0
    xs = ((x - V*t + L/2) % L) - L/2
    ii = (xs > 0) & (xs < L/4)
    f[ii] = 2*xs
    ii = np.abs(xs+L/4)<L/8
    f[ii] = (1-((xs+L/4)/(L/8))**2)
    f[ii] = np.sqrt(f[ii])

################################################################################
#%% Prepare the figures

AUTO_SCHEMES = {'CIR': AutoScheme(x, N=1,M=-1),
           'Lax-Wendroff': AutoScheme(x, N=2, M=-1),
           'Leonard': AutoScheme(x, N=3, M=-2),
           'Warming-Beam': AutoScheme(x, N=2, M=-2),
           '5th order': AutoScheme(x, N=5, M=-3)}

if method_set==0:
    # Methods of various orders without flux correction
    methods = ['AUTO CIR','AUTO Lax-Wendroff','AUTO Warming-Beam','AUTO Leonard','AUTO 5th order']
elif method_set==1:
    # Second-order methods with flux correction, compared to the third-order
    # DOES NOT WORK FOR rand_coef > 0!!!
    methods = ['GUES vanLeer','GUES superbee','GUES Sweby1.5','DIFF Leonard','AUTO Leonard']
elif method_set==2:
    # Only second-order methods without flux correction
    methods = ['AUTO Lax-Wendroff', 'AUTO Warming-Beam', 'COMB Fromm']
elif method_set==3:
    methods = ['DIFF Lax-Wendroff', 'DIFF Leonard', 'DIST Lax-Wendroff', 'DIST Leonard',
               'AUTO Lax-Wendroff', 'AUTO Leonard']
elif method_set==4:
    # The ENO methods compared to interpolating methods
    methods = ['ENO2 LW', 'AUTO Lax-Wendroff', 'ENO2 WB', 'AUTO Warming-Beam', 'ENO2 simple']
elif method_set==5:
    # The ENO methods only
    methods = ['ENO2 LW', 'ENO2 WB', 'ENO2 simple', 'AUTO Leonard']
else:
    raise Exception('Unknown method set')
nmethods=len(methods)
zmin,zmax = (-0.4,1.4)
fig = plt.figure(100,figsize=(10,5))

#%%#############################################################################
# Initial value
fini = fdo.ExtendedArray(ncells, stags=(0,), nls=(2,), nus=(2,))
fini.arr = np.ndarray(fini.shape)
waveform(0, fini)
def smear(ea,n=0):
    for k in range(n):
        ea.setv = n_mean(c_mean(ea))
    #return ea
smear(fini,num_smear)
fexact = fini.copy_arr()
   
################################################################################
#%% The main cycle
# Initial value

farr=[]
for kmethod, method in enumerate(methods):
    #dd = np.max(np.abs(schemes[method].stencil))
    if method in AUTO_SCHEMES:
        dd = AUTO_SCHEMES[method].order
    else:
        dd = 2 # second order flux-correcting schemes
    f = fdo.Field('f_'+method, ncells, stags=(0,), nls=(dd,), nus=(dd,))
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    f.bc[end:end+dd]=f.bc[0:dd]
    f.bc[-dd:-1]=f.bc[end-dd:end-1]
    f.bc.freeze()
    farr.append(f)
    
for f in farr:
    f.setv = fini

#%%
tot_turns = 10
dt = CFL*np.min(co.decustomize(c_diff(x)[span]/V))
#dt = CFL*np.min(fdo.co.decustomize(c_dif(x_rand).arr)/V)
Nt = np.int(np.ceil(tot_turns*L/(V*dt)))
print('Wait for',dt*Nt*V/L,'turns')
line = [None]*nmethods
t = 0
for kt in range(Nt+1):
    for kmethod, method in enumerate(methods):
        f = farr[kmethod]
        f.setv = advection_next_value(geom, f, V*dt, alg=method)
        f.bc.apply()
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0 or kt==Nt:
        if not plt.fignum_exists(fig.number):
            print('Figure closed, exiting ...')
            sys.exit()
        plt.figure(fig.number)
        plt.clf()
        for kmethod in range(nmethods):
            plt.plot(x[:], farr[kmethod][:],'.-')
        # Plot the exact value
        waveform(t, fexact)
        plt.plot(x[:], fexact[:],'k',alpha=0.3)
        plt.legend(methods)
        plt.grid(True)
        plt.ylim(zmin,zmax)
        plt.xlim(-L/2,L/2)
        plt.title('n = '+str(neat_float(V*t/L)))
        plt.pause(0.1)
        # fig.wait() # progress by clicking mouse in Figure 100

