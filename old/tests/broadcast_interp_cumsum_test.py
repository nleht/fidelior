#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 24 14:06:26 2020

@author: nle003
"""
import numpy as np
import fidelior as fdo
from fidelior import c_diff, n_diff, end, half, span
from fidelior.geometries import random_grid_delta, _integrate_on_indexgrid, _interpolate_on_dindexgrid,\
     _integrate_on_coorgrid
from fidelior.extended_arrays import symbolic_index_to_double
fdo.set_global_debug_level(5)

try:
    from utils import nice_print
    nice_print()
except:
    pass

fdo.print_motto()

Nx = 5
Ny = 4
A = fdo.ExtendedArray((Nx, Ny), (0,0), (1,2), (3, 0))
A.arr = np.random.randn(*A.nts)

B = A.cumsum()
with fdo.numpy_override:
    B1 = A.cumsum()
    assert fdo.ea.same_ea_structure(B1, B)
    print('B1-B =',  np.max(np.abs(B-B1)))

#%%
A1 = n_diff(n_diff(B,0),1)
assert fdo.ea.same_ea_structure(A1, A)
with fdo.numpy_override:
    print('error =', np.max(np.abs(A-A1)))
    
#%% Broadcasting
Lx = 1
Ly = 1
do_uniform = False
if do_uniform:
    deltax = Lx/Nx
    detlay = Ly/Ny
else:
    random_coef = 0
    deltax = random_grid_delta(Lx, Nx, random_coef)
    deltay = random_grid_delta(Ly, Ny, random_coef)
gridx = fdo.Grid(num_cells=Nx, delta=deltax, start=0)
gridy = fdo.Grid(num_cells=Ny, delta=deltay, start=0)
geom = fdo.Geometry((gridx,gridy), nls=(3,3), nus=(3,3))

X = gridx.r_n.broadcast(gridy.r_n)
x = gridx.r_n.copy_arr()
y = gridy.r_n.copy_arr()

try:
    x.setv = y
except Exception as e:
    print(fdo.co.alert('Caught exception:'), fdo.co.error(str(e)))

A1.setv = x
A1.setv = y

#%% tmp = integrate(a, geom, (None, None))
index_grid = ((-half,end+half),(-half,end+half))

tmp = _integrate_on_indexGrid(A, index_grid)

print('error =',tmp[0,0]-np.sum(A[:,:]))

#%%
index_grid = ((-half, 1+half, end+3+half), (-half, half, end+half))
tmp = _integrate_on_indexGrid(A, index_grid)

# Again, we shall interpolate B==cumsum(A)
dindex = np.array([
        [symbolic_index_to_double(el, B, axis) for el in e]
        for axis,e in enumerate(index_grid)])    

#ea = B

tmp1 = B.interpolate(dindex)
tmp2 = np.array([B[i] for i in zip(*index_grid)]) # reference value
print('error =', tmp1-tmp2)

#%% Multi-dimensional indices
dindex = np.array([[.5], [1]])
tmp1 = B.interpolate(dindex)

#%% Multi-dimensional indices
dindex = (np.array([[.5, 1.5]]), np.array([[0, 1]]))
tmp1 = B.interpolate(dindex)

#%%
print(fdo.co.blue('*** On grid ***'))
dindexgrid = ([.5, 1.5], [0])
tmp1 = _interpolate_on_dindexGrid(B, dindexgrid)

#%%
tmp1 = _integrate_on_coorGrid(A, geom, [[0, Lx], [0, Ly]])

#%% The new setv
x_n = gridx.r_n.copy_arr() # to make it assignable
y_n = gridy.r_n.copy_arr()
print(fdo.co.blue('Trying: x_n.setv = A'))
try:
    x_n.setv = A
except Exception as e:
    print(fdo.co.alert('Caught exception:'),fdo.co.error(repr(e)))

print(fdo.co.blue('Trying: x_n.setv = y_n'))
try:
    x_n.setv = y_n
except Exception as e:
    print(fdo.co.alert('Caught exception:'),fdo.co.error(repr(e)))

A.setv = x_n # no problemo
print('A.arr =\n', A.arr)