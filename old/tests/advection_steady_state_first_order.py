#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 16:12:58 2020

Steady-state advection.

Watch out, there must be a gauge condition along each streamline, otherwise there is a possibility
of the matrix to be singular!!!

See also: advection_steady_state_higher_order.py

@author: nle003
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import time
import fidelior as fdo
from fidelior import end, half, span
from fidelior.plotting import UpdateablePcolor, ea_plot
#, n_mean, c_mean, geometry_cartesian, geometry_cylindrical, \
#    c_mean, n_mean, c_diff, n_diff, c_upc, n_upc, minmax, \
#    n_interp_upc, c_interp_upc
fdo.set_global_debug_level(3) # 0 is dangerous!
fdo.set_sparse(True) # was by default, but just in case
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()
    
def make_figure(geom, ea, fignum=None):
    fig = UpdateablePcolor(fignum=None)
    fig.plot(geom, ea)

#%% Setup the grid and velocity field: CCW for r<R1, CW for R1<r<R2, w=vmax/R2
# The unknown function f is calculated on a Nx x Ny array, at x,y coordinates
Lx = 1
Ly = 1
#Lx = 200; Ly=200; dx=1; dy=1
Nx = 200
Ny = 200
dx = Lx/Nx
dy = Ly/Ny
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2)
geom = fdo.GeometryCartesian((gridx,gridy), nls=(3,3), nus=(3,3))
x = geom.grids[0].r_n
y = geom.grids[1].r_n
xc = geom.grids[0].r_c
yc = geom.grids[1].r_c
# xpcolor = xc[-half:end+half].flatten()
# ypcolor = yc[-half:end+half].flatten()
# xec = x[:].flatten()
# yec = y[:].flatten()

vmax = 0.5
R = np.sqrt(Lx**2+Ly**2)/2
vangle=vmax/R # angular velocity

ncells = geom.ncells
psi = fdo.ExtendedArray(ncells, stags=(1,1), nls=(3,3), nus=(3,3))
psi.arr = np.full(psi.nts, fill_value=np.nan)
rc = np.sqrt(xc**2+yc**2)
psi.setv = -rc**2/2*vangle # at points (i-1/2,j-1/2)
psi = -rc**2/2*vangle
vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)
# Check if the divergence is zero at points (i,j)
print('max div v =', np.max(np.abs(geom.divergence([vx, vy]))), flush=True)
dt = 1/(np.max(vx)/dx+np.max(vy)/dy)

#%%
#vx = vx + 0.5*vmax

#%%
dens = fdo.Field('dens', ncells, stags=(0,0), nls=(0,0), nus=(0,0))
tt1 = time.time()
# Be careful with the boundary conditions: set to zero only where vn<0
vxu = fdo.n_upc(vx, vx, 0)
vyu = fdo.n_upc(vy, vy, 1)
dens.bc[0,:]=0
dens.bc[end,:]=0
dens.bc[1:end-1,0]=0
dens.bc[1:end-1,end] = 0
# Gauge
use_gauge = True
if use_gauge:
    dens.bc[Nx//2,Ny//2:end-1] = 0
tt2 = time.time()
dens.bc.freeze() # takes pretty long
tt3 = time.time()
print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)

def Oper(n):
    """Output is ExtendedArray(ncells,stags=(0,0),nls=(d-1,d-1), nus=(d-1,d-1))"""
    return geom.divergence((vx*fdo.c_upc(n,vx,0), vy*fdo.c_upc(n,vy,1)))

#%% Source
source = fdo.Field('source', ncells, stags=(0,0), nls=(-1,-1), nus=(-1,-1))
r_source = Lx/20
ip = (x+Lx/4)**2+y**2 < r_source**2
im = (x-Lx/4)**2+y**2 < r_source**2
source.setv = 0
source[ip] = 1
source[im] = -1
#source.setv = np.exp(-((x-0.5)**2+y**2)/2/r_source**2) - \
#    np.exp(-((x+0.5)**2+y**2)/2/r_source**2)
make_figure(geom, source)

#%%
dens_solver = fdo.solver(dens, Oper(dens.symb), ea_skip=dens.dep.view[1:end-1,1:end-1])
dens_solver.solve_full(dens, source)
make_figure(geom, dens)

M = dens_solver.op_full.M

if Nx*Ny<1000:
    print(np.linalg.det(M.A))
