#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 16 21:01:05 2020

TODO:
    - c_ave(f,0) for cylindrical (!!!)
    - integrate
    - arbitrarily extended coordinate arrays (symmetric around border by default)
    - inheritance from Geometry_nonuniform?
    - maybe a nonuniform grid class?
    - maybe automatic repeat of 1D to 2D in operations? -- in extended_array.py

@author: nleht
"""
#%% Imports
import numpy as np
from . import common as CO
#from . import extended_arrays as EA
from .extended_arrays import ExtendedArray, _diff, _mean, end, half, span, reslice
from .extended_array_operations import c_diff, n_diff, c_mean, n_mean


#%% Grid-generation routines

def random_grid_delta(L, N, coef):
    r"""x = randomize_Grid(uniform_grid, nl, nu)
    A randomly-spaced grid"""
    assert coef>=0 and coef<=1
    d0 = coef*(2*np.random.rand(N)-1)+1
    return d0*L/np.sum(d0)

def growing_grid_delta(L, N, dx, power=0, error=1e-12):
    """Generate a non-uniform grid, with dx0 being the initial dx value, with dx
    growing linearly (power=1) or quadratically (power=2), so that the number of cell is N
    and the total length is L.
    If N is too high to have a growing grid, return a regular grid with interval dx.
    Note that in this case, real N turns out to be smaller.
    power==0 corresponds to an exponential grid."""
    if L<=0:
        return np.array([])
    if round(L/dx)<=N:
        N = np.int(np.round(L/dx))
        if N==0:
            N = 1
        return (L/N)*np.ones((N,))
    if power==0:
        # Exponential grid
        # Determine the geometric progression coefficient C from equation
        # (C^N-1)/(C-1) = L/dx
        # Solve it by iterating
        C = 2 # initial estimate
        err = 1
        while err>error:
            C1 = np.exp(np.log(L/dx*(C-1)+1)/N)
            err = np.abs(C1-C)
            C = C1
            #print(C,err)
        dxi0 = dx*np.exp(np.log(C)*np.arange(N))
        # Return a corrected value so that the sum is correct
        dxi = L*dxi0/np.sum(dxi0)
    else:
        # See https://en.wikipedia.org/wiki/Faulhaber%27s_formula
        if power==1:
            s = N*(N-1)/2
        elif power==2:
            s = N*(N-1)*(2*N-1)/6
        elif power==3:
            s = (N*(N-1)/2)**2
        else:
            raise Exception('Powers > 3 are not implemented / power = '+str(power))
        ddx = (L-N*dx)/s
        assert ddx>0
        dxi = dx + np.arange(N)**power*ddx
    return dxi

#%% Grid class

class GridPoints:
    """A private class used only by grid."""
    def __init__(self, gr):
        self.grid = gr
        self.calculated = False
        self.n = gr.num_cells
    def calculate(self, nl, nu):
        d0 = self.grid.delta
        if self.grid.periodic:
            d = np.hstack((d0[self.n-nl:], d0, d0[:nu]))
        else:
            d = np.hstack((np.flip(d0[:nl]), d0, np.flip(d0[self.n-nu:])))
        self.dx = d
        self.dx0 = (d[1:] + d[:-1])/2
        self.x = np.hstack((0,np.cumsum(d)))
        self.x += self.grid.start - self.x[nl]
        self.xc = (self.x[1:] + self.x[:-1])/2
        self.nl = nl
        self.nu = nu
        self.calculated = True
    def update(self, nl, nu):
        if not self.calculated or nl>self.nl or nu>self.nu:
            self.calculate(nl, nu)
    def __repr__(self):
        if self.calculated:
            return 'grid_points(nl = '+repr(self.nl)+', nu = '+repr(self.nu)+')'
        else:
            return 'grid_points()'

class Grid:
    """Grid(num_cells, delta=1, start=0, periodic=False, maxnlnu=None)
    
    Grid may be
        simple (uniform) :
            constant :math:`\Delta`
        non-uniform :
            :math:`\Delta`, or rather, :math:`x` is stored in an array
    
    """
    def __init__(self, num_cells, delta=1, start=0, periodic=False):
        if not (CO.is_integer_scalar(num_cells) and num_cells>0):
            raise ValueError('num_cells must be a positive integer')
        self.num_cells = num_cells
        self.uniform = (np.asarray(delta).size==1)
        if self.uniform and not (CO.is_real_scalar(delta) and delta>0):
             raise ValueError('delta must be a positive number')
        if not self.uniform and not \
            (CO.is_real_vector(delta) and len(delta)==num_cells and (delta>0).all()):
            raise ValueError(
                'delta must be a 1D array of positive numbers of length '+str(num_cells))
        self.delta = delta
        if not CO.is_real_scalar(start):
            raise ValueError('start must be a real number')
        self.start = start
        if self.uniform:
            self.stop = self.start + self.num_cells*self.delta
        else:
            self.stop = self.start + np.sum(self.delta)
        if not self.uniform:
            self.periodic = periodic
            self.points = GridPoints(self)
        self._geom = None
    def copy(self):
        if self.uniform:
            return Grid(self.num_cells, delta=self.delta, start=self.start)
        else:
            return Grid(self.num_cells, delta=self.delta, start=self.start, periodic=self.periodic)
    def _arr(self, stag, nl, nu):
        "Coordinates of the given configuration"
        assert (stag==0 or stag==1)
        n = self.num_cells + 1 - stag
        if self.uniform:
            return self.start + (stag/2 + np.arange(-nl, n+nu))*self.delta
        # Nonuniform case
        self.points.update(nl, nu)
        return (self.points.x if stag==0 else self.points.xc)[self.points.nl-nl:self.points.nl + n + nu]
    # def dx_arr(self, stag, nl, nu):
    #     if self._uniform:
    #         return self._delta
    #     # Nonuniform case
    #     n = self._num_cells + 1 - stag
    #     self.points.update(nl, nu)
    #     return (self.points.dx if stag==1 else self.points.dx0)[self.points.nl-nl:self.points.nl + n + nu]
    def add_Geometry(self, geom, axis, nl=0, nu=0):
        if self._geom is not None:
            raise SyntaxError('geometry already added!')
            # if self._geom is not geom:
            #     raise SyntaxError('cannot reuse grid with another geometry')
            # # Safeguard against adding it once again
            # return
        self._geom = geom
        self.axis = axis
        stags0 = tuple(0 if a==axis else None for a in range(geom.ndim))
        stags1 = tuple(1 if a==axis else None for a in range(geom.ndim))
        nls_ = tuple(nl if a==axis else None for a in range(geom.ndim))
        nus_ = tuple(nu if a==axis else None for a in range(geom.ndim))
        ncells_ = tuple(gr.num_cells for gr in geom.grids)
        self._x = ExtendedArray(ncells_, stags0, nls=nls_, nus=nus_, is_constant=True)
        self._x.arr = self._arr(0, nl, nu)
        self._xc = ExtendedArray(ncells_, stags1, nls=nls_, nus=nus_, is_constant=True)
        self._xc.arr = self._arr(1, nl, nu)
        # Or just (with the same effect)
        # self._xc = EA._ave(self._x, axis)
        self._dx = _diff(self._x, axis)
        self._dx0 = _mean(self._dx, axis)
    @property
    def r_n(self):
        return self._x
    @property
    def r_c(self):
        return self._xc
    @property
    def dr_c(self):
        if self.uniform:
            return self.delta
        else:
            return self._dx
    @property
    def dr_n(self):
        if self.uniform:
            return self.delta
        else:
            return self._dx0
    #def dx(self, geom=None, stags=None, nls=(0,), nus=(0,)):
    #    res = ExtendedArray(geom, stags, nls=nls, nus=nus)
    #    res.arr = self.dx(stags[0], nl=nls[0], nu=nus[0])
    #    return res
    pass

CO.make_indented_printable(Grid)

#%% A few auxiliary routines

def _integrate_on_index_grid(ea, index_grid):
    "Not really used yet"
    cs = ea.cumsum()
    # First, need to convert index_grid to indices into cs.arr
    index_grid_iterator = iter(index_grid)
    numpy_index_grid = ()
    for axis in range(ea.ndim):
        if not ea.is_dim[axis]:
            continue
        index_direction = next(index_grid_iterator)
        numpy_index_direction = ()
        for i in index_direction:
            numpy_index_direction += reslice(
                    i, cs.ncells[axis], cs.stags[axis], cs.nls[axis],cs.nus[axis],axis),
        numpy_index_grid += np.array(numpy_index_direction),
    # Now, ii is a regular Python index
    tmp = cs.arr
    for axis in range(cs.ndim_real):
        s = tuple(numpy_index_grid[axis] if a==axis else slice(None) for a in range(cs.ndim_real))
        tmp = tmp[s]
    # Now, take the differences
    for axis in range(cs.ndim_real):
        tmp = np.diff(tmp, axis=axis)
    return tmp

def _interpolate_on_dindex_grid(ea, dindex_grid):
    """
    Extended array interpolated on a grid of doubles between integer or half-integer indices.

    Parameters
    ----------
    ea : extended_array
        Array being interpolated.
    dindex_grid : iterable of length ea.ndim_real
        Coordinates in each direction, with lengths N[a].

    Returns
    -------
    result: np.array of shape (N[0],N[1], ... N[ea.ndim_real-1])
    """
    assert len(dindex_grid)==ea.ndim_real
    points = CO.ndgrid(*dindex_grid)
    return ea.interpolate(points)

def _integrate_on_coor_grid(ea, geom, boundary_grid):
    assert len(boundary_grid)==ea.ndim_real
    # Density to amount
    eadvol = ea.copy_arr()
    for a in range(ea.ndim):
        if not ea.is_dim[a]:
            continue
        eadvol *= (geom.grids[a].dr_n if ea.stags[a]==0 else geom.grids[a].dr_c)
    cs = eadvol.cumsum()
    # Convert coordinates to indices (double)
    boundary_iter = iter(boundary_grid)
    dindex_grid = []
    for axis in range(ea.ndim):
        if not ea.is_dim[axis]:
            continue
        boundary = next(boundary_iter)
        dindex_boundary = geom.dindex(boundary, axis)
        #r =  geom.grids[axis].r_n
        #index_boundary = np.interp(boundary, r.arr, np.arange(r.nts[axis])-r.nls[axis])
        dindex_grid.append(dindex_boundary)
    # Step two
    tmp = _interpolate_on_dindex_grid(cs, dindex_grid)
    # Now, take the differences
    for axis in range(cs.ndim_real):
        tmp = np.diff(tmp, axis=axis)
    return tmp

class Geometry:
    """Abstract class.
    Orthogonal (i.e., diagonal metric) geometry which may be:
        flat (default) :
            metric is a unit tensor
        curved :
            with metric given as a _function_ of coordinates
    """
    def __init__(self, grids, nls=None, nus=None):
        assert isinstance(grids,tuple)
        self.grids = grids
        self.ndim = len(grids)
        self.ncells = tuple(agrid.num_cells for agrid in grids)
        for axis, agrid in enumerate(self.grids):
            agrid.add_Geometry(self, axis,
                           nl=(0 if nls is None else nls[axis]),
                           nu=(0 if nus is None else nus[axis]))
    def dindex(self, coor, axis):
        r = self.grids[axis].r_n
        return np.interp(coor, r.arr, np.arange(r.nts[axis])-r.nls[axis])
    def interpolate(self, ea, coor):
        assert self.ncells==ea.ncells
        coor_iterator = iter(coor)
        dindex = []
        for axis in range(ea.ndim):
            if not ea.is_dim[axis]:
                continue
            coor_axis = next(coor_iterator)
            dindex_axis = self.dindex(coor_axis, axis)
            dindex.append(dindex_axis)
        return ea.interpolate(dindex)
    # Differential operators
    def _grad(self, f, axis):
        assert f.ncells==self.ncells
        gr = self.grids[axis]
        if gr.uniform:
            d = gr.delta
        else:
            d = (gr.dr_c if f.stags[axis]==0 else gr.dr_n)
        return _diff(f, axis)/d # unfortunately, defined at the wrong points for c-functions!
    def _div(self, f, axis):
        raise NotImplementedError('virtual _div: use concrete geometry!')
    def grad(self, f, axis):
        raise NotImplementedError('virtual grad: use concrete geometry!')
    def divergence(self, eas):
        assert len(eas)==self.ndim
        return sum(self._div(ea, axis) for axis, ea in enumerate(eas))
    def c_aver(self, f, axis):
        "Node function interpolated to cell centers"
        # Just a front end to c_mean, to make more symmetric because of counterpart n_aver
        assert f.ncells==self.ncells
        assert f.stags[axis]==0
        return c_mean(f, axis)
    def n_aver(self, f, axis):
        "Cell-center function interpolated to grid nodes"
        assert f.ncells==self.ncells
        assert f.stags[axis]==1
        gr = self.grids[axis]
        if gr.uniform:
            return n_mean(f, axis)
        else:
            return n_mean(f/gr.dr_c, axis)/n_mean(1/gr.dr_c, axis)
    def n_gradn(self, f, axis):
        "Centered gradient"
        assert f.ncells==self.ncells
        assert f.stags[axis]==0
        return self.n_aver(self.grad(f,axis),axis)
    def laplacian(self, f):
        assert f.ncells==self.ncells
        return self.divergence([self.grad(f, axis) for axis in range(self.ndim)])
    pass

CO.make_indented_printable(Geometry)

class GeometryCartesian(Geometry):
    "Coordinates are nameless"
    def grad(self, f, axis):
        return self._grad(f, axis)
    def _div(self, f, axis):
        if f.stags[axis]==1:
            return self._grad(f, axis)
        else:
            #return self._grad(f, axis)
            gr = self.grids[axis]
            if gr.uniform:
                return c_diff(f, axis)/gr.delta
            else:
                return c_diff(f, axis)/c_mean(gr.dr_n, axis)
    def integrate(self, ea, boundary_grid):
        return _integrate_on_coor_grid(ea, self, boundary_grid)

class GeometryCylindrical(Geometry):
    "Also includes polar. Must have names, e.g., ('r', 'z')."
    def __init__(self, grids, names, nls=None, nus=None):
        "The order of grids is important!"
        self.names = names
        super().__init__(grids, nls=nls, nus=nus)
        # Error check the 'names'
        if self.ndim==1:
            assert self.names==('r',)
        elif self.ndim==2:
            assert self.names==('r','z') or self.names==('r','phi')
        elif self.ndim==3:
            assert self.names==('r','phi','z')
        # Sanity check
        assert self.names.index('r')==0
        gridr = self.grids[0] # or self.grids[0], because it will always be!
        self.r_trick = gridr.r_n.copy_arr()
        assert np.abs(self.r_trick[0]/self.r_trick[1]) < 1e-10 # make sure it is really zero
        self.r_trick[0] = 1. # any number !=0, for the tricky formula with divergence
        self.r_correction = 2*(1-n_mean(gridr.r_c, axis=0)/self.r_trick)/gridr.dr_n
    def grad(self, f, axis):
        assert f.ncells==self.ncells
        if self.names[axis] != 'phi':
            return self._grad(f, axis)
        else:
            raise NotImplementedError('no phi grad yet')
    def _div(self, f, axis):
        "Component of divergence"
        assert f.ncells==self.ncells
        if self.names[axis]=='r':
            gridr = self.grids[axis]
            if f.stags[axis]==1:
                # Maybe not the super-efficent way, but ...
                # I was missing the second term before!!!
                res = n_diff(gridr.r_c*f, axis)/(gridr.dr_n*self.r_trick) + \
                    n_diff(f, axis)*self.r_correction
            else:
                # The node-valued vector field
                # We do not have to use the trickery because gridr.r_c is never zero
                if gridr.uniform:
                    tmp = c_mean(f, axis)
                else:
                    tmp = c_mean(f/gridr.dr_n, axis)/c_mean(1/gridr.dr_n, axis)
                res = c_diff(f, axis)/c_mean(gridr.dr_n,axis) + tmp/gridr.r_c
            return res
        elif self.names[axis]=='phi':
            raise NotImplementedError('no phi div yet')
        else:
            # Along z, same as in Cartesian case
            assert self.names[axis]=='z'
            return self._grad(f, axis)       
    def integrate(self, ea, boundary_grid):
        assert self.names.index('r')==0 # just in case
        if ea.stags[0]==0:
            r = self.grids[0].r_n
        else:
            r = self.grids[0].r_c
        return _integrate_on_coor_grid(2*np.pi*ea*r, self, boundary_grid)

