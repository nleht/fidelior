#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 26 15:21:05 2020

Common notations:
    stencil -- a list of tuples (points), denoting the shifts from the central point, e.g.,
        a forward CIR stencil [(0,0), (1,0), (0,1)]
    ndim -- number of dimensions
    npoints -- number of points in a stencil
    degree -- the degree of a stencil polynomial
    dof -- degrees of freedom, number of independent polynomials of a given degree
        (also the number of distinct atomic polynomials)
    atom -- atomic polynomial, like x^2y^4z, represented by a tuple of length ndim, like (2,4,1)
    u_ea -- the discretized function u_i represented by an ExtendedArray
    Emat -- matrix of values E^i_{lj} = A_l(r'_{i+s_j}) of size (dof, npoints) [+ nts]
    Cmat -- coefficient C^i_{kl} of size (npoints,dof) [+ nts]
        For a uniform grid, there is only one Emat, Cmat (size does not have nts)
        For nonuniform grids, nts is the size of the ExtendedArray to which they are applied

@author: Nikolai G. Lehtinen
"""

import numpy as np
import matplotlib.pyplot as plt
import itertools
if True:
    from . import common as CO
    from . import extended_arrays as EA
    from . import geometries as GE
    from . import end, half, span, c_mean, n_mean, c_diff, n_diff, c_upc, n_upn
else:
    from fidelior import common as CO
    from fidelior import extended_arrays as EA
    from fidelior import geometries as GE
    from fidelior import end, half, span, c_mean, n_mean, c_diff, n_diff, c_upc, n_upn

#%% A simple class to keep track of directions
class Quadrants:
    """A simple class to keep track of directions.
    Usage: quads = Quadrants(ndim)
    ndim -- number of dimensions"""
    def __init__(self, ndim):
        self.n = 2**ndim
        self.qaddr = np.zeros((2,)*ndim, dtype=np.int)
        self.qaddr.flat[:] = np.arange(self.n)
    def num(self, i):
        """The number of the quadrant, i.e., in 3D: kq = quads.num((0,0,1)).
        Same as kq = quads.qaddr[0,0,1]"""
        return self.qaddr[i]
    def is_neg(self, kq):
        """Which directions are negative? E.g., in 3D:
            kq = quads.qaddr[0,0,1] # or quads.num((0,0,1))
            is_neg = quads.is_neg(kq) # returns array([False, False, True])
        """
        return np.array(np.unravel_index(kq, self.qaddr.shape)).astype(bool)
    def sign(self, kq):
        """Similar to quads.is_neg, but returns an array of signs (integers -1 or 1)"""
        return -2*self.is_neg(kq).astype(int)+1
    
CO.make_indented_printable(Quadrants, multiline=False)

#%% Operations on stencils; automatically-generated stencils
# We remind that a stencil is a list of tuples denoting the shifts from the central point

# Operation on stencils
def plot_stencil(stencil):
    """Plot a 2D stencil. The central point is highlighted."""
    plt.plot(0,0,'ro',markersize=10)
    plt.plot(*list(zip(*stencil)),'ko')
    plt.gca().set_aspect('equal')

def transformed_stencil(stencil, permute=None, scale=None, shift=None):
    """Permute, then scale, then shift."""
    s = np.array(stencil)
    if permute is not None:
        s = s[:,permute]
    if scale is not None:
        s = s * scale
    if shift is not None:
        s = s + shift
    # Back to a list of tuples
    return [tuple(s_) for s_ in s]

def stencil_on_Grid(stencil, grids):
    """Make stencil nonuniform, with new coordinates given by grids."""
    s = np.array(stencil)
    s -= np.min(s, axis=0)
    snew = s.copy()
    for axis, grid in enumerate(grids):
        snew[:,axis] = np.array(grid)[s[:,axis]]
    return [tuple(s_) for s_ in snew]

def boundary_thickness(stencil):
    """Maximum displacement from the central point in any direction"""
    return max(max(abs(s) for s in point) for point in stencil)

# Basic stencils

def odd_order_stencil(ndim, n, cover=0):
    """Automatically generate. Order is N=2n-1. If cover==1, then it is the union of all
    flipped stencils."""
    if ndim==1:
        s = [(Z,) for Z in range(-n, n+cover)]
        return s
    assert ndim==2 # sorry, ndim>2 not implemented
    s = []
    for Z in range(2*n+cover):
        Z1 = Z//2
        Z2 = Z-n-Z1
        for T in range(n+1-Z%2):
            s_ = (-Z1+T,-Z2-T)
            #print(Z, T, s_)
            s.append(s_)
    return s

def even_order_stencil(ndim, n):
    """A tail scheme. Order N=2*n.
    In 2D, for n>1 these are only useful as Warming-Beam schemes
    (flipped in both x and y and shifted by (-1,-1))
    because they are slightly unstable for small c. See demo_stability."""
    if ndim==1:
        s = [(Z,) for Z in range(-n,n+1)]
        return s
    assert ndim==2
    s = []
    for Z in range(2*n+2):
        Z1 = Z//2
        Z2 = Z-n-1-Z1
        for T in range(1-Z%2,n+1):
            s_ = (-Z1+T,-Z2-T)
            #print(Z, T, s_)
            s.append(s_)
    return s

# Derivative stencils

def odd_left_stencil(ndim, n):
    "Flip just one point"
    s = odd_order_stencil(ndim, n)
    s[s.index((1,-n+1))] = (1,n)
    return s

def odd_right_stencil(ndim, n):
    return transformed_stencil(odd_left_stencil(ndim, n), permute=(1,0))

def warming_beam_stencil(ndim, n):
    return transformed_stencil(even_order_stencil(ndim, n), scale=-1, shift=1)

def flipped_stencils(stencil0, ndim):
    quad = Quadrants(ndim)
    stencils = []
    stencil = []
    for kq in range(quad.n):
        # Transform the stencil to the given quadrant
        stencil_flipped = transformed_stencil(stencil0, scale=quad.sign(kq))
        stencil, _, _ = CO.list_union(stencil, stencil_flipped)
        stencils.append(stencil_flipped)
    return quad, stencil, stencils

#%% The atomic polynomial operations

def apply_atom(atom, r):
    r"""Apply atomic polynomial `atom` to the radius-vector `r`.
    
    `r` is an iterable (preferably, a tuple) of length `ndim`, whose elements are
    numbers or of type ``ExtendedArray``. The result is either a number or an
    ``ExtendedArray``."""
    muls = [xi**p for xi, p in zip(r, atom)]
    prod = muls[0]
    if all(mul.shape==prod.shape for mul in muls):
        #return np.prod([xi**p for xi, p in zip(r, atom)], axis=0)
        return np.prod(muls, axis=0)
    else:
        # Broadcast!
        for mul in muls[1:]:
            prod = prod * mul
        return prod

def atomic_polynomials(ndim, degree):
    "Generate all atomic polynomials of ndim variables of given degree"
    res = list(itertools.combinations(np.arange(ndim + degree), ndim))
    atoms = []
    for p in res:
        a = 0
        atom = ()
        for i in p:
            atom += (i - a,)
            a = i + 1
        atoms.append(atom)
    # Check if we have all of them
    dof = np.math.factorial(degree+ndim) // np.math.factorial(ndim) // np.math.factorial(degree)
    assert len(atoms)==dof
    return atoms

#%% The most important class: the stencil polynomial
class StencilPolynomial:
    """The most important class: the stencil polynomial"""
    def __init__(self, geom, u_indep, degree, stencil0, flip_stencil=False):
        """u_indep is used only for:
            -- stags -- to determine whether the scheme is centered,
            -- nus, nls -- to determine the sizes of needed velocities/displacments w
        """
        self._geom = geom
        self.degree = degree
        ndim = geom.ndim
        # Calculate the stencil table
        self.flip_stencil = flip_stencil
        if flip_stencil:
            self.quad, self.stencil, self.stencils = flipped_stencils(stencil0, geom.ndim)
        else:
            self.quad = None
            self.stencil = stencil0
            self.stencils = [stencil0]
        nstencils = len(self.stencils)
        # Determine whether we determine on nodes or centers
        tmp = np.unique(u_indep.stags)
        if not len(tmp)==1:
            print('u_indep =',u_indep)
            raise Exception('Mixed center-node stencil polynomial is not implemented')
        self.is_centered = (tmp[0]==1) # if is_centered, the determination is at centers
        # Apparent volume element
        self.dr = [EA._diff((gr.r_n if self.is_centered else gr.r_c), a)
                   for a,gr in enumerate(geom.grids)]
        self.apparent_volume = np.prod(self.dr)
        # "u_used" describes which points are necessary to calculate the advection step
        assert u_indep.ncells==geom.ncells
        self.u_indep = u_indep
        # self.u_used = # one more cell outside is used to calculate the outer face values
        self.atoms = atomic_polynomials(ndim, degree)
        dof = len(self.atoms)
        npoints = len(self.stencil)
        self.extra_size = (nstencils, npoints, dof)
        self.uniform = np.array([gr.uniform for gr in geom.grids])
    def r_rb(self, axis):
        gr = self._geom.grids[axis]
        return (gr.r_c, gr.r_n) if self.is_centered else (gr.r_n, gr.r_c)
    def _set_u_used(self, u_used, allocate_Cmat=True):
        ndim = self._geom.ndim
        self.u_used = u_used
        self.size = u_used.nts
        #self.size_short = tuple(np.array(self.size)[~self.uniform])
        self.size_real = tuple((self.size[a] if not self.uniform[a] else 1) for a in range(ndim))
        # Other info
        self.Cshape_real = self.extra_size + self.size_real
        #self.Cshape_short = extra_size + self.size_short
        self.Cshape_full = self.extra_size + self.size
        if allocate_Cmat:
            self._Cmat = np.zeros(self.Cshape_real)
        self.nus = tuple(np.max([point[a] for point in self.stencil]) for a in range(ndim))
        self.nls = tuple(-np.min([point[a] for point in self.stencil]) for a in range(ndim))
        self.nus_min = np.array(self.u_used.nus) + np.array(self.nus)
        self.nls_min = np.array(self.u_used.nls) + np.array(self.nls)
    # The next few function describe separate steps for finding the next value
    def _check_u(self, u):
        assert isinstance(u, EA.ExtendedArray) and u.ncells==self.u_used.ncells and \
            u.stags==self.u_used.stags
        # Extended checks for the size: u must have enough margin to update u_used
        assert all(np.array(u.nus)>=self.nus_min)
        assert all(np.array(u.nls)>=self.nls_min)
    def _check_inputs(self, u, ws, fem=False):
        self._check_u(u)
        ndim = self._geom.ndim
        assert len(ws)<=2
        for w in ws:
            assert len(w)==ndim
            for a,wa in enumerate(w):
                if fem:
                    dnls = tuple(half if a1==a else 1 for a1 in range(ndim))
                    dnus = tuple(half if a1==a else 1 for a1 in range(ndim))
                    assert EA.same_ea_structure(self.u_indep.widen(dnls=dnls, dnus=dnus), wa)
                else:
                    assert EA.same_ea_structure(self.u_used, wa)
    def _choose_stencil(self, ws):
        "Select only C.shape = size + (npoints, dof) into self._Cmat_s"
        #self.check_inputs(u, (w,))
        ndim = self._geom.ndim
        #Cmat_ = self._Cmat.reshape(self.Cshape_real) # unsqueeze (maybe not necessary)
        if self.flip_stencil:
            if len(ws)>1:
                assert len(ws)==2
                w1, w2 = ws
                wav = [(w1a+w2a)/2 for w1a, w2a in zip(w1, w2)]
            else:
                wav, = ws
            q = tuple((wa < 0).arr.astype(np.int) for wa in wav) # q[a].shape==size
            for qa in q:
                assert qa.shape==self.size
            kq = self.quad.num(q) # kq.shape==size
            self._Cmat_s = CO.choose(kq, self._Cmat, axis=0, which=range(3,3+ndim))
        else:
            self._Cmat_s = self._Cmat[0]
        # Now self._Cmat_s.shape = (npoints, dof) + size_real
    def _apply_u(self, u):
        "Apply self._Cmat_s to u, get self._Cmat_u of shape (dof,) + size"
        self._check_u(u)
        s = span(self.u_used)
        self._Cmat_u = np.zeros((len(self.atoms),) + self.size)
        for k, shift in enumerate(self.stencil):
            us = u.shifted(shift)[s][None,:] # us.shape==(1,) + size
            C = self._Cmat_s[k] # C.shape==(dof,) + size_real
            self._Cmat_u += us*C # broadcasted, hopefully, to shape=(dof,)+size
    def interpolate_on_displacement(self, u, w):
        self._check_inputs(u, (w,))
        self._choose_stencil((w,))
        self._apply_u(u) # produces self._Cmat_u.shape = (dof,) + size
        Ar = self.calculate_Ar('interpolate', (w,)) # produces Ar.shape = (dof,) + size
        res = self.u_used.copy()
        res.arr = np.sum(self._Cmat_u * Ar, axis=0)
        return res
    def integrate_on_displacement(self, u, ws):
        assert len(ws)==2
        self._check_inputs(u, ws)
        self._choose_stencil(ws)
        self._apply_u(u) # produces self._Cmat_u.shape = (dof,) + size
        Ar = self.calculate_Ar('integrate', ws) # produces Ar.shape = (dof,) + size
        res = self.u_used.copy()
        res.arr = np.sum(self._Cmat_u * Ar, axis=0)
        return res
    def _get_Ar_element(self, operation, was, p, axis):
        if operation=='interpolate':
            wa, = was
            return wa.arr**p
        wa1, wa2 = was
        v1 = wa1.arr; v2 = wa2.arr
        t1 = (v2**(p+1)-v1**(p+1))/(p+1)
        cyl_r = (isinstance(self._geom, GE.GeometryCylindrical) \
                and self._geom.names[axis]=='r')
        if not cyl_r:
            if operation=='integrate':
                return t1
            elif operation=='average':
                dv = v2-v1
                i = (dv!=0)
                dv[i] =  t1[i]/dv[i]
                dv[~i] = v1[~i]**p # by L'Hospital's rule or just the same as "interpolate"
                return dv
        # Now, the more complicated integration over radial coordinate
        assert axis==0
        assert self.is_centered # volume averaging only implemented for cell-centered FEM schemes
        gr = self._geom.grids[0]
        r = gr.r_c.broadcast(wa1).arr
        res = r.copy()
        t = r*t1 + (v2**(p+2)-v1**(p+2))/(p+2)
        if operation=='integrate':
            return t
        elif operation=='average':
            # Average over r, like in operation==2
            rint = r*(v2-v1) + (v2**2-v1**2)/2
            i = (rint!=0)
            res[i] = t[i]/rint[i]
            res[~i] = (r[~i]*v1[~i]**p + v1[~i]**(p+1))/(r[~i]+v1[~i])
            return res
    def _get_Ar_atom(self, operation, wsr, atom):
        return np.prod([self._get_Ar_element(operation, wsr[axis], atom[axis], axis)
                        for axis in range(self._geom.ndim)], axis=0)
    def calculate_Ar(self, operation, ws):
        assert operation in ['interpolate', 'integrate', 'average']
        # Rearrange ws by axis
        wsr = list(zip(*ws))
        return np.array([self._get_Ar_atom(operation, wsr, atom) for atom in self.atoms])
    def _fem_limits(self, w, axes):
        """We already have C*u=Cu=self._Cmat_u with Cu.shape = (dof,) + size.
        Below, i is half-integer index of w, and j is integer index of the cell I_j.
        We cycle over all geometrical axes, and `axis` is a boolean element of array `axes`.
        The following operations are performed on Cu:
        1. Cu is shifted upstream in several dimensions indicated by `axes`.
            If axis==True:
                take Cu_j, with j= (i+1/2, if w_i>0 else i-1/2), j is integer, for each i!
                Now we have Cu_i (stag=1 along the `axis`)
            If axes==False:
                There is no upstream shift!
            The resulting Cu_i has the same stags as all relevant (ie.axis=True) w
        2. Along each axis, the integration limits are:
            if axis==False:
                from x_{j-1/2}-x_j<0 
                to x_{j+1/2}-x_j>0
            if axis==True:
                from  s=x_{j-1/2}-x_j<0 (if w_i>0) or s=x_{j+1/2}-x_j>0, (if w_i<0)
                    (which may be generalized to s=x_i-x_j)
                to s+w (which must be inside cell I_j)
        This looks like the most tricky function in this class
        (the previous version was much worse!)
        """
        ndim = self._geom.ndim
        assert len(axes)==ndim
        wmod = []
        lolim = []
        hilim = []
        wa_shape = None
        for a in range(ndim):
            #gr = self._geom.grids[a]
            r, rb = self.r_rb(a)
            #r, rb = (gr.r_c, gr.r_n) if self.is_centered else (gr.r_n, gr.r_c)
            if axes[a]:
                wa = w[a]
                for a1 in range(ndim):
                    if a1!=a and axes[a1]:
                        if self.is_centered:
                            wamean = self._geom.n_aver(wa, a1)
                        else:
                            wamean = self._geom.c_aver(wa, a1)
                        #wa = wa.lo(a1).view[span(wamean)] # -- does not work!
                        #wa = fdo.c_upc(w[a], fdo.c_mean(w[a1], a), a1).view[span(wamean)]
                        # -- VERY slightly (~10%) less spreading, not worth the hassle!
                        wa = wamean
                wmod.append(wa)
                if wa_shape is None:
                    wa_shape=wa.copy()
                assert EA.same_ea_structure(wa, wa_shape)
                # This one is tricky, see the writeup on autoschemes.
                # 'r' is taken in the w-shifted cell -- the same where Cu_w will be taken
                # _up = (n_upn if self.is_centered else c_upc)
                # rup = _up(r, -wa, a) #(n_upn(r, -wa, a) if self.is_centered else c_upc(r, -wa, a))
                # To avoid ambiguity, we define our own upstream-shifting function here
                rup = r.lo(a)*(wa<0) + r.hi(a)*(wa>=0)
                lo = rb - rup
                lolim.append(lo.broadcast(wa_shape))
                hilim.append(lo+wa)
                # Note that hilim<lolim if wa<0 -- we integrate backwards but this automatically
                # provides the correct sign of the result.
                # lolim and hilim are at half-integer indices along "a"
            else:
                # These will need to be broadcasted later.
                lolim.append(rb.lo(a) - r) # at integer indices along "a"
                hilim.append(rb.hi(a) - r)
        # Fix the shape of trivial limits
        if wa_shape is None:
            wa_shape = self.u_used
        for a in range(ndim):
            if not axes[a]:
                lolim[a] = lolim[a].broadcast(wa_shape)
                hilim[a] = hilim[a].broadcast(wa_shape)
        # wa_shape has integer indices where axis==False, and half-integer where axis==True,
        # the same with lolim and hilim
        # We already have the integration limits but do not have the upstream-shifted Cu_w
        Cu_w = 0
        Cu = self._Cmat_u # Cu.shape==(dof,)+self.size
        order = sum(axes)
        # wmod contains only the relevant components
        assert len(wmod)==order
        quad = Quadrants(order)
        for kq in range(quad.n):
            is_neg = quad.is_neg(kq) # array of bool of length "order"
            # Did not like wa.arr>=0 in the next line :O
            tmp = tuple(wa.arr<0 if i else wa.arr>=0 for i,wa in zip(is_neg, wmod))
            select = np.logical_and.reduce(tmp) # of the same shape as wa_shape
            # Take Cu.lo(axis)*(wmod<0) + Cu.lo(axis)*(wmod>=0)
            ssel = (slice(-1) if i else slice(1, None) for i in is_neg) # shifted for relevant w
            # The first axis of Cu is the observer index (of length dof)
            s = (slice(None),) + tuple(next(ssel) if axis else slice(None) for axis in axes) # full index
            # s, when applied to Cu,  (except first axis) supposed to give the same shape as wa_shape.
            Cu_w += Cu[s]*select[None,:]
        if quad.n==0:
            Cu_w = Cu
        self._Cu_w = Cu_w
        return tuple(lolim), tuple(hilim), wmod
    def _get_FG(self, operation, w, axes):
        """For the CENTERED polynomials only (h_i = r_i).
        We integrate in the limits given by w1, w2 which denote the diplacements of the lower
        left and upper right corners of the integration rectangle from the reference point h_i.
        kq indicates which quadrant to use."""
        assert operation in ['integrate', 'average']
        w1, w2, wmod = self._fem_limits(w, axes)
        Ar = self.calculate_Ar(operation, (w1, w2))
        F = w1[0].copy()
        F.arr = np.sum(self._Cu_w * Ar, axis=0)
        #F = self._F_from_limits(w1, w2)
        return F, wmod
    def face_values(self, u, w):
        "Slightly faster? WARNING: Only 2D!"
        g = self._geom
        if not g.ndim in [1,2,3]:
            raise Exception('Sorry, the higher dimensions are not implemented yet!')
        # Calculate face values
        _aver = (g.c_aver if self.is_centered else g.n_aver)
        if g.ndim==1:
            fv, _ = self._get_FG('average', w, [True])
            return (fv,)
        elif g.ndim==2:
            Gx, _ = self._get_FG('average', w, [True, False])
            Gy, _ = self._get_FG('average', w, [False, True])
            Gxy, wmod = self._get_FG('average', w, [True, True])
            # Face values -- not sure which formula to use, seem approximately equivalent
            fvx = Gx + 0.5*g.grad(wmod[1]*Gxy, 1)
            fvy = Gy + 0.5*g.grad(wmod[0]*Gxy, 0)
            # Supposedly better results for u==1?
            # fvx = Gx + 0.5*n_mean(wmod[1],1)*g.grad(Gxy, 1)
            # fvy = Gy + 0.5*n_mean(wmod[0],0)*g.grad(Gxy, 0)
            # More accurate?
            # fvx = Gx + 0.5*_aver(wmod[1],1)*g.grad(Gxy, 1)
            # fvy = Gy + 0.5*_aver(wmod[0],0)*g.grad(Gxy, 0)        
            return (fvx, fvy)
        elif g.ndim==3:
            # Brute force for now
            Gx, _ = self._get_FG('average', w, [True, False, False])
            Gy, _ = self._get_FG('average', w, [False, True, False])
            Gz, _ = self._get_FG('average', w, [False, False, True])
            Gxy, wxy = self._get_FG('average', w, [True, True, False])
            Gyz, wyz = self._get_FG('average', w, [False, True, True])
            Gzx, wzx = self._get_FG('average', w, [True, False, True])
            Gxyz, _ = self._get_FG('average', w, [True, True, True])
            w1a = g.n_aver(wxy[1],1) # to <c,n,n> grid
            w2a = g.n_aver(wzx[2],2) # to <c,n,n> grid
            fvx = Gx + 0.5*w1a*g.grad(Gxy, 1) + 0.5*w2a*g.grad(Gzx, 2) \
                + (1./3.)*w1a*w2a*g.grad(g.grad(Gxyz, 1), 2)
            w2a = g.n_aver(wyz[2],2) # to <n,c,n> grid
            w0a = g.n_aver(wxy[0],0) # to <n,c,n> grid
            fvy = Gy + 0.5*w2a*g.grad(Gyz, 2) + 0.5*w0a*g.grad(Gxy, 0) \
                + (1./3.)*w2a*w0a*g.grad(g.grad(Gxyz, 2), 0)
            w0a = g.n_aver(wzx[0],0) # to <n,n,c> grid
            w1a = g.n_aver(wyz[1],1) # to <n,n,c> grid
            fvz = Gy + 0.5*w0a*g.grad(Gzx, 0) + 0.5*w1a*g.grad(Gyz, 1) \
                + (1./3.)*w0a*w1a*g.grad(g.grad(Gxyz, 0), 1)
            return (fvx, fvy, fvz)
    def _fem_step_face_value(self, u, w):
        "Use the face value approach"
        fv = self.face_values(u, w)
        g = self._geom
        if self.volume_averaging:
            # This is the default!
            du = np.sum([g.grad(w[a]*fv[a], a) for a in range(g.ndim)], axis=0)
        else:
            # Old algorithm without volume averaging
            if self.is_centered:
                raise Exception('WARNING: cannot use usual divergence for cell-centered scheme')
            du = g.divergence([wa*fva for wa, fva in zip(w, fv)])
        return du
    def _fem_step_integration(self, u, w):
        # Not using face values. This gives too much spreading
        _diff = (c_diff if self.is_centered else n_diff)
        ndim = self._geom.ndim
        if ndim==1:
            F, _ = self._get_FG('integrate', w, [True])
            du = _diff(F, 0)
        elif ndim==2:
            #F, _ = self._get_F(w, [False, False]) # Not needed -- just gives prev. value
            Fx, _ = self._get_FG('integrate', w, [True, False])
            Fy, _ = self._get_FG('integrate', w, [False, True])
            Fxy, wmod = self._get_FG('integrate', w, [True, True])
            du = _diff(Fx, 0) + _diff(Fy, 1) + _diff(_diff(Fxy, 0), 1)
            # # The following is completely equivalent -- debugging
            # g = self._geom
            # Gx, _ = self._get_FG('average', w, [True, False])
            # Gy, _ = self._get_FG('average', w, [False, True])
            # Gxy, wmod = self._get_FG('average', w, [True, True])
            # #aFx = Gx*w[0]*self.dr[1]
            # #aFy = Gy*w[1]*self.dr[0]
            # #aFxy = Gxy*wmod[0]*wmod[1]
            # #duv = (_diff(aFx, 0) + _diff(aFy, 1) + _diff(_diff(aFxy, 0), 1))/self.apparent_volume
            # #duv = g.grad(Gx*w[0], 0) + g.grad(Gy*w[1],1) + g.grad(g.grad(Gxy*wmod[0]*wmod[1],0),1)
            # fvx = Gx + 0.5*g.grad(Gxy*wmod[0]*wmod[1],1)/w[0] # spreads out
            # fvy = Gy + 0.5*g.grad(Gxy*wmod[0]*wmod[1],0)/w[1] # spreads out
            # fvx = Gx + 0.5*g.grad(Gxy*wmod[1],1) # does not spread out
            # fvy = Gy + 0.5*g.grad(Gxy*wmod[0],0) # does not spread out
            # fv = [fvx, fvy]
            # duv = np.sum([g.grad(fv[a]*w[a], a) for a in range(ndim)], axis=0)
            # du = duv*self.apparent_volume
        else:
            raise Exception('ndim =',ndim,'is not implemented yet!')
        du /= self.apparent_volume
        return du
    def _fem_step_simple_integration(self, u, w):
        """Just integrate the local interpolating function.
        However, this method is not simpler:
            - because it is unstable for wrong stencil, we have to flip stencils;
              choosing stencils is slow!
            - even with correct stencil it is less stable, need smaller time step
            - accuracy is lowered by one order
        """
        w1 = []; w2 = []
        s = span(self.u_indep)
        for a, gr in enumerate(self._geom.grids):
            rb, r = ((gr.r_n, gr.r_c) if self.is_centered else (gr.r_c, gr.r_n))
            w1.append((rb.lo(a) - r).broadcast(self.u_indep) + w[a].lo(a).view[s])
            w2.append((rb.hi(a) - r).broadcast(self.u_indep) + w[a].hi(a).view[s])
        return self.integrate_on_displacement(u, (w1, w2))/self.apparent_volume - u 
    def _fem_step_cumint(self, u, w):
        "The advection step based on cumilative integrals"
        self._choose_stencil(None)
        self.cumint_prepare(u)
        s = span(self.u_indep)
        ndim = self._geom.ndim
        coor1 = []
        coor2 = []
        for axis in range(ndim):
            r, rb = self.r_rb(axis)
            coor1.append((rb.lo(axis).broadcast(self.u_indep) + w[axis].lo(axis).view[s]).arr)
            coor2.append((rb.hi(axis).broadcast(self.u_indep) + w[axis].hi(axis).view[s]).arr)
        tmp = self.cumint([coor2[0], coor2[1]]) - self.cumint([coor1[0], coor2[1]]) -\
            self.cumint([coor2[0], coor1[1]]) + self.cumint([coor1[0], coor1[1]])
        uint = u.copy_arr()
        uint[s]= tmp
        return uint/self.apparent_volume - u
    def fem_advection_step(self, u, w):
        self._check_inputs(u, (w,), fem=True)
        if  len(self.stencils)>1: # was: self.fem_algorithm=='simple integration'
            s = span(self.u_indep) # same as span(self.u_used) for this case
            _mean = (c_mean if self.is_centered else n_mean)
            wav = [_mean(wa, a).view[s] for a, wa in enumerate(w)]
        else:
            wav = None
        self._choose_stencil((wav,))
        #assert len(self.stencils)==1 # for now
        #self._Cmat_s = self._Cmat[0]
        #self._choose_stencil((w,))
        self._apply_u(u)
        if self.fem_algorithm=='face value':
            return self._fem_step_face_value(u, w)
        elif self.fem_algorithm=='integration':
            return self._fem_step_integration(u, w)
        elif self.fem_algorithm=='simple integration':
            return self._fem_step_simple_integration(u, w)
        elif self.fem_algorithm=='cumint':
            return self._fem_step_cumint(u, w)
    # Several routines for integration/mesh resizing
    def get_wref(self, coor, is_grid=False):
        """Get the displacement w and indeces into r[a] which correspond to a given coordinate set
        or coordinate grid 'coor'.  Unfortunately, we cannot determine by shape of 'coor' whether
        full coordinates or just a grid is given.
        """
        ndim = self._geom.ndim
        assert len(coor)==ndim
        if is_grid:
            for a in range(ndim):
                assert len(coor[a].shape)==1
            refsize = tuple(len(c) for c in coor)
        else:
            # Full cooridinates are given
            refsize = coor[0].shape
            for a in range(ndim):
                assert coor[a].shape==refsize
        # Determine the indices
        rspan = span(self.u_used)
        rbspan = span(self.u_used.widen())
        wref_ = ()
        index_ = ()
        for axis in range(ndim):
            r, rb = self.r_rb(axis)
            iaxis = np.searchsorted(rb[rbspan[axis]].flatten(), coor[axis])-1
            wrefaxis = coor[axis] - r[rspan[axis]].flatten()[iaxis]
            wref_ += (wrefaxis,)
            index_ += (iaxis,)
        if is_grid:
            wref = CO.ndgrid(*wref_)
            index = CO.ndgrid(*index_)
        else:
            wref = wref_
            index = index_
        return refsize, index, wref
    def interpolate(self, u, coor, is_grid=False):
        "AFTER the stencil is chosen"
        assert self._Cmat_s is not None
        # we have sp._Cmat_s
        # Check the inputs
        self._check_u(u)
        # Determine the indices
        refsize, index, wref = self.get_wref(coor, is_grid=is_grid)
        index = (slice(None), *index)
        self._apply_u(u) # produces sp._Cmat_u
        Cu = self._Cmat_u[index] # make it correspond to the "refined" points
        # These take the same time, second seems a bit simpler
        Ar = np.array([apply_atom(atom, wref) for atom in self.atoms])
        return np.sum(Cu * Ar, axis=0)
    # Cumulative integral
    def cumint_prepare(self, u):
        """AFTER the stencil is chosen! Stores some aux info."""
        # We already have sp._Cmat_s
        assert self._Cmat_s is not None
        self._check_u(u)
        ndim = self._geom.ndim
        self._apply_u(u) # Produces sp._Cmat_u with shape == (dof,) + sp.size
        # The auxiliary integrals
        lo = ()
        hi = ()
        for axis in range(ndim):
            r, rb = self.r_rb(axis)
            lo += ((rb.lo(axis)-r).broadcast(self.u_used).arr,)
            hi += ((rb.hi(axis)-r).broadcast(self.u_used).arr,)
        #Ar = np.zeros((len(sp.atoms),) + sp.size)
        # Partial integral
        Ccums = []
        quad = Quadrants(ndim)
        dof = len(self.atoms)
        for q in range(quad.n):
            axes = quad.is_neg(q)
            # Integrate between lo and hi for axes marked with "False"
            Cint = np.zeros((dof,)+self.size)
            for ka, atom in enumerate(self.atoms):
                if all(axes):
                    Ar_atom = 1
                else:
                    Ar_atom = np.prod([(hi[a]**(p+1)-lo[a]**(p+1))/(p+1)
                                   for a,p in enumerate(atom) if not axes[a]], axis=0)
                Cint[ka,:] = self._Cmat_u[ka]*Ar_atom
            for a in range(ndim):
                if not axes[a]:
                    Cint = np.cumsum(Cint, axis=a+1)
            Ccum = np.zeros((dof,)+tuple(s if axes[a] else s+1 for a,s in enumerate(self.size)))
            itmp = (slice(None),)+tuple(slice(None) if axes[a] else slice(1,None) for a in range(ndim))
            Ccum[itmp] = Cint
            Ccums.append(Ccum)
        self._Ccum = Ccums
        self._lo = lo
        self._hi = hi
    def cumint(self, coor, is_grid=False):
        "AFTER a call to cumint_prepare!"
        # We already have sp._Cmat_s
        #sp_cumint_prepare(sp, u)
        # We already have sp._Cmat_u and sp._Ccum
        ndim = self._geom.ndim
        refsize, index, wref = self.get_wref(coor, is_grid=is_grid)
        #coor = CO.ndgrid(*coorgrid)
        quad = Quadrants(ndim)
        res = np.zeros(refsize)
        for q in range(quad.n):
            axes = quad.is_neg(q)
            for ka, atom in enumerate(self.atoms):
                if any(axes):
                    Ar_atom = np.prod([(wref[a]**(p+1)-self._lo[a][index]**(p+1))/(p+1)
                                       for a,p in enumerate(atom) if axes[a]], axis=0)
                else:
                    Ar_atom = 1
                res += self._Ccum[q][ka][index]*Ar_atom
        return res

CO.make_indented_printable(StencilPolynomial, replace_repr=True)

#%% Interpolation on arbitrary coordinates




#%% Interpolation or FEM scheme setup
def _reduced_ea(ea, uniform):
    stags = tuple(None if unif else v for v, unif in zip (ea.stags, uniform))
    nls = tuple(None if unif else v for v, unif in zip (ea.nls, uniform))
    nus = tuple(None if unif else v for v, unif in zip (ea.nus, uniform))
    return EA.ExtendedArray(ea.ncells, stags=stags, nls=nls, nus=nus)

def _full_ea(ea, val):
    tmp = ea.copy()
    tmp.arr = np.full(tmp.nts_real, fill_value=val)
    return tmp

def prepare_stencil_polynomial(sp, algorithm, rtol=1e-12):
    r"""Calculate the stencil polynomial coefficient C for a non-uniform grid and all directions.
    Result: Cmat; Cmat.shape = (2**ndim,) + size_short + (npoints, dof)
    where size_short is the size of u_used with uniform grid directions collapsed"""
    nstencils = len(sp.stencils)
    dof = len(sp.atoms)
    npoints1 = len(sp.stencils[0])
    assert npoints1==dof # matrix must be invertible
    u_reduced = _reduced_ea(sp.u_used, sp.uniform)
    Emat = np.zeros((dof, npoints1) + sp.size_real)
    for kq in range(nstencils):
        # Transform the stencil to the given quadrant
        stencil_flipped = sp.stencils[kq] #transformed_stencil(stencil, scale=sp.quad.scale(kq))
        for kp, point in enumerate(stencil_flipped):
            # Calculate ws = (w1, w2)
            if algorithm=='interpolate':
                points = [point]
            else:
                points = [tuple(p-half for p in point), tuple(p+half for p in point)]
            n = len(points)
            ws = [[] for k in range(n)]
            for a, gr in enumerate(sp._geom.grids):
                rb, r = ((gr.r_n, gr.r_c) if sp.is_centered else (gr.r_c, gr.r_n))
                if algorithm=='interpolate':
                    rb = r
                if sp.uniform[a]:
                    for k in range(n):
                        ws[k].append(_full_ea(u_reduced, gr.delta*float(points[k][a])))
                else:
                    for k in range(n):
                        ws[k].append((rb.shifted(points[k]) - r).broadcast(u_reduced))
                        #print(k, ws[k])
            Ar = sp.calculate_Ar(algorithm, ws).reshape((dof,)+sp.size_real)
            Emat[:, kp, :] = Ar
        # sp._Cmat.shape == (nstencils, npoints, dof) + sp.size_real
        i = CO.list_where(stencil_flipped, sp.stencil)
        # Invert in the first two axes (the exception handling is for debugging only!)
        atol = rtol*min([np.min(dr) for dr in sp.dr])**sp.degree
        try:
            tmp = CO.invert_many_matrices(Emat, alg=1, atol=atol)
        except ValueError as e:
            print('max(E) =', np.max(np.abs(Emat)))
            raise e
        sp._Cmat[kq, i, :] = tmp

# A couple of wrappers
VOLUME_AVERAGING = True
FEM_ALGORITHM_NUM = {'face value':0, 'integration':1, 'simple integration':3}
FEM_ALGORITHM = 'face value'

def make_interpolation_scheme_sp(geom, u_indep, degree, stencil0, flip_stencil=True):
    sp = StencilPolynomial(geom, u_indep, degree, stencil0, flip_stencil=flip_stencil)
    sp._set_u_used(u_indep)
    prepare_stencil_polynomial(sp, 'interpolate')
    return sp

def make_fem_scheme_sp(geom, u_indep, degree, stencil0, flip_stencil=False,
                  volume_averaging=None, fem_algorithm=None):
    if volume_averaging is None:
        volume_averaging=VOLUME_AVERAGING
    if fem_algorithm is None:
        fem_algorithm = FEM_ALGORITHM
    if fem_algorithm=='simple integration':
        flip_stencil = True
    sp = StencilPolynomial(geom, u_indep, degree, stencil0, flip_stencil=flip_stencil)
    sp.volume_averaging=volume_averaging
    sp.fem_algorithm = fem_algorithm
    if sp.fem_algorithm=='simple integration':
        sp._set_u_used(sp.u_indep)
    else:
        ndim = sp._geom.ndim
        u_used = u_indep.widen((1,)*ndim, (1,)*ndim)
        sp._set_u_used(u_used)
    prepare_stencil_polynomial(sp, 'average')
    return sp

#%% Some "symbolic" operations with stencil polynomials
def _poly_operation(sp, coef, operation, axis, atomds, arg=None):
    dofd = len(atomds)
    coefd = np.zeros((dofd,)+sp.size_real)
    dof = len(sp.atoms)
    for ka in range(dof):
        atom = sp.atoms[ka]
        if operation=='deriv':
            if atom[axis]==0:
                continue
            atomd = tuple(p-1 if a==axis else p for a,p in enumerate(atom))
            coefd[atomds.index(atomd)] += atom[axis]*coef[ka]
        elif operation=='times':
            atomd = tuple(p+1 if a==axis else p for a,p in enumerate(atom))
            coefd[atomds.index(atomd)] += coef[ka]
        elif operation=='partial':
            atomd = tuple(0 if a==axis else p for a,p in enumerate(atom))
            coefd[atomds.index(atomd)] += arg**atom[axis]*coef[ka]
        elif operation=='raise':
            coefd[atomds.index(atom)] += coef[ka]
        elif operation=='divide':
            if atom[axis]==0:
                assert np.allclose(coef[ka],0)
                continue
            atomd = tuple(p-1 if a==axis else p for a,p in enumerate(atom))
            coefd[atomds.index(atomd)] += coef[ka]
        else:
            raise SyntaxError('Unknown operation: ' + str(operation))
    return coefd

def operation(sp, operation, axis, arg=None):
    assert sp.atoms is not None
    npoints = len(sp.stencil)
    # dof = len(sp.atoms)
    # sp._Cmat.shape==(npoints, dof) + size
    if operation=='deriv':
        degreed = sp.degree - 1
    elif operation=='times':
        degreed = sp.degree + 1
    elif operation=='divide':
        degreed = sp.degree - 1
    elif operation=='partial':
        degreed = sp.degree
    elif operation=='raise':
        degreed = sp.degree + 1
    else:
        raise SyntaxError('Unknown operation: '+operation)
    res = StencilPolynomial(sp._geom, sp.u_used, degreed, sp.stencil)
    # res.atoms = atomds # already done
    res._set_u_used(sp.u_used, allocate_Cmat=False)
    # atomds = atomic_polynomials(sp._geom.ndim, degreed)
    atomds = res.atoms
    Cmatd = []
    for p in range(npoints):
        coef = sp._Cmat_s[p,:]
        coefd = _poly_operation(sp, coef, operation, axis, atomds, arg=arg)
        Cmatd.append(coefd)
    res._Cmat = np.array([Cmatd])
    res.quad = None
    res._choose_stencil(None) # do it right away
    return res
