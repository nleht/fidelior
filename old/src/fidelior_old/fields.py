#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Change log:

Nov 30, 2016:
Vertion 4 of API:
    Removed UpdateField1D class, user must update BC himself by "apply_bc()"

Nov 29, 2016:
Version 3 of API:
    Moved Temp1D to another file extended_array.py, changed the indexing to a
    more intuitive. God rid of dependent/independent tracking on top level (all
    done in a symbolic array)

Oct 27, 2016:
Version 2 of API:
    1. Move dif, ave and up functions to user space
    2. Instead of nl, nu track "dependent" and "independent" compononents
    3. Move BC setting also to user space. Generalize it to setting constraints
    4. Old version saved as field1d_api1.py

Created on Thu Oct 13 20:00:56 2016

@author: nle003
"""
#%% Imports
import sys
import io
import numpy as np
import time
from . import common as CO
#from . import extended_arrays as EA
from . import symbolic_arrays as SY
#from .symbolic_arrays import SymbolicArray
from .extended_arrays import ExtendedArray, same_ea_structure
#from .extended_array_operations import c_dif, n_dif, c_ave, n_ave, c_upc, n_upc

#%% Auxiliary: the BC manager - names constraints and can update variable constraints
class BCManager(SY.ConstraintManager):
    def __init__(self,field):
        self.field = field
        self.cnst = field.symb.arr.ref.cnst
        # Auxiliary information about complex constraints
        self.start = np.zeros((0,),dtype=np.int)
        self.is_variable = np.zeros((0,),dtype=np.bool)
        self.names = []
        # Take over as the constraint manager
        self.saved_manager = self.field.symb.arr.cnst_manager # will not be ever restored
        self.field.symb.arr.cnst_manager = self
        self._frozen = False
        self.current_name = None # for all constraints
        self.default_constraint_number = 0
        self.updating_k = None # only for updating variable constraints
        self.block_setitem = True # Do not allow setitem outside of this manager
    def record(self,name,is_variable=False):
        assert not self._frozen
        self.end_update() # cleanup from the previous
        try:
            k = self.names.index(name)
        except ValueError:
            pass
        else:
            if self.is_variable[k]:
                raise Exception('Use "update" for variable BC '+name)
            else:
                raise Exception('There is already BC called '+name)
        self.names.append(name)
        self.current_name = name
        self.is_variable = np.hstack((self.is_variable,is_variable))
        self.start = np.hstack((self.start,self.cnst.op.M.shape[0]))
    def update(self,name):
        self.end_update() # cleanup from the previous
        k = self.names.index(name)
        assert self.is_variable[k]
        self.updating_k = k
        self.tmp_op = SY.Operator(np.ndarray((0,self.cnst.n)),np.ndarray((0,)))
        if self.field.symb.arr.ref.array_like:
            self.tmp_ind = np.ndarray((0,),dtype=np.int)
        else:
            self.tmp_ind = None
    def __getitem__(self,i):
        # Just forward, no extra work
        return self.field.symb[i]
    def workon(self,c,ind):
        "Called by __setitem__ in a symbolic array"
        if self.block_setitem:
            raise Exception('Please use bc manager for setting constraints')
        if self.updating_k is None:
            # Usual situation, just do the default thing
            assert not self.current_name is None
            self.cnst.append(c,ind)
        else:
            # Updating a variable constraint
            self.tmp_op = self.tmp_op.cat(c)
            if self.field.symb.arr.ref.array_like and ind is None:
                raise Exception('internal error')
            else:
                self.tmp_ind = np.hstack((self.tmp_ind,ind))
    def __setitem__(self,i,v):
        # Forward to self.field.symb, this will trigger workon anyway
        self.block_setitem = False
        if self.current_name is None:
            # Recording a nameless non-variable bc
            name='_default_bc_#'+str(self.default_constraint_number)
            self.default_constraint_number += 1
            self.record(name,is_variable=False)
        self.field.symb[i] = v # triggers a call to "self.workon"
        self.block_setitem = True
    def coords(self,k):
        "Start and end of constraint #k"
        start = self.start[k]
        if k == len(self.is_variable) - 1:
            stop = self.num
        else:
            stop = self.start[k+1]
        return slice(start,stop)        
    def end_update(self):
        "Cleanup after previous variable bc recording"
        if not self.updating_k is None:
            # do the cleanup
            k = self.updating_k
            i = self.coords(k)
            if not SY.array_equal(self.tmp_op.M, SY.to_indexable(self.cnst.op.M)[i,:]):
                #print_constraints(self,'f')
                # No recovery, must re-do the whole thing
                raise Exception('The updated BC "'+self.current_name+'" must be the same!')
            assert SY.array_equal(SY.to_indexable(self.cnst.op.M)[i,:],self.tmp_op.M)
            # Not necessary to determine ind, they are the same
            if not self.tmp_ind is None:
                assert np.array_equal(self.tmp_ind,self.cnst.ind[i])
            self.cnst.op.V[i]=self.tmp_op.V
            # switch back the manager
            #assert not self.saved_manager is None
            #self.field.symb.arr.cnst_manager = self.saved_manager
            #self.saved_manager = None
            self.updating_k = None
        # There is no need to cleanup for a regular bc recording
    def freeze(self):
        "Before applying the constraints, make sure they are final"
        # Duplication of SY.constraint_storage.freeze() ???
        if self.frozen:
            print('WARNING: re-freezing')
        t0 = time.time()
        self.end_update() # cleanup from the previous update
        self._frozen = True
        if self.num==0:
            return
        if CO.DEBUGLEVEL>2: t1 = time.time()
        self.cnst.op.M = SY.to_indexable(self.cnst.op.M)
        M = self.cnst.op.M
        if CO.DEBUGLEVEL>2: t2 = time.time()
        self.Md = SY.to_solvable(M[:,self.cnst.dep])
        if CO.DEBUGLEVEL>2: t3 = time.time()
        # This step used to be longest but after changing the definition of to_indexable(m)
        # in symbolic_array from m.tolil() to m.tocsc() it became fast!
        self.Mi = SY.to_solvable(M[:,~self.cnst.dep])
        if CO.DEBUGLEVEL>2: t4 = time.time()
        # Check for conflicting/repeating BC:
        assert self.Md.shape[0]==self.Md.shape[1]
        if CO.DEBUGLEVEL>0:
            if np.linalg.matrix_rank(SY.to_dense(self.Md))<self.Md.shape[0]:
                raise Exception('Conflicting or repeating BC in field '+self.field.name)
        if CO.DEBUGLEVEL>2:
            t5 = time.time()
            print(CO.info('BCManager.freeze():'),
                  'Freezing BC for',self.field.name,', times: clean =',(t1-t0),': index =',(t2-t1),
                  '; dep =',(t3-t2),'; indep =',(t4-t3),'; check =',(t5-t4))
        self.cnst.frozen = True # ???
    @property
    def num(self):
        return self.cnst.op.M.shape[0]
    @property
    def frozen(self):
        return self.num==0 or self._frozen
    def apply(self):
        self.end_update()
        assert self.frozen
        # if self.field.trace:
        #     unset = ((~self.field.touched) & self.field.indep)
        #     if unset.any():
        #         s = self.field.name + '.bc.apply():\n\t'
        #         s += 'The following elements had not been updated before applying BC:\n\t'
        #         ii=np.where(unset)[0]
        #         s += '['
        #         s += ', '.join(str(self.field.restore_symb_index(i)) for i in ii)
        #         s += ']'
        #         raise Exception(s)
        if self.num > 0:
            dep = self.cnst.dep
            x = self.field.arr.flat
            x[dep] = - SY.solve(self.Md, self.cnst.op.V+self.Mi.dot(x[~dep]))
        # if self.field.trace:
        #     self.field.touched[:]=False
    def check(self):
        "Check if BC are satisfied numerically. Returns error (0 if satisfied)."
        assert self.frozen
        if self.num>0:
            x = self.field.arr.flat
            return np.max(np.abs(self.cnst.op(x)))
        else:
            return 0
    def print(self,file=sys.stdout):
        "Pretty print of BC's"
        if not self.frozen:
            raise Exception("Must freeze BC before printing them")
        # This function is ugly, must clean up
        name = self.field.symb.arr.ref.name
        print('Boundary conditions for symbolic array',name,file=file)
        def arg_str(x,mid=False):
            if x==0:
                return '' if mid else '0'
            return (' %+f ' if mid else '%f ') % (x,)
        def coef_str(x,ss,mid=False):
            if x==0:
                return ''
            if x == 1:
                s = ' + ' if mid else ''
            elif x==-1:
                s = ' - '
            else:
                s = ('%+f * ' if mid else '%f * ') % (x,)
            return s+ss
        def index_str(rii):
            return ', '.join(repr(i) for i in rii)
        for kc in range(len(self.is_variable)):
            isvar = self.is_variable[kc]
            print('BC #',kc,': ','variable' if isvar else 'nonvariable',', name="',
                  self.names[kc],'"',sep='',file=file)
            i = self.coords(kc)
            op = self.cnst.op[i]
            ind = self.cnst.ind[i]
            M = op.M.copy() # sy.to_dense( op.M.copy())
            n = M.shape[0]
            for k in range(n):
                ris = index_str(self.field.symb.ea_index(ind[k]))
                Mrow = SY.get_dense_row(M,k) # M[k,:]
                mainc = Mrow[ind[k]]
                Mrow[ind[k]]=0
                print('\t%s = ' % (coef_str(mainc,'%s[%s]' % (name,ris),False)),
                      end='',file=file)
                ii = np.where(Mrow)[0]
                mid = False
                for jj in ii:
                    rjs = index_str(self.field.symb.ea_index(jj))
                    print('%s' % (coef_str(-Mrow[jj],'%s[%s]' % (name,rjs),mid)),
                          end='',file=file)
                    mid = True
                print('%s' % arg_str(-op.V[k],mid),file=file)
    def __str__(self):
        sio=io.StringIO()
        self.print(file=sio)
        s=sio.getvalue()
        sio.close()
        return s
    pass

#%% Extended array with boundary conditions and symbolic operations
class Field(ExtendedArray):
    """Same as `extended_array` but with a name and boundary conditions enforced""" 
    def __init__(self, name, ncells, stags, nls, nus, use_array=None):
        """See extended_array.__init__ for most of the arguments. The only new one
        is `name` --- the name of the field."""
        super().__init__(ncells, stags, nls, nus)
        if use_array is None:
            self.arr = np.full(self.nts_real, fill_value=np.nan)
        else:
            self.arr = use_array
        #self.name = name
        # To store the bc and to record the symbolic operations
        self.symb = self.copy(is_flat=True)
        if CO.DEBUGLEVEL>3:
            t = time.time()
            print(' < field.__init__: alloc_symbolic ... ',end='',flush=True)
        self.symb.arr = SY.SymbolicArray(name, self.size) # constraints are handled by self.bc
        if CO.DEBUGLEVEL>3:
            print('done (',time.time()-t,') > ',flush=True)
        self.bc = BCManager(self)
        # Dependent values
        self.dep = self.copy()
        # NOTE: this is a link, not a copy
        self.dep.arr = self.symb.arr.ref.cnst.dep.reshape(self.nts)
    @property
    def name(self):
        return self.symb.arr.ref.name
    @property
    def indep(self):
        "Mostly just a forwarder"
        raise Exception('OBSOLETE! Use field.dep')
        return ~self.symb.arr.ref.cnst.dep
    def info(self):
        #return 'Field1D ' + self.name + super().info(name='')
        return super().info(name=self.name)
    pass

#%% Class that does not record BC but is responsible for solving for some other
class Solver:
    """We skip some equations, default - the ones that are dependent"""
    def __init__(self, lhs, ea_op, ea_skip=None):
        "ea_op is the symbolic operator, an `extended_array` with a symbolic arr"
        assert isinstance(ea_op, ExtendedArray) and isinstance(ea_op.arr, SY.ConstrainedExpression)
        assert isinstance(lhs, Field)
        assert(lhs.symb.arr.ref is ea_op.arr.ref)
        self.symb_op = ea_op.arr # need it because bc may change
        assert(lhs.bc.frozen)
        #self.lhs = lhs
        self.unknown_info = lhs.copy()  # shallow copy, just info
        self.rhs_info = ea_op.copy() # shallow copy, just info
        if ea_skip is None:
            skip = None
        else:
            assert isinstance(ea_skip, ExtendedArray) and same_ea_structure(self.rhs_info, ea_skip)
            skip = ea_skip.arr
        equations = self.symb_op.op if skip is None else self.symb_op.op[~skip.flatten()]
        self.skip = skip # save for future use
        self.padding = np.zeros((lhs.bc.num,))
        self.op_full = equations.cat(self.symb_op.ref.cnst.op)
        if not self.op_full.M.shape[0]==self.op_full.M.shape[1]:
            raise ValueError('# equations = '+str(self.op_full.M.shape[0])+\
                             ' is not the same as # variables = '+str(self.op_full.M.shape[1])+\
                             ' in solver for '+self.symb_op.ref.name)
        self.n_rhs = self.op_full.num_eqs - lhs.bc.num
    def solve_full(self, lhs, rhs_ea):
        "With BC"
        lhs.bc.end_update() # cleanup the bc
        assert isinstance(rhs_ea, ExtendedArray)
        assert same_ea_structure(self.rhs_info,rhs_ea)
        #assert rhs_ea1.arr.size == self.n_rhs
        if CO.DEBUGLEVEL>3:
            t0=time.time()
        tmp = self.symb_op.ref.cnst.op.V
        if any(lhs.bc.is_variable):
            self.op_full.V[-len(tmp):]=tmp
        real_rhs = rhs_ea.arr.flat if self.skip is None else rhs_ea.arr[~self.skip]
        lhs.arr[...] = self.op_full.solve(np.hstack((real_rhs, self.padding))).reshape(lhs.nts)
        if CO.DEBUGLEVEL>3:
            print(CO.info('solver.solve_full:'),'Solve time =',time.time()-t0)
    def print(self, file=sys.stdout):
        print('Solver for the unknown', self.symb_op.ref.name, 'which is an',\
              self.unknown_info, file=file)
        print('\twith RHS as an', self.rhs_info, file=file)
        print('\tThere are',self.n_rhs,'independent variables', file=file)
        print('\twhich includes',self.op_full.num_eqs, 'total variables minus',
              len(self.padding), 'linear constraints', file=file)
    def __repr__(self):
        sio=io.StringIO()
        self.print(file=sio)
        s=sio.getvalue()
        sio.close()
        return s        
    pass
