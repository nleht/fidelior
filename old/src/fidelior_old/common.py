#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 16:45:54 2016

Some general-purpose routines to be used with TRUMP package

@author: nle003
"""

import time
import numpy as np
import functools
import numbers

#%% Global variables

def set_global_debug_level(new_debug_level):
    """Debug levels are:
        
        0 - no error checking at all (DANGEROUS!)
        1 - user input parameter error checking (recommended)
        
        2 - internal error checking but no diagnostic printout
        
        3 - some diagnostic printout
        
        4 - detailed diagnostic output
        
        5 - very detailed output, for debugging details, for the developer only!!!
    """
    global DEBUGLEVEL
    DEBUGLEVEL = new_debug_level

DEBUGLEVEL=1

#%% Terminal colors
# See https://www.digitalocean.com/community/tutorials/how-to-customize-your-bash-prompt-on-a-linux-vps
# \e -- same as \033 is escape char
# \e[ -- start of color info
# m\] -- end of color info
# Color info: 0,1,4 - normal,bold or underlined text; 3x -- foreground color; 4x -- background color, where
# x = 0-7 is black, red, green, yellow, blue, purple, cyan, white
# \[\e]0; -- start of the terminal title (not displayed in the normal prompt)
# \a\] -- end of the terminal title

def set_colorful(b=True):
    global __colors, COLORFUL
    COLORFUL=b
    if b:
        __colors = {'black': '\x1b[30m',
            'red': '\x1b[31m',
            'green': '\x1b[32m',
            'yellow': '\x1b[33m',
            'blue': '\x1b[34m',
            'magenta': '\x1b[35m',
            'cyan': '\x1b[36m',
            'white': '\x1b[37m',
            'end': '\x1b[0m',
            'bold': '\x1b[1m',
            'underline': '\x1b[4m'}
    else:
        __colors = {'black':'', 'red':'', 'green':'', 'yellow':'',
            'blue':'', 'magenta':'', 'cyan':'', 'white':'',
            'end':'', 'bold':'', 'underline':''}
set_colorful()

def colorize(s, color=None, bold=False, underline=False):
    if color is not None:
        s = s + __colors['end']
        if bold:
            s = __colors['bold'] + s
        if underline:
            s = __colors['underline'] + s
        s = __colors[color] + s
    return s

# Some shortcuts
BOLD = True
# Set to False if too bright

def red(s):
    return colorize(s, color='red', bold=BOLD)

def blue(s):
    return colorize(s, color='blue', bold=BOLD)

def yellow(s):
    return colorize(s, color='yellow', bold=BOLD)

def green(s):
    return colorize(s, color='green', bold=BOLD)

def white(s):
    return colorize(s, color='white', bold=BOLD)
        
def info(s):
    return green(s)
def alert(s):
    return yellow(s)
def error(s):
    return red(s)


#%% IO routines
def make_indented_printable(cls, multiline=True, replace_repr=True):
    "A general-purpose output utility"
    def indented_repr(obj,level=1):
        if multiline:
            tabs = '\n' + ('\t' * level)
        else:
            tabs = ' '
        if not hasattr(obj,'_indented_repr'):
            s=repr(obj).split('\n')
            # if one line, just s
            # if many, <CR>+tabulated output
            if len(s)==1:
                return s[0]
            else:
                return tabs + tabs.join(s)
        outputs = []
        for k in sorted(obj.__dict__):
            if k[0]=='_':
                outputs.append('%s = <%s>' % (k, obj.__dict__[k].__class__.__name__))
            else:
                outputs.append('%s = %s' % (k, indented_repr(obj.__dict__[k], level+1)))
        s = (','+tabs).join(outputs)
        if '\n' in s:
            return ('%s(' + tabs + '%s)') % (obj.__class__.__name__, s)
        else:
            return  '%s(%s)' % (obj.__class__.__name__, s)
    setattr(cls,'_indented_repr',indented_repr)
    if replace_repr:
        setattr(cls,'__repr__',indented_repr)

#%% Various NumPy amends
def list_where(list_, biglist_):
    """addr = list_where(list_, biglist_)
    
    Addresses of elements of `list_` `in biglist_`"""
    return np.array([biglist_.index(element) for element in list_])

def list_union(list1, list2):
    """list_combined, address1, address2 = list_union(list1, list2)
    
    A useful general-purpose utility: a union of two lists, with addresses"""
    list_ = list1.copy()
    addr1 = np.arange(len(list1))
    addr2 = np.zeros((len(list2),), dtype=np.int)
    for k, element in enumerate(list2):
        if not element in list_:
            list_.append(element)
        addr2[k] = list_.index(element)
    return list_, addr1, addr2

def ravel_multi_index(index, shape):
    """Convert a multidimensional index into one-dimensional.
    Unfortunately, numpy.ravel_multi_index does not work on slices.
    """
    if DEBUGLEVEL>4:
        print(info('ravel_multi_index:'), 'index =', index,', shape =', shape)
    if True:
        tot_index = np.arange(np.prod(shape)).reshape(shape)
        return tot_index[index].flatten()
    else:
        tot_index = np.zeros(shape, dtype=np.bool)
        tot_index[index]=True
        return tot_index.flatten()

def broadcast_ravel_multi_index(*index_shape_s, extras=False):
    """
    Calculate raveled index into each array (i.e., index into a flattened array)

    Parameters
    ----------
    *index_shape_s : args
        One or more (index, shape) tuples.
    index : tuple (multi-index) with ``None``s in some positions
        Index into an array. `None` present in i1 indicate dimensions which should be broadcast.
    shape : tuple
        Shape of an array

    Returns
    -------
    result: tuple
        A tuple of arrays with indices.
    reps : tuple (optional, if extras==True)
        Number of repetitions of each array
    """
    subshapes = ()
    tot_indices =[]
    for index, shape in index_shape_s:
        #print('index =',index,', shape =',shape)
        tot_index = np.arange(np.prod(shape)).reshape(shape)
        tot_indices.append(tot_index[index])
        subshapes += (tot_index[index].shape,)
    #print('totindices =', tot_indices)
    #print('subshapes =', subshapes)
    conj_reps = ()
    for elem_dims in zip(*subshapes):
        #print('elem_dims =', elem_dims)
        res_dim = np.max(elem_dims)
        for k in elem_dims:
            assert k==1 or k==res_dim
        conj_reps += (tuple(res_dim if k==1 else 1 for k in elem_dims),)
    reps = tuple(zip(*conj_reps))
    #print('reps =',reps)
    result = tuple(np.tile(tot_index, rep).flatten() for tot_index, rep in zip(tot_indices, reps))
    if extras:
        return result, reps
    else:
        return result

def slice_to_index_array(i,n):
    "Convert a slice or a single index to an array of indices"
    #print(np.repeat(np.arange(n)[i],1,axis=0))
    if True:
        return np.repeat(np.arange(n)[i],1,axis=0)
    else:
        # another way to do it
        tmp = np.zeros((n,),dtype=np.bool)
        tmp[i]=True
        return tmp

#def replace_in_tuple(t,i,new):
#    return t[:i]+(new,)+t[i+1:]

# Input type checks, should raise ValueError if not compliant

def is_integer_scalar(n):
    return np.isscalar(n) and isinstance(n,numbers.Integral)

def is_real_vector(x):
    return isinstance(x,np.ndarray) and len(x.shape)==1 and np.isreal(x).all()

def is_real_scalar(x):
    return np.isscalar(x) and np.isreal(x)

def ndgrid(*arrays):
    return tuple(np.meshgrid(*arrays,indexing='ij'))

def all_unique(a):
    return len(np.unique(a))==len(a)

def push_axes_forward(ndim, chosen):
    """Returns axis permutation which pushes the chosen axes forward
    and inverse axis permutation (maybe a bit lame, but I need it!)."""
    assert all_unique(chosen)
    direct = list(chosen) + [i for i in range(ndim) if i not in chosen]
    inverse = list(np.argsort(direct))
    return direct, inverse

def choose(index, tensor, axis=0, which=None):
    """Same as np.choose, but can choose along any axis, not just the first one.
    Also, the axes of the index can be arbitrary."""
    if axis==0 and which is None:
        return np.choose(index, tensor)
    assert which is not None
    which = list(which)
    assert all_unique([axis]+which)
    # The automatic determination of "which" can wait
    assert index.ndim==len(which)
    # For broadcasting, the following assert does not hold
    #assert np.array_equal(np.array(tensor.shape)[which], np.array(index.shape))
    direct, inverse = push_axes_forward(tensor.ndim, [axis] + which)
    # The first axis is dropped, so it must be removed from the "inverse"
    inverse_compressed = [i-1 for i in inverse if i>0]
    tensor_transposed = tensor.transpose(direct)
    extra = len(tensor.shape)-len(index.shape)-1
    assert extra>=0
    index_reshaped = index.reshape(index.shape + (1,)*extra)
    #index_broadcast = np.broadcast_to(index, tensor_transposed.shape[1:])
    tmp = np.choose(index_reshaped, tensor_transposed)
    return tmp.transpose(inverse_compressed)
        

def invert_many_matrices(A, axes=(0,1), alg=1, save_A=True, out=None, atol=1e-12):
    """B = invert_many_matrices(A, alg=1, save_A=True, out=None, atol=1e-12)
    Gaussian elimination, now with pivoting!
    Algorithms:
        0 -- just do matrices one-by-one,
        1 -- vectorize as much as possible (default),
        2 -- partially vectorize---may be faster than alg=1 for small matrices (K<50)
    """
    assert len(axes)==2
    assert axes[0]!=axes[1]
    direct, inverse = push_axes_forward(A.ndim, axes)
    return _invert_many_matrices_first_axes(A.transpose(direct),
                                alg=alg, save_A=save_A, out=out, atol=atol).transpose(inverse)

def _invert_many_matrices_first_axes(A, alg=1, save_A=True, out=None, atol=1e-12):
    "Same as invert_many_matrices, but the matrices are stored along the first two axes"
    if DEBUGLEVEL>2:
        t0 = time.process_time()
    s = A.shape
    K = s[0]
    assert s[1]==K
    N = np.prod(s[2:])
    st = (K,K,N)
    A = A.reshape(*st) # creates a different view of A!
    if out is None:
        B = A.copy() # instead of np.zeros(st, dtype=A.dtype)
    else:
        B = out.reshape(st)
    if alg==0:
        for p in range(N):
            B[:,:,p] = np.linalg.inv(A[:,:,p])
        if DEBUGLEVEL>2:
            t = time.process_time()-t0
            if t>1:
                print(info('invert_many_matrices:'), 'alg =', alg, ', time =', t)
        return B.reshape(s)
    # Vectorized Gaussian elimination
    if save_A:
        # We are going to destroy the original A
        A = A.copy()
    B[...] = np.tile(np.eye(K)[:,:,None],[1,1,N])
    columns = np.zeros((K,), dtype=np.int64)
    for k in range(K):
        # Pivoting: find a column in row k which has the highest abs value
        kp = np.argmax(np.min(np.abs(A[k,:,:]),axis=1))
        columns[k] = kp
        scale = A[k,kp:kp+1,:].copy()
        if np.min(np.abs(scale))<atol: #np.allclose(np.min(np.abs(scale)),0):
            print(scale)
            print('Min scale =',np.min(np.abs(scale)))
            raise ValueError('Row '+str(k)+': Singular Matrix!')
        A[k,:,:] /= scale
        B[k,:,:] /= scale
        if alg==1:
            # Vectorize as much as possible (VAMAP)
            Arow = A[k,:,:].copy() # Save the values. Otherwise have to dance around row k
            Brow = B[k,:,:].copy()
            coef = A[:,kp:kp+1,:].copy()
            A -= Arow*coef
            B -= Brow*coef
            A[k,:,:] = Arow
            B[k,:,:] = Brow
        elif alg==2:
            # A simple cycle. Not much slower, at least for K<50, could be even faster!
            Arow = A[k,:,:]
            Brow = B[k,:,:]
            for row in range(K):
                if row==k:
                    continue
                coef = A[row,kp:kp+1,:].copy()
                A[row,:,:] = A[row,:,:] - Arow*coef
                B[row,:,:] = B[row,:,:] - Brow*coef
    pivot = np.zeros((K,), dtype=np.int64)
    pivot[columns] = np.arange(K)
    if DEBUGLEVEL>2:
        t = time.process_time()-t0
        if t>1:
            print(info('invert_many_matrices:'), 'alg =', alg, ', time =', t)
    return B[pivot,:,:].reshape(s)

#%%
# Decorator makers

_info_unary = """

TRUMP Note
==========

This is a TRUMP-overriden unary function. It may take an ``extended_array`` as
the first argument, and return an ``extended_array``."""

_info_binary = """

TRUMP Note
==========

This is a TRUMP-overriden binary function. It may take ``extended_array`` s as
the first two arguments, and return an ``extended_array``."""

_info_default = """

TRUMP Note
==========

This is a TRUMP-overriden variable-arg function. It may take TRUMP objects as
arguments in the same way as NumPy does, and return TRUMP objects."""

# See https://numpy.org/devdocs/reference/ufuncs.html
UFUNC_ATTRIBUTES = ['nin','nout','nargs','ntypes','types','identity','signature',
                    'reduce','accumulate','reduceat','outer','at']

def update_wrapper(newfunc, func, nin, copy_info):
    if copy_info:
        functools.update_wrapper(newfunc, func)
        if nin==1:
            info = _info_unary
        elif nin==2:
            info = _info_binary
        else:
            info = _info_default
        if newfunc.__doc__ is None:
            newfunc.__doc__ = info
        else:
            newfunc.__doc__ += info
    else:
        newfunc.__name__ = func.__name__
    # Copy ufunc attributes
    if func.__class__ is np.ufunc:
        for attr in UFUNC_ATTRIBUTES:
            setattr(newfunc,attr,getattr(func,attr))

#%% Decorate NumPy functions
class NumpyOverrideContext:
    context_stack = [] # previous overrides
    nums_instances = {} # make sure there is only one with each name
    def __init__(self,name):
        tmp = self.__class__.nums_instances.get(name)
        if tmp is None:
            self.__class__.nums_instances[name] = 1
        elif tmp>0:
            raise TypeError('Cannot have more than one ' + self.__class__.__name__ +
                            ' with name ' + name)
        self.name = name
        self.enter_level = 0
        self.ea_functions = {}
        self.saved_np_functions={}
    def _PS(self):
        return info(self.name+':')
    def register(self,func_name, new_func):
        self.ea_functions[func_name] = new_func
    def __enter__(self):
        # can return something to be use in "with .. as .. " block
        if DEBUGLEVEL>4:
            print(self._PS(), 'Entering, level =', self.enter_level)        
        self.enter_level += 1
        if self.enter_level > 1:
            if DEBUGLEVEL>2:
                print(self._PS(),
                      alert('Nested entering (level=' + str(self.enter_level)+'), doing nothing'))
            return
        # The main stuff occurs here, i.e., entering the context
        if DEBUGLEVEL>2:
            print(info(self.name+':'),'overriding NumPy functions')
        for func_name in self.ea_functions:
            if func_name not in self.saved_np_functions:
                if DEBUGLEVEL>3:
                    print(self._PS(),'overriding',func_name)
                self.saved_np_functions[func_name] = getattr(np, func_name)
                setattr(np, func_name, self.ea_functions[func_name])
        if DEBUGLEVEL>4:
            print('Appending',self.name,'to stack =',self.__class__.context_stack)
        self.__class__.context_stack.append(self.name)
    def enter(self):
        self.__enter__()
    def __exit__(self, type, value, traceback):
        if DEBUGLEVEL>4:
            print(self._PS(), 'Exiting, level =', self.enter_level)
        if self.enter_level < 1:
            print(self._PS(),alert('Exiting non-entered context, ignoring'))
            return
        if self.enter_level > 1:
            if DEBUGLEVEL>2:
                print(self._PS(),
                      alert('Nested exiting (level=' + str(self.enter_level) + '), doing nothing'))
            self.enter_level -= 1
            return
        prev_name = self.__class__.context_stack.pop()
        if prev_name != self.name:
            print(self._PS(),error('Cannot exit, must exit '+prev_name+' first'))
            self.__class__.context_stack.append(prev_name)
            return
        for fun in self.saved_np_functions:
            setattr(np,fun, self.saved_np_functions[fun])
        self.saved_np_functions={}
        self.enter_level -= 1
        if DEBUGLEVEL>2:
            print(self._PS(),'NumPy functions restored')
        if DEBUGLEVEL>3:
            print(self._PS(),'Exit parameters: type =', type, ', value =',value,
                  ', traceback =', traceback)
    def exit(self):
        self.__exit__(None, None, None)
    def complete_exit(self):
        "Exit even if self.enter_level>1"
        self.enter_level = min(1, self.enter_level)
        self.exit()
    #def __del__(self):
    #    "Not sure if this even needed"
    #    if DEBUGLEVEL>2:
    #        print('Deleting',self.__class__.__name__)
    #    if self.enter_level>0: self.complete_exit()
    #    self.__class__.nums_instances[self.name] -= 1
 
#%% Customizing the arrays so that they do not take over control unnecessarily
class CustomNDArray(np.ndarray):
    """A customized NumPy ndarray, in order to make operations like v * f and
    v + f work with ndarray v and arbitrary class f.
    NOTE: f * v and f + v work fine even without it
    """
    overloaded_types=[]
    def __mul__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_):
                if DEBUGLEVEL>4:
                    print(info('custom:'),'calling rmul from',type_.__name__)
                return obj.__rmul__(self)
        if DEBUGLEVEL>4:
            print(info('custom:'),'calling own mul for',obj.__class__.__name__)
        return super().__mul__(obj)
    def __add__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_):
                if DEBUGLEVEL>4:
                    print(info('custom:'),'calling radd from',type_.__name__)
                return obj.__radd__(self)
        if DEBUGLEVEL>4:
            print(info('custom:'),'calling own add for',obj.__class__.__name__)
        return super().__add__(obj)
    def __sub__(self,obj):
        for type_ in self.__class__.overloaded_types:
            if isinstance(obj,type_):
                if DEBUGLEVEL>4:
                    print(info('custom:'),'calling rsub from',type_.__name__)
                return obj.__rsub__(self)
        if DEBUGLEVEL>4:
            print(info('custom:'),'calling own add for',obj.__class__.__name__)
        return super().__sub__(obj)
    def allocate(self, shape):
        return CustomNDArray(shape)
    def decustomize(self):
        return decustomize(self)

def reset_overloaded_types():
    CustomNDArray.overloaded_types = []

def register_customization_for(type_):
    # If there is already with this name, replace
    try:
       ii = CustomNDArray.overloaded_types.index(type_)
    except ValueError:
        if DEBUGLEVEL>2:
            print('Registering custom operations ( ndarray %',type_.__name__,')')
        CustomNDArray.overloaded_types.append(type_)
    else:
        if DEBUGLEVEL>2:
            print('Re-registering custom operations ( ndarray %',type_.__name__,')')
        CustomNDArray.overloaded_types[ii]=type_

# An optimized version
def customize(arr):
    if isinstance(arr, CustomNDArray):
        return arr
    elif isinstance(arr, np.number): # a special NumPy scalar type (not Python scalar number!)
        return arr.item() # was: np.asscalar(arr), but is deprecated
    elif isinstance(arr,numbers.Number):
        return arr
    elif isinstance(arr, np.ndarray):
        return arr.view(CustomNDArray)
    else:
        return arr # do nothing, we already use a custom array

def decustomize(carr):
    if isinstance(carr, CustomNDArray):
        #tmp=np.ndarray(carr.shape,dtype=carr.dtype)
        #tmp[...]=carr
        #return tmp
        return np.array(carr) # subok = False
    else:
        return carr
