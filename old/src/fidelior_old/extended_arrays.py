#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 29 13:33:08 2016

Extended array with a non-Python indexing system, which is used in other languages like
MATLAB and Fortran.

Negative indices give the element in the extension on the lower side; and the "stop" element
of a slice is the last index (like in MATLAB).

Geometry-free extended_array now!
    
@author: nle003
"""

#%% Imports
import numpy as np
from scipy.interpolate import interpn
import numbers
from . import common as CO

#%% EndIndex, SpanIndex and HalfIndex
class EndIndex:
    def __init__(self,i=0):
        self.i = i
    def __add__(self,i):
        "Returns an index"
        if isinstance(i,numbers.Integral):
            return EndIndex(self.i+i)
        else:
            return i.__radd__(self)
    def __sub__(self,i):
        return self + (-i)
    def __eq__(self, o):
        return (type(self)==type(o) and (self.i==o.i))
    def __call__(self,ea):
        assert isinstance(ea, ExtendedArray)
        return tuple(self.i + ea.ncells[a] for a in range(ea.ndim) if ea.is_dim[a])
    def __repr__(self):
        if self.i==0:
            return 'end'
        else:
            return 'end{:+d}'.format(self.i)
end = EndIndex() # Should be a constant! But in Python, no way to enforce it

class SpanIndex:
    "The whole range of value along an axis"
    def __call__(self, ea, axis=None):
        assert isinstance(ea, ExtendedArray)
        if axis is None:
            return tuple(self(ea, a) for a in range(ea.ndim))
            # return tuple(slice(-ea.nls[a] + (half if ea.stags[a]==1 else 0),
            #                end+ea.nus[a]-(half if ea.stags[a]==1 else 0))
            #                for a in range(ea.ndim))
        else:
            if ea.stags[axis] is None:
                assert ea.nls[axis] is None and ea.nus[axis] is None
                return None
            else:
                return slice(-ea.nls[axis] + (half if ea.stags[axis]==1 else 0),
                           end+ea.nus[axis]-(half if ea.stags[axis]==1 else 0))
    def __repr__(self):
        return 'span'
span = SpanIndex() # another global, like "end"

half_string = '½' # customizeable global
class HalfIndex:
    """Half-index like `k+1/2`, for the staggered arrays.
    Really, just a decorated integer or EndIndex"""
    def __init__(self, whole_part=0):
        self.int = whole_part # The "old" value of index into staggered array
        # The index on the grid is self.int + (1/2)
    def __float__(self):
        return self.int + 0.5
    def __neg__(self):
        return HalfIndex(-self.int - 1)
    def __pos__(self):
        return self
    def __radd__(self, i):
        if isinstance(i, HalfIndex):
            raise TypeError('cannot add half-index to half-index')
        return HalfIndex(i + self.int) # can be anything that supports __add__
    def __rsub__(self, i):
        return (i + (-self))
    def __eq__(self, o):
        return (type(self)==type(o) and (self.int==o.int))
    def _out(self, i, sign=False):
        """Fancy printout of i+½. More complicated than I thought.
        Just a function, does not use 'self'"""
        sign_str = ('+' if sign else '')
        if i < 0:
            if i+1==0:
                return '-' + half_string
            else:
                return '-' + repr(-(i+1)) + half_string
        elif i==0:
            return sign_str + half_string
        else:
            return sign_str + repr(i) + half_string
    def __repr__(self):
        if isinstance(self.int, numbers.Integral):
            return self._out(self.int, sign=False)
        elif isinstance(self.int, EndIndex):
            return 'end' + self._out(self.int.i, sign=True)
        else:
            return repr(self.int) +' + ' + half_string
    pass

# Should it be possible to use expressions like (half+half)?
# Allow the flexibility!
def _HalfIndex_add(self, hi):
    if not isinstance(hi, HalfIndex):
        #raise IndexError('Please use "half" as the last term')
        return hi + self
    return self.int + hi.int + 1

def _HalfIndex_sub(self, hi):
    return self + (-hi)

def allow_half_plus_half(b):
    #global half
    if b:
        # Load methods __add__ and __sub__ for HalfIndex class
        if hasattr(HalfIndex,'__add__'):
            assert hasattr(HalfIndex,'__sub__')
            return
        setattr(HalfIndex,'__add__',_HalfIndex_add)
        setattr(HalfIndex,'__sub__',_HalfIndex_sub)
    else:
        # Erase methods __add__ and __sub__ for HalfIndex class
        if not hasattr(HalfIndex,'__add__'):
            assert not hasattr(HalfIndex,'__sub__')
            return
        delattr(HalfIndex,'__add__')
        delattr(HalfIndex,'__sub__')
    # This seems to be unnecessary -- I am not sure how Python works
    #half = HalfIndex()

half = HalfIndex() # another global, like "end"
#allow_half_plus_half(False)

#%% Reslicing routines
def to_i(ei, ncell):
    "First stage of converting a 'symbolic' index to index into the extended_array storage"
    if isinstance(ei, EndIndex):
        return ei.i + ncell # ei.i + grid.num_cells
    elif isinstance(ei, HalfIndex):
        return to_i(ei.int, ncell)
    else:
        return ei
    pass

def symbolic_index_to_double(n, ea=None, axis=None):
    "Useful for interpolating"
    if isinstance(n, EndIndex):
        return np.double(n(ea)[axis])
    if isinstance(n, HalfIndex):
        return symbolic_index_to_double(n.int, ea, axis) + 0.5
    return np.double(n)

def info_index(nc, st, nl, nu):
    if st is None:
        return 'None'
    if st==1:
        return str(-nl+half) + ':' + str(nc) + half._out(nu-1, sign=True)
    else:
        return str(-nl) + ':' + str(nc) + '{:+d}'.format(nu)

# def info_index_old(nc, st, nl, nu):
#     n = None if st is None else nc + 1 - st
#     t = 'x' if st is None else ('n' if st==0 else 'c')
#     return ('x' if nl is None else repr(nl)) + '|' + \
#         ('x' if n is None else repr(n)) + '|' + \
#             ('x' if nu is None else repr(nu)) + '<' + t + '>'
#     #return '{:d}|{:d}|{:d}<{:s}>'.format(nl,n,nu,'n' if s==0 else 'c')

def reslice(i, nc, st, nl, nu, a=None):
    """numpy_index = reslice(ea_index, nc, st, nl, nu, a=None)
    Convert extended_array slice into a NumPy or Python array slice.
    The axis is given only for reference (for IndexError output).
    
    Parameters
    ----------
    i : slice or number or 1D array
        Index into extended_array.
    nc : int
        Number of cells.
    st: 0 or 1
        The staggering indicator along the given axis.
    nl : int
        Number of lower ghost cells.
    nu : int
        Number of upper ghost cells.
    a : int, optional
        The axis. The default is None.
    
    
    Who would have thought that arr[0:1:1] is arr[0] but arr[0:-1:-1] is empty?
    And other idiotisms of Python index system. Good we don't have to convert the other
    way around.
    Added on 2017/11/28: allow returning empty array for indices like [11:10] or [13:11:2]
    but not [12:10] or [14:11:2] (the negative difference is too high for step).
    """
    if i is span:
        return slice(None)
    n = nc + 1 - st # number of points on the 'inside' part (without ghost cells)
    nt = n + nl + nu # total number of points
    text = ' out of bounds [{}:{}]'.format(-nl, n+nu-1)
    # The axis number information is only used here, for debugging output
    if a is not None:
        text += ' on axis {}'.format(a)
    if isinstance(i, slice):
        # Check the consistency first
        if (i.start is not None and int(isinstance(i.start, HalfIndex)) != st) or \
           (i.stop is not None and int(isinstance(i.stop, HalfIndex)) != st):
            raise IndexError('slice '+repr(i)+' must match stag = '+str(st))
        step = (1 if i.step is None else i.step)
        sign = (1 if step>0 else -1)
        if step == 0: raise ValueError('slice step is zero')
        # START
        if i.start is None:
            start = (nl if step>0 else nl+n-1)
        else:
            start = nl + to_i(i.start, nc)
        if start < 0 or start >= nt:
            raise IndexError('start {}'.format(start-nl)+text)
        # STOP
        if i.stop is None:
            stop = (nl+n if step>0 else nl-1)
        else:
            stop = nl + to_i(i.stop, nc) + sign
        if stop > nt or stop < -1:
            raise IndexError('stop {}'.format(stop-nl-sign)+text)
        if (step>0 and start>=stop+step) or (step<0 and start<=stop+step):
            gt_or_lt = (' > ' if step>0 else ' < ')
            delta = '{:+d}'.format(-step)
            raise ValueError(('(start=={})'+delta+gt_or_lt+'(stop=={}), step=={}').format(
                             start-nl,stop-nl-sign,step))
        if stop == -1: # This is tricky, we cannot have -1
            stop = None
        i = slice(start, stop, i.step)
        return i
    if isinstance(i, HalfIndex) or (isinstance(i, CO.CustomNDArray) and \
                                    all(isinstance(i0, HalfIndex) for i0 in i)):
        # The array index treatment is kinda lame for now :(
        if st != 1:
            raise IndexError('index '+repr(i)+' must match stag = '+str(st))
        if isinstance(i, CO.CustomNDArray):
            i = CO.CustomNDArray([i0.int for i0 in i])
        else:
            i = i.int
    elif st != 0:
        raise IndexError('index '+repr(i)+' must match stag = '+str(st))
    if isinstance(i, numbers.Integral) or isinstance(i, EndIndex):
        i = to_i(i, nc)
        if (i<-nl or i>=n+nu):
            raise IndexError('index {}'.format(i)+text)
        i += nl
        #print('i=',i)
    elif isinstance(i,np.ndarray):
        assert i.dtype is np.dtype('int')
        i = i.copy() + nl # do not mess up the original
    else:
        raise IndexError('unimplemented index type')
    return i

def ea_view_slice(s, nc, stag, nl, nu, a=None):
    """
    Parameters
    ----------
    s : slice
        Index into extended_array, from somewhere close to beginning to close to `end`.
    nc : int
        Number of cells.
    st: 0 or 1
        The staggering indicator along the given axis.
    nl : int
        Number of lower ghost cells.
    nu : int
        Number of upper ghost cells.
    a : int, optional
        The axis. The default is None.

    Raises
    ------
    IndexError
        If the index is incorrect.

    Returns
    -------
    nl_new, nu_new, s_new
        Parameters for the ea_view.

    """
    if s is None:
        return (None, None, None)
    if s is span:
        return (nl, nu, slice(None))
    if not isinstance(s,slice):
        raise IndexError('EA view can be only a slice')
    if not (s.step is None or s.step==1):
        raise IndexError('EA view can only be forward')
    if not (s.start is None or (stag==0 and isinstance(s.start,numbers.Integral)) or \
            (stag==1 and isinstance(s.start, HalfIndex)
                and isinstance(s.start.int, numbers.Integral)) ):
        raise IndexError('EA view start index is invalid')
    if not (s.stop is None or (stag==0 and isinstance(s.stop,EndIndex))  or \
            (stag==1 and isinstance(s.stop, HalfIndex) and isinstance(s.stop.int, EndIndex)) ):
        raise IndexError('EA view stop index is invalid')    
    s_new = reslice(s, nc, stag, nl, nu, a=a)
    nl_new = nl - s_new.start
    nt = nc + 1 - stag + nl + nu # total
    nu_new = nu - (nt  - s_new.stop)
    return (nl_new, nu_new, s_new)

#%% Extended Array narrowed view
class ExtendedArrayView:
    r"""
    An object that allows limited subindexing of an `extended_array`
    without copying. Indexing returns an extended_array object.

    Example
    -------
    
    >>> ncells = (10,)
        rho = Field('rho', ncells, stags=(0,), nls=(1,), nus=(1,))
        phi = Field('phi', ncells, stags=(0,), nls=(0,), nus=(0,))
        phi.bc[0]=0; phi.bc[end]=0; phi.bc.freeze()
        rho[:] = 1
        laplacian_solver = solver(phi, -n_dif(c_dif(phi.symb)))
        laplacian_solver.solve_full(phi, rho.view[1:end-1])
        plt.plot(grid.x(0), phi[:])
    """
    def __init__(self,ea):
        self.ea = ea
    def __getitem__(self,sNreal):
        "Like getitem (with limited capability), and return an extended_array"
        if isinstance(sNreal,tuple):
            Nreal = len(sNreal)
        else:
            Nreal = 1
            sNreal = (sNreal,)
        assert Nreal==self.ea.ndim_real
        sNreal_iter = iter(sNreal)
        sN = tuple(next(sNreal_iter) if self.ea.is_dim[a] else None for a in range(self.ea.ndim))
        tmp = tuple(ea_view_slice(
                s, self.ea.ncells[a], self.ea.stags[a], self.ea.nls[a], self.ea.nus[a], a)
                for (a,s) in enumerate(sN))
        nls_new, nus_new, ss_new = zip(*tmp)
        ss_new = tuple(s for s in ss_new if s is not None)
        res = ExtendedArray(
                self.ea.ncells, self.ea.stags, nls_new, nus_new, is_flat=self.ea.is_flat)
        res.arr = self.ea.arr[self.ea.adjust_for_flat(ss_new)]
        return res


#%% Extended Array
class ExtendedArray:
    r"""ExtendedArray(ncells, stags, nls, nus, is_flat=False, is_constant=False)
    
    An array with MATLAB-type indexing of arbitrary dimension, with an arbitrary number
    of ghost cells on lower (`nls`) and upper (`nus`) ends. It is linked to an
    `ndim`-dimensional grid.
    
    For ``stag==0`` along a certain dimension, it is defined on the nodes of the grid; for
    ``stag==1`` at the centers of grid cells.
    
    Parameters
    ----------
    
    ncells : tuple(ndim)
        Geometry (number of cells in each direction)
    stags : tuple(ndim)
        A ``tuple`` of zeros or ones; `ndim` is the dimensionality of the `extended_array`.
    nls : tuple(ndim)
        Small integers giving number of ghost cells on the lower end (may be negative)
    nus : tuple(ndim)
        Same, on the higher end.
    trace : optional
        For debugging, not currently used.

    Attributes
    ----------
    
    arr : ndarray
        A float (by default) ``numpy`` array of arbitrary dimension or a ``SymbolicArray``
        from trump.symbolic_array
    
    is_flat : bool
        Whether the storage is 1D (useful for symbolic extended arrays).
    
    Methods
    -------
    
    copy() :
        Shallow copy

    See Also
    --------
    
    field :
        A named `extended_array` allowing symbolic operations
    
    geometry_abstract, geometry_flat : 
        Used by `extended_array`
    
    grid :
        Used by `geometry_*`

    Notes
    -----
    The dimensionality is determined from the given `geometry`.

    Examples
    --------
    These are written in doctest format, and should illustrate how to
    use the function.

    >>> gr = Grid(num_cells=5, delta=0.1, start=0)
    >>> geom = geometry_flat((gr,))
    >>> f = ExtendedArray(geom, stags=(0,), nls=(1,), nus=(1,))
    
    then
    
    >>> f.alloc(np.double) # unset array
    
    or set it explicitly in the beginning
    
    >>> f.arr = np.arange(np.prod(f.nts)).reshape(f.nts)
    >>> print(f)
    extended_array [1|6|1<n>]: [ 0. [| 1. 2. 3. 4. 5. 6. |] 7. ]
    """
    # After closing class docstring, there should be one blank line to
    # separate following codes (according to PEP257).
    # But for function, method and module, there should be no blank lines
    # after closing the docstring.
    pass
    def __init__(self, ncells, stags, nls, nus, is_flat=False, is_constant=False):
        assert not ncells is None and not stags is None and not nls is None and not nus is None
        assert isinstance(ncells, tuple)
        self.ncells = ncells
        self.ndim = len(ncells)
        assert isinstance(stags, tuple) and len(stags)==self.ndim
        assert isinstance(nls, tuple) and len(nls)==self.ndim
        assert isinstance(nus, tuple) and len(nus)==self.ndim
        self.stags = stags
        # Dimension mask
        self.is_dim = np.array([stag is not None for stag in stags])
        self.ndim_real = np.sum(self.is_dim)
        #self.nts = tuple(None if not self.is_dim[a] else ns[a] + nls[a] + nus[a]
        #                 for a in range(self.ndim))
        self.nts = tuple(None if not self.is_dim[a] else ncells[a] + 1 - stags[a] + nls[a] + nus[a]
                         for a in range(self.ndim))
        self.nts_real = tuple(np.array(self.nts)[self.is_dim])
        self.size = np.prod(self.nts_real)
        # self.shape is the shape of self.arr
        if is_flat:
            self.shape = (self.size,)
        else:
            self.shape = self.nts_real
        # Want to get rid of this:
        #self.ns = tuple(None if not self.is_dim[a] else ncells[a] + 1 - stags[a]
        #           for a in range(self.ndim))
        self.nls = nls
        self.nus = nus
        self.view = ExtendedArrayView(self)
        self.is_integer = False
        self.arr = None
        self.is_flat = is_flat
        self.is_constant = is_constant
    @property
    def is_numeric(self):
        return isinstance(self.arr, CO.CustomNDArray)
    @property
    def arr(self):
        return self._arr
    @arr.setter
    def arr(self,v):
        if v is None:
            self._arr = None
            return
        assert v.shape==self.shape
        self._arr = CO.customize(v)
        if not hasattr(self._arr, 'allocate'):
            raise ValueError('The array must have "allocate" method')
    def copy(self,is_flat=None):
        if is_flat is None:
            is_flat=self.is_flat
        return ExtendedArray(self.ncells, self.stags, self.nls, self.nus, is_flat=is_flat)
    def copy_arr(self):
        y = self.copy()
        y.arr = self.arr.copy()
        return y
    def start(self, a):
        s = span(self, a)
        return (None if s is None else s.start)
    def stop(self, a):
        s = span(self, a)
        return (None if s is None else s.stop)
    def like(self, stags, nls, nus):
        "Copy, with different stags, nls, nus"
        res = ExtendedArray(self.ncells, stags, nls, nus, is_flat=self.is_flat)
        res.arr = self.arr.allocate(res.shape)
        return res
    def index(self, i):
        return self.adjust_for_flat(self.numpy_index(i))
    def numpy_index(self, i):
        """Can take positive and negative integers and EndClass and logical index"""
        if isinstance(i, ExtendedArray) and i.arr.dtype==np.bool and i.arr.ndim==self.ndim_real:
            # A hack: logical index
            iea=self.copy()
            iea.arr=np.zeros(self.nts_real,dtype=np.bool)
            iea |= i
            return iea.arr
        if not isinstance(i, tuple):
            if self.ndim_real==1:
                # non-tuple index
                i = (i,)
            else:
                raise IndexError('invalid index: '+str(i)+', must be tuple or logical array')
        if not len(i)==self.ndim_real:
            raise IndexError('Wrong index dimension')
        #ii = tuple(reslice(i1,self.nls[a],self.nus[a],self.ns[a],self.stags[a],a)
        #           for a,i1 in enumerate(i))
        #for (a,i) in enumerate(i): # this is a risky operation (!?) but Python no complain
        #    ii += (reslice(i,self.nls[a],self.nus[a],self.ns[a],self.stags[a],a),)
        ii = ()
        i_iter = iter(i)
        for a in range(self.ndim):
            if not self.is_dim[a]:
                assert self.nls[a] is None and self.nus[a] is None
                ii += (None,) # all of them! Degenerate direction
            else:
                ii += (reslice(
                        next(i_iter),self.ncells[a],self.stags[a],self.nls[a],self.nus[a],a),)
        # Now, ii is a regular Python index
        return ii
        # return self.adjusted_symb_index(ii)
    def ea_index(self,i):
        "Reverse operation to `index`"
        if not isinstance(i, tuple):
            i = np.unravel_index(i,self.nts_real)
        return tuple(i[a]-self.nls[a]+(half if self.stags[a]==1 else 0)
                     for a in range(self.ndim_real))
    def adjust_for_flat(self,index):
        if self.is_flat:
            # Maybe we don't need to do anything?
            if self.ndim_real==1:
                if isinstance(index, tuple):
                    assert len(index)==1
                    return index[0]
                else:
                    return index
            else:
                return CO.ravel_multi_index(index, self.nts_real)
        else:
            # A regular ndarray
            return index
    def __getitem__(self,i):
        if self.arr is None:
            raise TypeError('Allocate and set values first')
        return self.arr[self.index(i)]
    def __setitem__(self,i,v):
        if self.is_constant:
            raise SyntaxError('cannot assign to constant array')
        if self.arr is None:
            raise TypeError('Allocate first')
        if isinstance(v, ExtendedArray):
            if not (isinstance(i, ExtendedArray) and i.arr.dtype==np.bool):
                raise IndexError('Only conditional assignment of extended_array is allowed')
            # This is very common operation, unfortunately not optimized here
            ii = i.view[intersection_span(i, self, v)]
            self[ii] = v[ii]
            return
        self.arr[self.index(i)] = v
    def get_value(self):
        "Do not call"
        raise SyntaxError('Do not call extended_array.setv as getter, use ea.arr or ea[:,:]')
        #return self[:,:]
    def set_value(self,rhs):
        "Assign maximum possible number of values"
        if self.is_constant:
            raise SyntaxError('cannot assign to constant array')
        if self.arr is None:
            raise TypeError('Allocate first')
        if np.isscalar(rhs):
            self.arr[...] = rhs
            return
        #if not isinstance(rhs,extended_array) and np.isscalar(rhs):
        #    raise ValueError('Use ea.arr[...]=x instead of ea.setv = x if x is a scalar')
        if not compatible_ea(rhs, self, ascend=True):
            raise ValueError('Assigning to incompatible extended_array')
        # -- Not just compatible, but all axes present in rhs must also be present in self
        i1, i2, stags, nls, nus = _common_numpy_indices(self, rhs)
        # Almost same procedure as in all binary operations
        is_flat = (self.is_flat or rhs.is_flat)
        if is_flat:
            # Must be careful: adjusting means repeating the numeric array (broadcasting)
            i1a, i2a = CO.broadcast_ravel_multi_index((i1, self.nts_real), (i2, rhs.nts_real))
            self.arr.flat[i1a] = rhs.arr.flat[i2a]
        else:
            self.arr[i1] = rhs.arr[i2]
    setv = property(fget=get_value, fset=set_value, doc=set_value.__doc__)
    def _neighbor(self, axis, do_hi):
        #assert self.arr is not None
        assert CO.is_integer_scalar(axis) and (axis>=0 and axis<self.ndim)
        s = self.stags
        stags = (); nls = (); nus = ()
        #if s[axis] is None:
        #    print(CO.info('extended_array._neighbor():'),CO.alert('Warning: shift has no effect'),
        #          'do_hi=', do_hi, 'stags =',s)
        for a in range(self.ndim):
            if s[a] is None:
                stags += (None,)
                nls += (None,)
                nus += (None,)
            else:
                stags += (1-s[a] if a==axis else s[a],)
                nls += (self.nls[a] + (-s[a] + do_hi if a==axis else 0),)
                nus += (self.nus[a] + (-s[a] + 1 - do_hi if a==axis else 0),)
        res = ExtendedArray(self.ncells, stags, nls, nus, is_flat=self.is_flat)
        res.arr = self.arr
        return res
    def lo(self, axis=None):
        "lo() and hi() are the the values shifted by half cell"
        if self.ndim==1: axis=0
        return self._neighbor(axis, 0)
    def hi(self, axis=None):
        "See lo()"
        if self.ndim==1: axis=0
        return self._neighbor(axis, 1)
    def shifted(self, k, copy=False):
        "Similar to 'lo' and 'hi', but shift by integer"
        assert (isinstance(k, tuple) or isinstance(k, np.ndarray) or isinstance(k, list)) \
            and len(k)==self.ndim
        for a in range(self.ndim):
            if isinstance(k[a],HalfIndex):
                knew = tuple((k[a1].int if a1==a else k[a1]) for a1 in range(self.ndim))
                return self.shifted(knew,copy).hi(a)
        nls = tuple(None if self.stags[a] is None else self.nls[a]+k[a] for a in range(self.ndim))
        nus = tuple(None if self.stags[a] is None else self.nus[a]-k[a] for a in range(self.ndim))
        res = ExtendedArray(self.ncells, self.stags, nls, nus, is_flat=self.is_flat)
        if copy:
            res.arr = self.arr.copy()
        else:
            res.arr = self.arr # do not copy!
        return res
    def broadcast(self, ea=None, stags=None, nls=None, nus=None):
        "Replace None-dimensions with real ones"
        if ea is None:
            assert isinstance(stags, tuple) and isinstance(nls, tuple) and isinstance(nus, tuple)
            ea = ExtendedArray(self.ncells, stags, nls, nus, is_flat=self.is_flat)
        else:
            assert isinstance(ea, ExtendedArray)
        if not compatible_ea(self, ea):
            raise ValueError('Broadcasting to an incompatible shape')
        i1, i2, stags_, nls_, nus_ = _common_numpy_indices(self, ea)
        # some code stolen from extend_binary_function
        ia, reps = CO.broadcast_ravel_multi_index(
                (i1, self.nts_real), (i2, ea.nts_real), extras=True)
        if self.is_flat:
            # Must be careful: adjusting means repeating the numeric array (broadcasting)
            i1a, i2a = ia
            tmp = self.arr.flat[i1a]
        else:
            tmp = np.tile(self.arr[i1],reps[0])
        res = ExtendedArray(self.ncells, stags_, nls_, nus_, is_flat=self.is_flat)
        # We have to reshape it to remove extra dimensions
        if not self.is_flat:
            res.arr = tmp.reshape(res.shape)
        else:
            res.arr = tmp
        return res
    def widen(self, dnls=None, dnus=None):
        if dnls is None:
            dnls = tuple(half if is_dim else None for is_dim in self.is_dim)
            dnus = tuple(half if is_dim else None for is_dim in self.is_dim)
        is_half = tuple(isinstance(dnls[a], HalfIndex) for a in range(self.ndim))
        dnus_is_half = tuple(isinstance(dnus[a], HalfIndex) for a in range(self.ndim))
        if dnus_is_half != is_half:
            raise ValueError('Shifts dnls='+str(dnls)+' and dnus='+str(dnus)+' are incompatible')
        s = self.stags
        l = tuple(dnls[a].int-s[a]+1 if is_half[a] else dnls[a] for a in range(self.ndim))
        u = tuple(dnus[a].int-s[a]+1 if is_half[a] else dnus[a] for a in range(self.ndim))
        stags = tuple((1-s[a] if is_half[a] else s[a]) if self.is_dim[a] else None
                      for a in range(self.ndim))
        nls = tuple(self.nls[a] + l[a] if self.is_dim[a] else None for a in range(self.ndim))
        nus = tuple(self.nus[a] + u[a] if self.is_dim[a] else None for a in range(self.ndim))
        return ExtendedArray(self.ncells, stags, nls, nus, is_flat=self.is_flat)
    def cumsum(self):
        """
        The inverse operation to ``_dif(_dif...)`` over all axes.
        Useful for integration.

        Returns
        -------
        result : extended_array
            Cumulative sum in each direction, with zeros added in the beginning. The ``stags``
            are therefore changed to opposite, and size increased by one in each direction.
            
        See also
        --------
            np.cumsum

        """
        # s = self.stags
        # stags = tuple(1 - s[a] if self.is_dim[a] else None for a in range(self.ndim))
        # nls = tuple(self.nls[a] - s[a] + 1 if self.is_dim[a] else None for a in range(self.ndim))
        # nus = tuple(self.nus[a] - s[a] + 1 if self.is_dim[a] else None for a in range(self.ndim))
        # result = ExtendedArray(self.ncells, stags, nls, nus, is_flat=self.is_flat)
        result = self.widen()
        result.arr = np.zeros(result.nts_real)
        # Sanity check
        if CO.DEBUGLEVEL>1:
            nts_check = tuple(self.nts[a]+1 if self.is_dim[a] else None for a in range(self.ndim))
            assert result.nts==nts_check
        index = tuple(slice(1,None,None) for a_real in range(self.ndim_real))
        result.arr[index] = self.arr
        tmp = result.arr[index]
        for a_real in range(self.ndim_real):
            np.cumsum(tmp, axis=a_real, out=tmp)
        return result
    def interpolate(self, dindex):
        """Interpolation between integer or half-integer indices.
        
        Parameters
        ----------
        dindex : array_like
            An iterable with elements of equal length N giving coordinate points.
            The length must coincide with the dimensionality of the calling object
            (``self.ndim_real``).
            Slices are not allowed, so this does not really work as a regular multi-index.

        Returns
        -------
        result : np.array (N,)
            The interpolated values.

        """
        l = len(dindex[0])
        for i in dindex:
            if len(i) != l:
                raise ValueError('The coordinates must be of same length')
        dindex = np.array(dindex)
        shape = dindex.shape[1:]
        dindex_iterator = iter(dindex)
        numpy_dindex = []
        for axis in range(self.ndim):
            if not self.is_dim[axis]:
                continue
            dindex_axis = next(dindex_iterator)
            numpy_dindex.append(dindex_axis + self.nls[axis]-0.5*self.stags[axis])
        # We have to transpose numpy_indices_double so that the LAST dimension is ea.ndim_real
        N = len(np.array(numpy_dindex).shape)
        permutation = np.hstack((np.arange(1,N),0))
        if CO.DEBUGLEVEL>4:
            print(CO.info('interpolate:'), 'permutation =',permutation)      
        numpy_dindex_transposed = np.transpose(numpy_dindex, permutation)
        # Now, ready to interpolate
        points = tuple(np.arange(self.nts_real[axis]) for axis in range(self.ndim_real))
        # Please disregard scipy official example which uses np.meshgrid, which switches
        # the first two coordinate arrays. There is a bug there and this does not work!!!
        #if len(points)>=2:
        #   points = (points[1],) + (points[0],) + points[2:]
        result = interpn(points, self.arr, numpy_dindex_transposed)
        if CO.DEBUGLEVEL>4:
            print(CO.info('interpolate:'),'result.shape=', result.shape, 'shape=',shape)
        if shape==():
            result = result.item() # or result.reshape(())
        return result
    def info(self,name=''):
       return self.__class__.__name__ + ' ' + name + '[' + \
            ', '.join(info_index(self.ncells[a],self.stags[a],self.nls[a],self.nus[a])
                      for a in range(self.ndim)) + ']'
    def __repr__(self):
        return self.info()
    def table(self):
        """Formatted table, like in pandas"""
        if self.arr is None:
            return '[None]'
        if not isinstance(self.arr, CO.CustomNDArray):
            # don't know how to output it
            return 'array =\n' + str(self.arr)
        if self.ndim==1:
            #axis = 0
            nc, st, nl, nu = (self.ncells[0], self.stags[0], self.nls[0], self.nus[0])
            n = nc + 1 - st
            ss = str(self.arr)[1:-1].split()
            s = '[ '
            for k in range(nl):
                s += ss[k] + ' '
            s += '[| '
            for k in range(n):
                if k < -nl or k >= n + nu:
                    s += '* '
                else:
                    s += ss[k+nl] + ' '
            s += '|] '
            for k in range(nu):
                s += ss[nl+n+k] + ' '
            s += ']'
            return s
        else:
            return('[output of multidim ext. array not implemented]')
    def __str__(self):
        return self.info() + ':\n' + self.table()
    @property
    def dindex(self):
        "The tuple of floating number arrays representing the indices of the extended_array"
        if not hasattr(self, '_dindex') or self._dindex is None:
            self._dindex = ()
            for axis in range(self.ndim):
                if not self.is_dim[axis]:
                    self._dindex += None,
                    continue
                res = ExtendedArray((self.ncells[axis],), (self.stags[axis],),
                                     (self.nls[axis],), (self.nus[axis],), is_constant=True)
                res.arr = np.arange(self.nts[axis])-self.nls[axis] + 0.5*self.stags[axis]
                self._dindex += res,
        return self._dindex
    pass

CO.make_indented_printable(ExtendedArray, replace_repr=False)

#%% Decorators: for functions that take one or two matching arrays
def extend_unary_func(func,allow_symbolic=False,in_place=False):
    """Make a function taking numpy arrays into a function which can also
    take ExtendedArray. Works for both 1D and 2D arrays.
    In-place functions are assumed to return the first argument or None."""
    def newfunc(ea, *args ,**kws):
        if not isinstance(ea, ExtendedArray):
            return func(ea, *args, **kws) # will return None for in-place
        if not ea.is_numeric and not allow_symbolic:
            raise TypeError('Cannot apply nonlinear operation '+func.__name__+
                            ' to a symbolic array')
        assert ea.arr is not None
        if not in_place:
            res = ea.copy()
            res.arr=func(ea.arr, *args, **kws)
            return res
        else:
            tmp = func(ea.arr, *args, **kws)
            res = (None if tmp is None else ea)
            return res
    return newfunc

def extend_binary_func(func,allow_symbolic=False,in_place=False):
    """Make a function taking two numpy arrays into a function which can also
    take two ExtendedArray. Works for both 1D and 2D arrays.
    In-place functions are assumed to return the first argument or None"""
    def newfunc(ea1, ea2, *args, **kws):
        # First, consider cases when one of the arguments is NOT an extended_array
        if not (isinstance(ea1, ExtendedArray) and isinstance(ea2, ExtendedArray)):
            if isinstance(ea1, ExtendedArray):
                assert ea1.arr is not None
                if not in_place:
                    res = ea1.copy()
                    res.arr = func(ea1.arr, ea2, *args, **kws)
                else:
                    tmp = func(ea1.arr, ea2, *args, **kws)
                    res = (None if tmp is None else ea1)
                return res
            elif isinstance(ea2, ExtendedArray):
                assert ea2.arr is not None
                # Both in_place and not in_place are treated the same
                res = ea2.copy()
                res.arr = func(ea1, ea2.arr, *args, **kws)
                return res
            else: # None of them are extended_array
                return func(ea1, ea2, *args, **kws)
        assert ea2.arr is not None
        if not compatible_ea(ea1,ea2):
            #(isinstance(ea1,extended_array) and ea1.ndim==ea2.ndim and
            #     ea1.ncells==ea2.ncells and stags_equal_wnone(ea1, ea2)):
            raise TypeError('in binary operation '+ func.__name__ +' operands must match')
        # Now we know that both are extended arrays
        if not allow_symbolic and not (ea1.is_numeric and ea2.is_numeric):
            raise TypeError('Cannot apply nonlinear operation '+func.__name__+
                            ' to symbolic arrays')
        i1, i2, stags, nls, nus = _common_numpy_indices(ea1, ea2)
        is_flat = (ea1.is_flat or ea2.is_flat)
        if is_flat:
            # Must be careful: adjusting means repeating the numeric array (broadcasting)
            i1a, i2a = CO.broadcast_ravel_multi_index((i1, ea1.nts_real), (i2, ea2.nts_real))
            tmp = func(ea1.arr.flat[i1a],ea2.arr.flat[i2a],*args,*kws)
        else:
            tmp = func(ea1.arr[i1],ea2.arr[i2],*args,*kws)
        if not in_place:
            res = ExtendedArray(ea1.ncells, stags, nls, nus, is_flat=is_flat)
            if CO.DEBUGLEVEL>4:
                print(CO.info('extended '+func.__name__+':'),'res =',repr(res),
                      ', tmp.shape = ', tmp.shape)
            # We have to reshape it to remove extra dimensions
            if not is_flat:
                res.arr = tmp.reshape(res.shape)
            else:
                res.arr = tmp
        else:
            res = (None if tmp is None else ea1)
        return res
    return newfunc

def extend_vectorized_function(nin=None, allow_symbolic=False, copy_info=True, in_place=False):
    def decorator(func):
        if hasattr(func,'nin'):
            the_nin = func.nin
        else: # I am not sure why this is needed
            the_nin = nin
        if the_nin is None:
            raise SyntaxError('For non-ufunc, need number of args (nin)')
        if the_nin == 1:
            extend_func = extend_unary_func
        elif the_nin == 2:
            extend_func = extend_binary_func
        else:
            raise NotImplementedError('Extending functions with nin>2 is not implemented')
        newfunc = extend_func(func,allow_symbolic=allow_symbolic,in_place=in_place)
        CO.update_wrapper(newfunc, func, the_nin, copy_info)
        return newfunc
    return decorator

#%% Adding more stuff outside class definition

BINARY_OPERATORS = ['add','radd','sub','rsub','mul','rmul','pow','rpow',
    'truediv','rtruediv','floordiv','rfloordiv','mod','rmod',
    'and','rand','or','ror','xor','eq','ne','gt','lt','ge','le']

UNARY_OPERATORS = ['neg','pos','abs','invert']

INPLACE_OPERATORS = ['iadd','isub','imul','itruediv','ipow','iand','ior']

# See https://numpy.org/devdocs/reference/ufuncs.html for complete list of ufunc functions
NUMPY_FUNCS = ['sign','sqrt','exp','sin','cos','tan','sinh','cosh','tanh','log','log10',
               'arcsin','arccos','arctan','arctan2','arcsinh','arccosh','arctanh',
               'minimum','maximum']

# Monkey-patching galore of the NumPy operators and functions
def make_binary_func(op):
    def _tmp(t1,t2):
        if CO.DEBUGLEVEL>4:
            print('binary <',t1,'>',op,'<',t2,'>')
        return getattr(t1,op)(t2)
    _tmp.__name__ = op
    return extend_binary_func(_tmp, allow_symbolic=True)

for bop in BINARY_OPERATORS:
    bopd='__'+bop+'__'
    setattr(ExtendedArray, bopd, make_binary_func(bopd))

def make_unary_func(op):
    def _tmp(t1):
        if CO.DEBUGLEVEL>4:
            print('unary',op,'<',t1,'>')
        return getattr(t1,op)()
    _tmp.__name__ = op
    return extend_unary_func(_tmp, allow_symbolic=True)

for uop in UNARY_OPERATORS:
    uopd = '__'+uop+'__'
    setattr(ExtendedArray, uopd, make_unary_func(uopd))

def make_inplace_func(op):
    def _tmp(t1,t2):
        if CO.DEBUGLEVEL>4:
            print('inplace <',t1,'>',op,'<',t2,'>')
        return getattr(t1,op)(t2)
    _tmp.__name__ = op
    return extend_binary_func(_tmp, allow_symbolic=True, in_place=True)
    
for iop in INPLACE_OPERATORS:
    iopd = '__'+iop+'__'
    setattr(ExtendedArray, iopd, make_inplace_func(iopd))

#%% Override NumPy functions
# Functions to be overridden
EA_OVERRIDE = CO.NumpyOverrideContext('ExtendedArray')
for func_name in NUMPY_FUNCS:
    np_func = getattr(np,func_name)
    EA_OVERRIDE.register(func_name, extend_vectorized_function()(np_func))

def minmax(x,y,s):
    "Depending on sign s, return either minimum or maximum"
    return np.minimum(x*s,y*s)*s

#%% Override more NumPy functions
def _extend_where(np_where):
    def where(flog,*args):
        if isinstance(flog, ExtendedArray):
            i = np_where(flog.arr)
            return tuple(i[a] - flog.nls[a] for a in range(flog.ndim))
        else:
            return np_where(flog,*args)
    CO.update_wrapper(where, np_where, 1, True)
    return where
EA_OVERRIDE.register('where', _extend_where(np.where))

def _extend_cumsum(np_cumsum):
    def cumsum(f, axis=None, dtype=None, out=None):
        "Beware of a constant bias!"
        if isinstance(f, ExtendedArray):
            if f.ndim==1: axis=0
            if dtype is not None or out is not None:
                print(CO.info('cumsum:'), CO.alert('ignoring dtype or out'))
            cs = f.lo(axis)
            cs.arr = np_cumsum(f.arr, axis=axis)
            return cs
        else:
            return np_cumsum(f, axis=axis, dtype=dtype, out=out)
    CO.update_wrapper(cumsum, np_cumsum, 1, True)
    return cumsum
EA_OVERRIDE.register('cumsum', _extend_cumsum(np.cumsum))

def _extend_minmax(np_minmax):
    def _minmax(f, axis=None, **kws):
        if isinstance(f, ExtendedArray):
            if len(kws)>0:
                print(CO.info('max:'), CO.alert('ignoring some keywords'))
            return np_minmax(f.arr, axis=axis)
        else:
            return np_minmax(f, axis=axis, **kws)
    CO.update_wrapper(_minmax, np_minmax, 1, True)
    return _minmax
EA_OVERRIDE.register('max', _extend_minmax(np.max))
EA_OVERRIDE.register('min', _extend_minmax(np.min))
EA_OVERRIDE.register('nanmax', _extend_minmax(np.nanmax))
EA_OVERRIDE.register('nanmin', _extend_minmax(np.nanmin))

#%% More useful stuff
def same_ea_structure(ea1,ea2):
    """Check if two extended_arrays's are compatible geometrically
    and by the number of elements"""
    assert isinstance(ea1, ExtendedArray) and isinstance(ea2, ExtendedArray)
    return ea1.ncells==ea2.ncells and ea1.stags==ea2.stags and ea1.nls==ea2.nls and ea1.nus==ea2.nus

def compatible_ea(*eas, extras=False, ascend=False):
    """Can be broadcast together. If ascend, then the first can be broadcast to the second."""
    assert len(eas)>0
    for ea in eas:
        assert isinstance(ea, ExtendedArray)
    ndim = eas[0].ndim
    ncells = eas[0].ncells
    for ea in eas:
        if not ea.ncells==ncells: # was: ea.ndim==ndim
            return False
    # Stags are tricky: some may be None
    res, stags = _tuples_equal_wnone(*tuple(ea.stags for ea in eas), ascend=ascend, total=True)
    if extras:
        return res, ndim, stags
    else:
        return res

def _all_in_tuple_equal_wnone(t, ascend=False):
    """All numbers are equal in a tuple, which may also contain None's.
    Return None if not equal, return the number if equal.
    If all are None, return None.
    If ascend==True, all the Nones should be in the beginning"""
    x = None
    for x1 in t:
        if x1 is not None and x is None:
            x = x1
        if x is not None:
            if (x1 is None and ascend) or (x1 is not None and x != x1):
                return False, None
    return True, x

def _tuples_equal_wnone(*ts, ascend=False, total=False):
    if len(ts)==0 or not all(len(t) == len(ts[0]) for t in ts):
        return (False, None)
    ttot = ()
    for ta in zip(*ts):
        # All in tuple 'ta' must be equal (to 'item') except some Nones
        are_equal, item = _all_in_tuple_equal_wnone(ta, ascend=ascend)
        if not are_equal:
            return (False, None) if total else False
        ttot += (item,)
    return (True, ttot) if total else True
    
# def stags_equal_wnone(*eas, total=False):
#     return _tuples_equal_wnone(tuple(ea.stags for ea in eas), total=total)    

def _union_or_intersection_span(operation, eas, extras):
    "Common code for 'intersection' and 'union'"
    res, ndim, stags = compatible_ea(*eas, extras=True)
    # assert len(eas)>0
    # for ea in eas:
    #     assert isinstance(ea, extended_array)
    # ndim = eas[0].ndim
    # for ea in eas:
    #     assert ea.ndim==ndim
    # # Stags are tricky: some may be None
    # res, stags = _tuples_equal_wnone(*tuple(ea.stags for ea in eas), total=True)
    assert res
    nls = ()
    nus = ()
    result = ()
    for a in range(ndim):
        stag = stags[a]
        if stag is None:
            nls += (None,)
            nus += (None,)
            result += (None,)
        else:
            nl = operation(ea.nls[a] for ea in eas if ea.is_dim[a])
            nu = operation(ea.nus[a] for ea in eas if ea.is_dim[a])
            nls += (nl,)
            nus += (nu,)
            result += (slice(-nl + (half if stag==1 else 0), end + nu - (half if stag==1 else 0)),)
    if extras:
        return result, stags, nls, nus
    else:
        return result

def intersection_span(*eas, extras=False):
    """
    Returns an intersection span (tuple of slices) of several arrays.
    
    See also
    --------
        span, union
    """
    return _union_or_intersection_span(min, eas, extras)

def union_span(*eas):
    """
    Returns a union span (tuple of slices) of several arrays.
    
    See also
    --------
        span, intersection
    """
    return _union_or_intersection_span(max, eas)

def common_index(i, *eas):
    """
    Returns an index which would allow operations between extended arrays of different sizes.
    
    Inputs
    ------
    
    i : boolean `extended_array`
    
    *eas : one or more `extended_array` s
    
    Example
    -------
    
    >>> f[i] += v[i] # Gives an error because f[i] and v[i] are different]
    >>> icommon = common_index(i, f, v)
    >>> f[icommon] += v[icommon] # OK
    
    Implementation
    --------------
    
    Extremely easy
    
    >>> icommon = i.view[intersection_span(i, *eas)]
    """
    return i.view[intersection_span(i, *eas)]

def _common_numpy_indices(ea1, ea2):
    """i1, i2, stags, nls, nus = _common_numpy_indices(ea1, ea2)
    Common NumPy indices for two extended_arrays. Also return common stags, nls, nus.
    (Elimination of repeating code)
    """
    s, stags, nls, nus = intersection_span(ea1, ea2, extras=True)
    # Shrink the common span s for arrays with none-dimensions
    ishrunk1 = tuple(s_ for a, s_ in enumerate(s) if ea1.is_dim[a]) # index into ea1
    ishrunk2 = tuple(s_ for a, s_ in enumerate(s) if ea2.is_dim[a])
    i1 = ea1.numpy_index(ishrunk1)
    # -- index into ea1.arr for numeric ea1, with None's for missing dir
    i2 = ea2.numpy_index(ishrunk2)
    return i1, i2, stags, nls, nus

def widened_span(ea, axis=None):
    """
    Span widened by half on both sides for an extended array along given axis.
    """
    # Implementation: stolen (with corrections) from SpanIndex
    assert isinstance(ea, ExtendedArray)
    if axis is None:
        return tuple(widened_span(ea, a) for a in range(ea.ndim))
    else:
        if ea.stags[axis] is None:
            assert ea.nls[axis] is None and ea.nus[axis] is None
            return None
        else:
            return slice(-ea.nls[axis] + (0 if ea.stags[axis]==1 else -half),
                       end+ea.nus[axis] + (0 if ea.stags[axis]==1 else half) )

#%% Elementary operations. No error checking!
def _diff(f, axis):
    return f.hi(axis)-f.lo(axis)
def _mean(f, axis):
    return (f.hi(axis) + f.lo(axis))/2
def _up(f, v, axis):
    """It is ambiguous what happens when v==0
    """
    if np.isscalar(v) or f.stags[axis] != v.stags[axis]:
        return f.lo(axis)*(v>=0) + f.hi(axis)*(v<0)
    else:
        return (f*(v>=0)).lo(axis) + (f*(v<0)).hi(axis)
def _up1(f, v, axis):
    "Best!"
    klo = tuple(-1 if a==axis else 0 for a in range(f.ndim))
    khi = tuple( 1 if a==axis else 0 for a in range(f.ndim))
    if np.isscalar(v):
        if v>0:
            return f.shifted(klo)
        elif v<0:
            return f.shifted(khi)
        else:
            return f
    if f.stags[axis] == v.stags[axis]:
        vs = v
    else:
        vs = _mean(v, axis)
    return f.shifted(klo)*(vs>0) + f.shifted(khi)*(vs<0)
def _interp_up(f, v, axis):
    if np.isscalar(v) or f.stags[axis]==v.stags[axis]:
        return f - v*_up(_diff(f,axis),v,axis)
    else:
        return f - _up(v*_diff(f,axis),v,axis)

#%% Customize
CO.register_customization_for(HalfIndex)
CO.register_customization_for(ExtendedArray)
