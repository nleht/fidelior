#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 11:03:30 2016

Symbolic array which records a linear operator acting on it
The interface is:

from custom_numpy import customize, decustomize
a = SymbolicArray('a',N)
v = np.linspace(1,2,N-1)
b = customize(v)*(a[:-1]+a[1:])
a[1]=1
gop = b/a # generalized operator, including constraints
op1 = gen_op_total(gop) # Oper(M,V)
op2 = gen_op_simplify(gop) # a shortened version of Oper(M,V)

@author: nle003
"""

#%% Compulsory imports
import numpy as np
import scipy.linalg
import scipy.sparse
import scipy.sparse.linalg
import numbers
from . import common as CO

# We are able to switch quickly between sparse and dense matrices
def set_sparse(use_sparse=True):
    global USE_SPARSE, array_equal ,argmax_row, eye, diag, vstack, solve,\
        to_indexable, to_solvable, to_storeable, to_dense, v_repeat,\
        get_dense_row, matrix, is_indexable
    USE_SPARSE = use_sparse
    if use_sparse:
        # Enhancements (!?) of the scipy.sparse package
        def array_equal(m1,m2):
            "I don't know why this is not in the package already"
            tmp = (m1 != m2)
            if isinstance(tmp,bool):
                assert tmp
                return False
            return (m1 != m2).nnz==0
        def argmax_row(m):
            "Returns a 1D ndarray"
            tmp = m.tolil()
            rows = tmp.rows
            data = tmp.data
            N = m.shape[0]
            inds = np.ndarray((N,),dtype=np.int) # preallocate
            for k in range(N):
                inds[k]=rows[k][np.argmax(data[k])]
            return inds
        eye=scipy.sparse.eye
        def diag(diagonal):
            return scipy.sparse.diags(diagonal, dtype=diagonal.dtype)
        vstack=scipy.sparse.vstack
        solve = scipy.sparse.linalg.spsolve
        # "Freeze" operation take a long time!
        #def to_indexable(m): return m.tolil() # before optimization
        #def to_indexable(m): return m.tocsc() # after optimization
        def to_indexable(m): return m.tocsr() # About the same (?)
        def is_indexable(m):
            return m.__class__.__name__=='csr_matrix'
        #def to_solvable(m): return m.tocsc()
        def to_solvable(m): return m.tocsr()
        def to_storeable(m): return m.tolil()
        def to_dense(m): return m.A
        def v_repeat(m,n): return scipy.sparse.lil_matrix(np.repeat(m.A,n,axis=0))
        def get_dense_row(m,k): return m.getrow(k).A[0]
        matrix = scipy.sparse.lil_matrix
    else:
        def array_equal(m1,m2):
            return np.array_equal(m1,m2)
        def argmax_row(m):
            return np.argmax(m,axis=1)
        def eye(n): return np.eye(n)
        diag = np.diag
        vstack=np.vstack
        solve = scipy.linalg.solve
        def to_indexable(m): return m
        def is_indexable(m): return True
        def to_solvable(m): return m
        def to_storeable(m): return m
        def to_dense(m): return m
        def v_repeat(m,n): return np.repeat(m,n,axis=0)
        #def matrix(s): return np.matrix(np.ndarray(s))
        def get_dense_row(m,k): return m[k,:]
        matrix = np.zeros
    pass

def get_sparse():
    return USE_SPARSE

#%% Operator classes

def is_scalar(v):
    return isinstance(v,numbers.Number) or isinstance(v, np.number) or \
        (isinstance(v, np.ndarray) and v.shape==())

class Operator:
    """Elementary operator M*x + V
    M is always a 2D np.ndarray (dense) or a sparse matrix (sparse)!
    V is always a 1D np.array!"""
    def __init__(self,M,V):
        self.M = M
        self.V = V
    @property
    def num_vars(self):
        return self.M.shape[1]
    @property
    def num_eqs(self):
        return self.M.shape[0]
    @property
    def valid(self):
        return len(self.M.shape)==2 and len(self.V.shape)==1 and self.num_eqs == self.V.shape[0]
    @property
    def complete(self):
        return not np.isnan(self.V).any()
    @property
    def solvable(self):
        return self.valid and self.num_vars == self.num_eqs
    def apply(self,x):
        return self.M.dot(x) + self.V
    def __call__(self,x):
        return self.apply(x)
    def solve(self,b):
        "Solve M*x+V=b for x"
        if CO.DEBUGLEVEL>0:
            assert self.solvable and len(b)==self.num_eqs
        return solve(to_solvable(self.M),b-self.V)
    def __getitem__(self, i):
        if isinstance(i, tuple) and len(i)==1:
            i = i[0]
        i = CO.slice_to_index_array(i, self.num_eqs) # so that M does not become a 1d
        # The following line was a bottleneck for many colculations
        #return operator(to_storeable(to_indexable(self.M)[i,:]),self.V[i])
        return Operator(self.M[i,:],self.V[i])
    def __setitem__(self, i, op):
        #i = CO.slice_to_index_array(i, self.num_eqs) # so that M does not become a 1d
        if isinstance(op, Operator):
            self.M[i,:] = op.M
            self.V[i] = op.V
        else:
            # Just regular number or vector
            self.M[i,:] = 0
            self.V[i] = op
    def __eq__(self,op):
        return (array_equal(self.M,op.M) and np.array_equal(self.V,op.V))
    def copy(self):
        return  Operator(self.M.copy(),self.V.copy())
    def __neg__(self):
        return Operator(-self.M,-self.V)
    def __add__(self,op):
        if isinstance(op, Operator):
            return Operator(self.M + op.M, self.V + op.V)
        else:
            #assume it is something that should be just added to the result of op application
            return Operator(self.M, self.V + op)
    def __sub__(self,op):
        return self + (-op)
    def __mul__(self,v):
        "Multiply by number"
        if is_scalar(v):
            return Operator(self.M*v,self.V*v)
        else:
            return Operator(diag(CO.decustomize(v)).dot(self.M), CO.decustomize(v)*self.V)
    def cat(self,op):
        if isinstance(op, Operator):
            return Operator(vstack((self.M,op.M)),np.hstack((self.V,op.V)))
        else:
            # assume it is a 1D array, set the matrix to zero
            return Operator(vstack((self.M, matrix((len(op), self.num_vars)))),
                            np.hstack((self.V,op)))
    def repeat(self,n):
        return Operator(v_repeat(self.M,n),np.repeat(self.V,n,axis=0))

CO.make_indented_printable(Operator)

class ConstrainedOperator:
    """Generalized operator, with constraints:
    Equation is equation.M * x + equation.V = rhs, or equation(x)=rhs
    Constraints are constraint.M * x + constraint.V = 0, or constraint(x)=0
    is_dependent is a bool mask showing which x components are dependent."""
    def __init__(self, equation, constraint, is_dependent):
        self.equation = equation # operator
        self.constraint = constraint # operator
        self.is_dependent = is_dependent # bool array
    @property
    def num_vars(self):
        return self.equation.num_vars
    @property
    def num_eqs(self):
        return self.equation.num_eqs
    @property
    def num_deps(self):
        return self.constraint.num_eqs
    @property
    def valid(self):
        "But not necessarily solvable"
        return self.equation.valid and self.constraint.valid and \
            self.num_vars==self.constraint.M.shape[1]==len(self.is_dependent) and \
            sum(self.is_dependent)==self.constraint.M.shape[0]
    #@property
    #def solvable(self):
    #    return self.valid and self.num_deps + self.num_eqs == self.num_vars
    def __eq__(self,gop):
        return self.equation==gop.equation and self.constraint==gop.constraint \
            and np.array_equal(self.is_dependent,gop.is_dependent)
    def apply(self,x):
        return (self.equation(x),self.constraint(x))
    def __call__(self,x):
        return self.apply(x)
    @property
    def total(self):
        return self.equation.cat(self.constraint)
    def solve(self,b):
        bt = np.hstack((b,np.zeros((self.num_deps,))))
        return self.total.solve(bt)
    @property
    def simplify(self):
        d = self.is_dependent
        if any(d):
            cM = to_indexable(self.constraint.M)
            eM = to_indexable(self.equation.M)
            A = solve(to_solvable(cM[:,d].T),to_solvable(eM[:,d].T)).T
            return Operator(eM[:,~d]-A.dot(cM[:,~d]),self.equation.V-A.dot(self.constraint.V))
        else:
            return self.equation

CO.make_indented_printable(ConstrainedOperator)

#%% A storage for constraints
class ConstraintStorage:
    def __init__(self,n):
        self.n = n
        self.op = Operator(matrix((0,n)),np.ndarray((0,)))
        self.ind = np.ndarray((0,),dtype=np.int)
        self.dep = np.zeros((n,),dtype=np.bool)
        self.frozen = False
    def append(self,op,ind=None):
        if CO.DEBUGLEVEL>4:
            print(CO.info('constraint_storage:'),'Appending op =',op,', ind =',ind,'to',self)
        assert op.num_vars == self.n
        if ind is None:
            # Automatically determine ind
            M=op.M.copy()
            M[:,self.dep]=0
            ind = argmax_row(abs(M))
            for k in range(len(ind)):
                if M[k,ind[k]] == 0:
                    raise Exception('Conflicting dependencies')
        l = len(op.V)
        assert len(ind)==l
        #print(op.M,np.arange(l),ind)
        #print(op.M[(np.arange(l),ind)])
        if not to_dense(op.M[(np.arange(l),ind)]).all():
            raise Exception('ind='+str(ind)+' is given incorrectly')
        if self.dep[ind].any():
            raise Exception('Conflicting dependencies: these elements are already set')
        self.ind = np.hstack((self.ind,ind))
        self.op = self.op.cat(op)
        self.dep[ind] = True
        self.frozen = False
    def freeze(self):
        if not self.frozen:
            M = to_indexable(self.op.M)
            self.Md = to_solvable(M[:,self.dep])
            self.Mi = to_solvable(M[:,~self.dep])
            self.frozen = True
    def enforce(self,a,copy=False):
        b = (a.copy() if copy else a)
        if self.op.num_eqs > 0:
            self.freeze()
            x = b.flat
            x[self.dep] = -solve(self.Md,self.op.V + self.Mi.dot(x[~self.dep]))
        return b

CO.make_indented_printable(ConstraintStorage)

#%% Constraints interface
class ConstrainedArray:
    """A wrapper for a linear system of equations, with parts of it being updateable.
    The total number of variables is "n", the dependent variables are indicated in
    bool array "dep"
    """
    def __init__(self, name, n, array_like=False, debug=None):
        self._debug = CO.DEBUGLEVEL if debug is None else debug
        assert isinstance(name,str) and isinstance(n,numbers.Integral)
        assert n > 0
        self.name = name
        self.n = n
        self.array_like = array_like
        assert isinstance(n,numbers.Integral) and n > 0
        self.cnst = ConstraintStorage(n)

CO.make_indented_printable(ConstrainedArray)

#%% Fake array expression (any linear stuff)
class ConstraintManager:
    def workon(self,c,ind):
        pass

class ConstraintManagerDefault(ConstraintManager):
    def __init__(self, cnst):
        self.cnst = cnst # constraint_storage
    def workon(self,c,ind):
        self.cnst.append(c,ind)
    pass

class ConstrainedExpression:
    def __init__(self, ref, op=None, is_view=True, constrained=True):
        # Reference to the abstract fake array, dispatch by type
        assert isinstance(ref, ConstrainedArray)
        self.ref = ref
        self._debug = self.ref._debug
        n = self.ref.n # Will never change
        if op is None:
            self.op = Operator(to_indexable(eye(n)),np.zeros((n,)))
            #self.op = operator(to_storeable(eye(n)),np.zeros((n,)))
        else:
            if self._debug>0:
                assert op.valid and op.num_vars==n
            self.op = op
        self.is_view = is_view
        self.constrained = constrained
        self.cnst_manager = ConstraintManagerDefault(ref.cnst)
    def from_Operator(self, op, is_view=False, constrained=True):
        """Copy everything except the operator. Since it is a result of some operation,
        it is not a view."""
        # The reason behind constrained=True by default is that to get unconstrained,
        # you should use allocate
        return ConstrainedExpression(self.ref, op=op, is_view=is_view, constrained=constrained)
    def allocate(self, shape):
        "Create an empty symbolic array of length n with the same reference"
        assert isinstance(shape, tuple) and len(shape)==1
        n, = shape
        assert CO.is_integer_scalar(n) and n>0
        # Create a "virtual" operator -- virtuality indicated by NaNs
        op = Operator(matrix((n, self.ref.n)), np.full((n,), fill_value=np.nan))
        return self.from_Operator(op, constrained=False)
    def __getitem__(self,i):
        if CO.DEBUGLEVEL>3:
            print(CO.info('ConstrainedExpression: __getitem__'),self.op.M.__class__.__name__)
        #i = common.slice_to_index_array(i,len(self)) # want i to be an array of indices
        return self.from_Operator(self.op[i], is_view=(self.is_view and isinstance(i,slice)))
    def check_ref(self,fa,operation):
        if not (self.ref is fa.ref):
            raise ValueError('Operation "' + operation + '" between different symbolic arrays ' +
                             self.ref.name +' and ' + fa.ref.name + ' is not allowed')
    def __setitem__(self,i,rhs):
        "Set constraints. To set operator, use self.op[i] = rhs.op"
        if isinstance(i, tuple) and len(i)==1:
            i = i[0]
        if not self.constrained:
            if isinstance(rhs, ConstrainedExpression):
                self.check_ref(rhs,'__setitem__')
                self.op[i] = rhs.op
            else:
                # It is just a number or array
                self.op[i] = rhs
            return
        i = CO.slice_to_index_array(i,len(self))
        if self.ref.array_like:
            if not self.is_view:
                raise SyntaxError(
                    'setting indeces for a non-view, not allowed for real arrays!')
            else:
                # Which indices of the original array?
                ind = argmax_row(self.op.M[i,:])
                # Check if it really a view
                if self._debug>1:
                    assert to_dense(self.op.M[(i,ind)]==1).all()
        else:
            # Cannot determine which variables are made dependent
            # Delegate this to Constraint.append
            ind = None
        # Calculate the constraint operator
        if isinstance(rhs, ConstrainedExpression):
            # A more complicated constraint
            self.check_ref(rhs,'__setitem__')
            #if not (self.ref is rhs.ref):
            #    raise ValueError('Assigning '+rhs.ref.name+' to '+self.ref.name+' is not allowed')
            # Allow assigning single value to multiple values
            if rhs.op.num_eqs==1:
                tmp=v_repeat(rhs.op.M,len(i))
            else:
                tmp=rhs.op.M
            if not (tmp.shape==self.op.M[i,:].shape):
                raise ValueError('Assigning '+str(tmp.shape[0])+' values to '+\
                                 str(self.op.M[i,:].shape[0])+' not allowed')
            #c = operator(self.op.M[i,:]-tmp,V=self.op.V[i]-rhs.op.V)
            c = self.op[i] - Operator(tmp,rhs.op.V)
        else:
            # Simple constraint
            c = self.op[i] - rhs
        if CO.DEBUGLEVEL>4:
            print(CO.info('ConstrainedExpression.__setitem__():'),'Constraint is',c,', ind =',ind)
        assert c.valid # len(c.V) == c.M.shape[0] # sanity check
        self.cnst_manager.workon(c,ind)
    def __add__(self,fa):
        if CO.DEBUGLEVEL>3:
            print(CO.info('ConstrainedExpression: __add__'),self.op.M.__class__.__name__,' + ',end='')
            if isinstance(fa, ConstrainedExpression):
                print(fa.op.M.__class__.__name__)
            else:
                print(fa.__class__.__name__)
        #print('add:\n',self,'\n + \n',fa)
        if isinstance(fa, ConstrainedExpression):
            self.check_ref(fa, '__add__')
            # Also check dimentions
            assert len(self)==len(fa) or len(fa)==1 or len(self)==1
            if len(self)==len(fa):
                return self.from_Operator(self.op + fa.op)
            elif len(fa)==1:
                return self.from_Operator(self.op + fa.op.repeat(len(self)))
            elif len(self)==1:
                return self.from_Operator(self.op.repeat(len(fa)) + fa.op)
            else:
                raise Exception('internal error')
        else:
            return self.from_Operator(self.op + CO.decustomize(fa))
    # def __iadd__(self, fa):
    #     self = self + fa
    #     return self
    def __radd__(self,fa):
        return self + fa
    def __sub__(self,fa):
        return self + (-fa)
    def __rsub__(self,fa):
        return -self + fa
    def __neg__(self):
        return self.from_Operator(-self.op)
    def __pos__(self):
        return self
    def __mul__(self,v):
        if CO.DEBUGLEVEL>3:
            print(CO.info('ConstrainedExpression: __mul__'), self.op.M.__class__.__name__)
        return self.from_Operator(self.op*v)
    def __rmul__(self,v):
        #print('----- rmul -----')
        return self*v
    def __truediv__(self,fa):
        return self*(1/fa)
    @property
    def action(self):
        "The ConstrainedOperator associated with the expression"
        gop = ConstrainedOperator(self.op, self.ref.cnst.op, self.ref.cnst.dep)
        if self._debug>1: assert gop.valid
        return gop
    def __len__(self):
        "Different from self.ref.size in general because it could be a sub-array"
        return self.op.num_eqs
    @property
    def shape(self):
        return (self.__len__(),)
    def reshape(self, shape):
        "Do nothing"
        assert np.prod(shape)==len(self)
        print(CO.info('ConstrainedExpression:'),
              CO.alert('Warning: reshaping -- doing nothing!'
                       ' Did you forget to set is_flat=True in a symbolic_array?'))
        return self
    def copy(self):
        # Do NOT copy the reference!
        return ConstrainedExpression(self.ref, self.op.copy(), is_view=self.is_view)
    def __eq__(self,fa):
        if not isinstance(fa, ConstrainedExpression): return False
        return self.op == fa.op and self.is_view == fa.is_view and \
            self.ref == fa.ref
    @property
    def flat(self):
        "It's always flat"
        return self
    def cat(self,fa):
        if isinstance(fa, ConstrainedExpression):
            self.check_ref(fa, 'cat')
            return self.from_Operator(self.op.cat(fa.op))
        elif isinstance(fa, Operator):
            return self.from_Operator(self.op.cat(fa))
        else:
            raise ValueError('got '+fa.__class__.__name__,', expected operator or CAExpression')
    pass

CO.make_indented_printable(ConstrainedExpression)

#%% Overload the NumPy hstack function for symbolic arrays
SY_OVERRIDE = CO.NumpyOverrideContext('SymbolicArray')
def _symbolize_hstack(np_hstack):
    def hstack(t):
        assert isinstance(t,tuple)
        f1 = t[0]
        if isinstance(t[0], ConstrainedExpression):
            f = t[0]
            for f1 in t[1:]:
                f = f.cat(f1)
            return f
        else:
            return np_hstack(t)
    CO.update_wrapper(hstack, np_hstack, None, True)
    return hstack

SY_OVERRIDE.register('hstack', _symbolize_hstack(np.hstack))

#%% A few more functions
# A small wrapper which looks like a class, thus the CamelCase naming
def SymbolicArray(name, n, array_like=True, constrained=True):
    return ConstrainedExpression(
        ConstrainedArray(name, n, array_like=array_like), constrained=constrained)

#%% Preparations and defaults
CO.register_customization_for(ConstrainedExpression)
set_sparse(True)
