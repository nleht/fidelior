#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul  5 20:31:33 2020

3D advection

@author: nleht
"""

#%% Mandatory imports
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import axes3d
import fidelior_old as fdo
#from utils import nice_print, nice_figures
from fidelior_old import end, half, span, ExtendedArray, Field
from fidelior_old.common import decustomize
from fidelior_old import n_mean, c_mean, n_diff, c_diff, n_ups, c_ups, n_interp_ups, c_interp_ups
from fidelior_old.plotting import UpdateablePcolor

do_symb = True
if do_symb:
    import fidelior_old.autoschemes_symb as ASSymb
else:
    import fidelior_old.autoschemes as AS

def make_3dplot(geom, fig, f, title='', vmin=None, vmax=None):
    g = geom.grids
    x = decustomize(g[0].r_n[0:end].flatten())
    y = decustomize(g[1].r_n[0:end].flatten())
    z = decustomize(g[2].r_n[0:end].flatten())
    Nz = geom.ncells[2]
    X, Y = fdo.co.ndgrid(x, y)
    fig.clear()
    ax = fig.gca(projection='3d')
    if vmin is None:
        vmin = decustomize(np.min(f))
    if vmax is None:
        vmax = decustomize(np.max(f))
    levels = np.linspace(vmin,vmax,11)
    for iz in range(Nz+1):
        cset = ax.contour(X, Y, decustomize(f[:,:,iz]), zdir='z', offset=z[iz],
                          vmin=vmin, vmax=vmax, levels=levels,
                          cmap=mpl.cm.coolwarm, alpha=0.3)
    ax.set_xlabel('X')
    ax.set_xlim(x[0], x[-1])
    ax.set_ylabel('Y')
    ax.set_ylim(y[0], y[-1])
    ax.set_zlabel('Z')
    ax.set_zlim(z[0], z[-1])
    if mpl.__version__ <= '3.0.3':
        ax.set_aspect('equal') # does not work with newer matplotlib
    # See https://stackoverflow.com/a/31364297 for a possible solution
    ax.set_title(title)
    fig.canvas.draw()


fdo.co.BOLD = True
fdo.set_global_debug_level(0)
#fdo.print_motto()

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

#%% Set up the grid and geometry
Nx = 50
Ny = 50
Nz = 50
Lx = 2
Ly = 2
Lz = 2
dx = Lx/Nx
dy = Ly/Ny
dz = Lz/Nz
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=True)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=True)
gridz = fdo.Grid(num_cells=Nz, delta=dz, start=-Lz/2, periodic=True)
geom = fdo.GeometryCartesian((gridx, gridy, gridz), nls=(5,5,5), nus=(5,5,5)) # ample wiggle room
x = gridx.r_n
y = gridy.r_n
z = gridz.r_n
# xc = gridx.r_c
# yc = gridy.r_c
# zc = gridz.r_c
# xe = xc[-half:end+half].flatten()
# ye = yc[-half:end+half].flatten()
# ze = zc[-half:end+half].flatten()
u_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0,0), nls=(0,0,0), nus=(0,0,0))
u_indep.arr = np.ones(u_indep.nts)

#%% Some schemes
if do_symb:
    CIR = ASSymb.InterpolatingScheme(geom, 1, [(0,0,0), (-1,0,0), (0,-1,0),(0,0,-1)])
    LW = ASSymb.InterpolatingScheme(geom, 2,
        [(0,0,0), (1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1),
            (1, -1, 0), (0, 1, -1), (-1, 0, 1)])
    UTOPIA = ASSymb.InterpolatingScheme(geom, 3,
        [(0,0,0), (-1,0,0), (0,-1,0), (1,0,0), (0,1,0), (-1, 1,0),
            (1, -1,0), (-2,0,0), (0,-2,0), (-1,-1,0),
            (0,0,1), (-1, 0, 1), (0, -1, 1), # Forward layer
            (0,0,-1), (-1, 0, -1), (0, -1, -1), (1, 0, -1), (0, 1, -1), # back layer 1
            (0, 0, -2), # back layer 2
            (-1, -1, -1) # extra point
        ])
else:
    # TODO: flip_stencil=True option makes this VERY slow!
    CIR = AS.make_interpolation_scheme_sp(geom, u_indep, 1,
        [(0,0,0), (-1,0,0), (0,-1,0),(0,0,-1)], flip_stencil=False)
    LW = AS.make_interpolation_scheme_sp(geom, u_indep, 2,
        [(0,0,0), (1, 0, 0), (-1, 0, 0), (0, 1, 0), (0, -1, 0), (0, 0, 1), (0, 0, -1),
            (1, -1, 0), (0, 1, -1), (-1, 0, 1)],
        flip_stencil=False)
    UTOPIA = AS.make_interpolation_scheme_sp(geom, u_indep, 3,
        [(0,0,0), (-1,0,0), (0,-1,0), (1,0,0), (0,1,0), (-1, 1,0),
            (1, -1,0), (-2,0,0), (0,-2,0), (-1,-1,0),
            (0,0,1), (-1, 0, 1), (0, -1, 1), # Forward layer
            (0,0,-1), (-1, 0, -1), (0, -1, -1), (1, 0, -1), (0, 1, -1), # back layer 1
            (0, 0, -2), # back layer 2
            (-1, -1, -1) # extra point
        ],
        flip_stencil=False)

SCHEMES = {'CIR':CIR, 'LW':LW, 'UTOPIA':UTOPIA}

#%% The initial value and velocity
fini = ExtendedArray(geom.ncells, stags=(0, 0, 0), nls=(0, 0,0), nus=(0, 0,0))
fini.arr = np.zeros(fini.nts)
mask = (np.abs(x)<Lx/4) & (np.abs(y)<Ly/6) & (np.abs(z)<Lz/8)
fini[mask] = 1
V = np.array([.5, .3, .4])
assert all([gr.uniform for gr in geom.grids]) # Nonunform will be in version 2
dr = np.array([dx, dy, dz])
dt = 1/3/np.sum(V/dr)
c = V*dt
methods = ['CIR', 'LW', 'UTOPIA']
nmethods = len(methods)

def apply_bc(f, d):
    # SCHEMES[method].order
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    f[span, span, end:end+d]     = f[span, span, 0:d]
    f[span, span, -d:-1]         = f[span, span, end-d:end-1]
    f[span, end:end+d, 0:end-1]  = f[span, 0:d,         0:end-1]
    f[span, -d:-1,     0:end-1]  = f[span, end-d:end-1, 0:end-1]
    f[end:end+d, 0:end-1, 0:end-1] = f[0:d, 0:end-1, 0:end-1]
    f[-d:-1,  0:end-1, 0:end-1] = f[end-d:end-1, 0:end-1, 0:end-1]

#%% Initial values
farr=[]
for kmethod, method in enumerate(methods):
    d = 2
    f = ExtendedArray(geom.ncells, stags=(0,0,0), nls=(d,d,d), nus=(d,d,d))
    f.arr = np.zeros(f.nts)
    f.setv = fini
    farr.append(f)

plt.close('all')
figs = []
for kmethod in range(nmethods):
    fig = plt.figure(100+kmethod)
    figs.append(fig)

#%% Let's go!
Nt = 10000
t = 0
for kt in range(Nt+1):
    for kmethod in range(nmethods):
        f = farr[kmethod]
        alg = methods[kmethod]
        if do_symb:
            f.setv = SCHEMES[alg].interpolate(f, -c)
        else:
            f.setv = SCHEMES[alg].interpolate_on_displacement(f, -c*u_indep)
        assert not np.isnan(f.arr).any()
        apply_bc(f, 2)
        assert not np.isnan(f.arr).any()
    t += dt
    if kt<20 or (kt<200 and kt % 10==0) or kt % 100==0:
        for kmethod in range(nmethods):
            f = farr[kmethod]
            fig = figs[kmethod]
            alg = methods[kmethod]
            if not plt.fignum_exists(100+kmethod):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            #plt.figure(100+kmethod)
            Dx = np.int(t*V[0]/dx)
            Dy = np.int(t*V[1]/dy)
            Dz = np.int(t*V[2]/dz)
            the_title = alg + ': D = [{:d}, {:d}, {:d}]'.format(Dx,Dy,Dz)
            make_3dplot(geom, fig, f, title=the_title, vmin=-.5, vmax=1.5)
            fig.canvas.draw()
        #plt.draw_all()
        plt.pause(1.)
        #fig[0].waitforbuttonpress() # progress by clicking mouse in Figure 100

plt.show()
