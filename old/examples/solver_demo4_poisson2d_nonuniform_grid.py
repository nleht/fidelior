#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  6 14:58:27 2016

We demonstrate (almost all) features of TRUMP by solving a discretized Poisson equation

    Δ(φ)=-ρ

for φ with given ρ, in 2D in cartesian coordinates. While doing this, we abuse
the feature of Python3 which allows Greek characters to be used in variable names.

@author: nle003
"""

import time
import numpy as np
import matplotlib.pyplot as plt
import fidelior_old as fdo
from fidelior_old import end, half, Field, Solver, c_diff, n_diff

use_fdo_plotting = True
if use_fdo_plotting:
    from fidelior.plotting import UpdateablePcolor

fdo.set_sparse(True) # use sparse matrices for operators, this is default
fdo.set_global_debug_level(0)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()
save_fig = False # set to True if you want PDF output of the figures

#%% Set up
case = 'growing grid'
Nx = 30; Ny = 20;
if case == 'random grid':
    rand_coef = 0.9
    dx = fdo.random_grid_delta(1, Nx, rand_coef)
    dy = fdo.random_grid_delta(1, Ny, rand_coef)
elif case == 'growing grid':
    # Growing grid
    dx = fdo.growing_grid_delta(1, Nx, 1/Nx/100)
    dy = fdo.growing_grid_delta(1, Ny, 1/Nx/100)
    Nx = len(dx)
    Ny = len(dy)
elif case == 'uniform grid':
    dx = 1/Nx
    dy = 1/Ny
else:
    raise NotImplementedError('case not found')
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=0.0)
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=0.0)
geom = fdo.GeometryCartesian((gridx,gridy), nls=(2,2), nus=(2,2))
# --- 'nls' and 'nus' provide wiggle room for the cooridinate axcess
ncells = (Nx, Ny)
# 'stags' is the staggering flag of the grid. '0' means the values are defined
# at nodes, while '1' would mean that the values are defined at centers of cells.
# 'nls' and 'nus' is the number of ghost cell layers on the bottom (lower) and
# the top (upper) boundaries of the array.
ρ = Field('ρ', ncells, stags=(0,0), nls=(-1,-1), nus=(-1,-1))
# The charge density ρ does not need to be defined at the boundary, so the
# negative number of ghost cells is specified (we cut off one layer)
print('Setting ρ=1 in the domain [',gridx.start,'--',gridx.stop,'x',\
    gridy.start,'--',gridy.stop,']')
ρ[1:end-1,1:end-1] = 1
# note that ρ[0,...] or ρ[end,...] whould give an error because the boundary is cut off.
# TRUMP uses MATLAB-style indexing, not Python-style! In particular, the upper
# boundary ('stop' of the slice) truly _is_ the last element, not the element after the last
# as it would be in Python indexing. Also, negative indices in ExtendedArrays
# mean exactly what they mean - ghost cells below the lower boundary.
φ = Field('φ',ncells,stags=(0,0),nls=(0,0),nus=(0,0))
print('Setting φ=0 on the right edge, bottom and top')
φ.bc[end,:] = 0 # ':' is equivalent to '0:end'
φ.bc[1:end-1,0] = 0
# -- We cannot use φ.bc[:,0] = 0 here because it would conflict/redefine the previous BC
φ.bc[1:end-1,end] = 0
print('Setting φ=0 on the left edge')
φ.bc.record('left edge',is_variable=True)
φ.bc[0,:] = 0
t0 = time.time()
φ.bc.freeze() # This is necessary - checks BC consistency and prepares BC for solvers
print(fdo.co.info('Freezing time:'),time.time()-t0,flush=True)
def Δ(f, geom):
    "Laplacian"
    # c_diff acts on stag=0 ExtendedArray's ('central' difference), the result
    # is a stag=1 array. The opposite is for n_diff ('node' difference).
    #g = geom.grids
    #return n_diff(c_diff(f,0)/g[0].c_dr,0)/g[0].n_dr + n_diff(c_diff(f,1)/g[1].c_dr,1)/g[1].n_dr
    return geom.laplacian(f)
t0 = time.time()
Δφ = Δ(φ.symb, geom)
print(fdo.co.info('Symbolic operator time:'),time.time()-t0,flush=True)
t0 = time.time()
φ_solver = Solver(φ, -Δφ) # Poisson equation with a given RHS = -Δ(φ)
print(fdo.co.info('Solver setup time:'),time.time()-t0,flush=True)

#%% Plot solution #1
print('Solving Δφ=-ρ with φ=0 on the left edge ..')
t0 = time.time()
φ_solver.solve_full(φ,ρ) # BC are automatically included
print(fdo.co.info('Solving time:'),time.time()-t0,flush=True)
print('Error =',np.max(np.abs(Δ(φ, geom)+ρ).arr)) # Out: 5.00155472594e-13
xe = gridx.r_c[-half:end+half].flatten()
ye = gridy.r_c[-half:end+half].flatten()
#φ.pcolor_x(0); ye = φ.pcolor_x(1)
# xe, ye are just for 'pcolor', these are a not the points where the function is
# defined. To get the grid points, use φ.x(0) and φ.x(1)
# print('Please close the figure window to continue') # if not using IPython

if use_fdo_plotting:
    fig1 = UpdateablePcolor(1, figsize=(6,5))
    fig1.plot(geom, φ, title=r'$\phi$ for $\rho=1$ and $\phi=0$ at the boundary')
    fig1.ax.set_xlabel('x')
    fig1.ax.set_ylabel('y')
else:
    plt.figure(1,figsize=(6,5))
    plt.clf()
    plt.pcolor(xe,ye,φ[:,:].T)
    plt.gca().set_aspect('equal')
    plt.xlabel('x'); plt.ylabel('y');
    plt.title(r'$\phi$ for $\rho=1$ and $\phi=0$ at the boundary')
    plt.colorbar()
    if save_fig:
        plt.savefig('poisson2d_fig_1.pdf')
    
#%% Plot solution #2
print('Updating the BC to φ=1 on the left edge')
t0 = time.time()
φ.bc.update('left edge')
φ.bc[0,:] = 1
φ.bc.end_update() # optional
print(fdo.co.info('Update time:'),time.time()-t0,flush=True)
print('Solving Δφ=-ρ with φ=1 on the left edge ..', flush=True)
t0 = time.time()
φ_solver.solve_full(φ,ρ) # the solver already knows about the new BC
print(fdo.co.info('Solving time:'),time.time()-t0,flush=True)
print('Error =',np.max(np.abs(Δ(φ, geom)+ρ).arr)) # Out: 4.15134593368e-12
if use_fdo_plotting:
    fig2 = UpdateablePcolor(2, figsize=(6,5))
    fig2.plot(geom, φ, title=r'$\phi$ for $\rho=1$, $\phi=1$ at left edge')
    fig2.ax.set_xlabel('x')
    fig2.ax.set_ylabel('y')
else:
    plt.figure(2,figsize=(6,5))
    plt.clf()
    plt.pcolor(xe,ye,φ[:,:].T)
    plt.gca().set_aspect('equal')
    plt.xlabel('x'); plt.ylabel('y');
    plt.title(r'$\phi$ for $\rho=1$, $\phi=1$ at left edge')
    plt.colorbar()
    if save_fig:
        plt.savefig('poisson2d_fig_2.pdf')

fdo.numpy_override.complete_exit()
plt.show()
