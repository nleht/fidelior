#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 17 13:00:45 2018

@author: nle003
"""

# Import the needed elements from TRUMP package
import time
import fidelior_old as fdo
from fidelior_old import c_diff, n_diff, end, Field, Solver

fdo.set_sparse(True)
fdo.set_global_debug_level(3) # 3 -- some diagnostic output

if not fdo.numpy_override.entered:
    fdo.numpy_override.enter() # if we want to use NumPy functions!
# We could also use it as a context:
#   with fdo.numpy_override:
#       # ... do stuff ...

#%% Setup
# Set up the calculation domain
N = 100 # number of cells
a = 0; b = 1 # the calculation domain [a,b]
uniform_grid = False
if uniform_grid:
    delta = (b-a)/N
else:
    # We can use an arbitrary non-uniform grid
    delta = fdo.random_grid_delta(b-a, N, 0.9)
grid = fdo.Grid(num_cells=N, delta=delta, start=a)
geom = fdo.GeometryCartesian((grid,))

# Set up f
f = Field('f', (N,), stags=(0,), nls=(0,), nus=(0,))
f.bc[0] = 1; f.bc[end] = 0; f.bc.freeze() # the BCs
f[1:end-1] = 0 # initial value at all points except boundaries
f.bc.apply() # enforce the explicit BCs
# Set up diffusion
D = 0.01
def Laplacian(fun):
    return geom.laplacian(fun)
    # Same as:
    # return n_diff(c_diff(fun,0)/geom.grids[0].dx,0)/geom.grids[0].dx0
    # or return geom.grad(geom.grad(fun,0),0)
dt = 0.01
implicit_diffusion = Solver(f,(f.symb - Laplacian(f.symb)*D*dt).view[1:end-1])

#%% Time cycle: Solve diffusion
T = 100
t = 0
tstart = time.time()
while t <= T:
    # Solve with the implicit BCs
    implicit_diffusion.solve_full(f,f.view[1:end-1]) # calculate f in terms of f[1:end-1]
    t += dt
print(time.time()-tstart)

#%% Plot the results
import matplotlib.pyplot as plt

#x1 = grid._arr(stag=f.stags[0], nl=f.nls[0], nu=f.nus[0]) # This is a regular array
x = grid.r_n # this is an extended_array

plt.figure(1)
plt.plot(x[0:end], f[0:end],'.-')
plt.show()

#%% Cleanup (not needed if using 'with numpy_override')
fdo.numpy_override.complete_exit()
