#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 18 18:48:23 2017

OBSOLETE, DO NOT USE!

Test of advective schemes in cylindrical coordinates.

The velocity profile we test is (such that div v = 0, vz=0 at z=+-L/2 and vr=0 at r=R):
    
    vr = vo (π/L) J_1(br/R) sin(πz/L) = -dψ/dz 
    vz = vo bJ_0(br/R) cos(πz/L) = (1/r)d(rψ)/dr

with b being the first root of J_1, b=spec.jn_zeros(1,1)[0]=3.8317

It may be represented as curl A where A_φ = ψ and is given by

    ψ(r,z) = vo J_1(br/R) cos(πz/L)


@author: nle003
"""

#%% Preliminaries
import sys
import os
import time
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import scipy.special as spec
#from utils import makedir
import fidelior_old as fdo
from fidelior_old import end, half, span
from fidelior_old.plotting import UpdateablePcolor, ea_plot, UpdateablePcolorSubplots
from fidelior_old import autoschemes as AS

def makedir(dirname):
    "Create directory *dirname* with subdirectories, warn if create, do nothing if exists"
    if not os.path.isdir(dirname):
        print(fdo.co.red('Warning:'),'Creating directory',dirname)
        os.makedirs(dirname)

class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

besselzero = spec.jn_zeros(1,1)[0]

fdo.set_global_debug_level(0)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

j1_fun = fdo.extend_vectorized_function()(spec.j1)
#def c_avez(f): return c_ave(f,1)

#%%  Setup
multifig = False
savefig = None # ['png', 'pkl']
shapetype = 'ring'
nsmear = 3
vo = 1
R = 50
L = 100
Nr = 100
Nz = 200
gridtypes = ['uniform', 'uniform']
gridtypes = ['random', 'random']
#gridtypes = ['random', 'uniform']

#methods = ['CIR', 'MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']
methods = ['CIR_interp', 'CIR_discr', 'LW_interp', 'WB_interp', 'Fromm_interp', 'ORD3_fem']
#methods = ['CIR_discr', 'MacCormack', 'Fromm_interp', 'ORD3_interp', 'CIR_fem', 'LW_fem', 'ORD3_fem']
#methods = ['CIR_fem', 'LW_fem', 'ORD3_fem', 'CIR_interp']
methods = ['CIR_discr', 'MacCormack', 'CIR_fem', 'LW_fem', 'ORD3_fem', 'ORD3_interp']
nfig = len(methods)


#%% Set up the grid and geometry
def get_dr(L, N, gridtype):
    if gridtype=='uniform':
        dr = L/N # *np.ones((N,))
    elif gridtype=='random':
        random_coef = 0.5
        dr = fdo.random_grid_delta(L, N, random_coef)
    elif gridtype=='growing':
        refine_coef = 0.3
        dr = fdo.growing_grid_delta(L, N, L/N*refine_coef)
    else:
        raise Exception('unknown gridtype')
    return dr
dr = get_dr(R, Nr, gridtypes[0])
dz = get_dr(L, Nz, gridtypes[1])
gridr = fdo.Grid(num_cells=Nr,delta=dr,start=0)
gridz = fdo.Grid(num_cells=Nz,delta=dz,start=-L/2)
cyl = fdo.GeometryCylindrical((gridr,gridz), names=('r','z'),nls=(3,3),nus=(3,3))
def get_tot(f):
    return cyl.integrate(f, [[0, R], [-L/2, L/2]]).item()
r = gridr.r_n
rc = gridr.r_c
z = gridz.r_n
zc = gridz.r_c

#%% Velocity
divergence_free = True
if divergence_free:
    if True:
        psi = vo*j1_fun(besselzero*rc/R)*np.cos(np.pi*zc/L)
    else:
        # Psi can actually be arbitrary, just make sure vr=0 at r=0
        psi = vo*np.sin(np.pi*rc/R)*np.cos(np.pi*zc/L)
    vr = -cyl.grad(psi,1) # vx at points (i-1/2,j)
    vz = cyl._div(psi, 0)
else:
    vr = vo*np.sin(np.pi*rc/R)*np.sin(np.pi*z/L)
    vz = vo*np.cos(np.pi*r/R)*np.cos(np.pi*zc/L)
    
# Check if the divergence is zero at points (i,j)
div_v = cyl.divergence([vr,vz])
print('max div v =',np.max(np.abs(div_v)),flush=True)

dt = min(np.min(dr),np.min(dz))/max(np.max(vr),np.max(vz))

w = (-vr*dt, -vz*dt)

#%% Initial value
fini = fdo.Field('fini', cyl.ncells, stags=(0, 0), nls=(1, 0), nus=(0, 0))
fini.setv = 0
fini.bc[-1,:] = fini.bc[1,:]
fini.bc.freeze()
#fini.arr = np.zeros(fini.nts)
if shapetype=='rectangle':
    z0=0;
    ar=2*R/3; az=L/4
    mask = (np.abs(z - z0) < az) & (np.abs(r)<ar)
    fini[mask] = 1
    def smear(f,n):
        for k in range(n):
            f.setv = fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(f,0),0),1),1)
            f.bc.apply()
        return f
    smear(fini, nsmear)
elif shapetype=='ring':
    z0=0; r0=R/2
    ar=R/3; az=L/4
    mask = (np.abs(z - z0) < az) & (np.abs(r-r0)<ar)
    fini[mask] = 1
    def smear(f,n):
        for k in range(n):
            f.setv = fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(f,0),0),1),1)
            f.bc.apply()
        return f
    smear(fini, nsmear)
elif shapetype=='full':
    fini.setv = 1
else:
    raise Exception('Unknown shape '+shapetype)

#%%
totals = np.zeros((nfig,))
fs=[]
for kfig in range(nfig):
    alg = methods[kfig]
    dd = 3
    # if alg in ['CIR']:
    #     dd=1
    # elif alg in ['MacCormack','MacCormack_Reverse','UTOPIA','ULTIMATE']:
    #     dd=2
    # else:
    #     raise Exception('Unknown method')
    f0 = fdo.Field('f_'+alg, cyl.ncells, stags=(0,0), nls=(dd,dd), nus=(dd,dd))
    tt1 = time.time()
    for k in range(dd):
        f0.bc[-k-1,:end-1]=f0.bc[k+1,:end-1] # The values at nz=end are dependent due to periodic bc
        f0.bc[:,end+k]=f0.bc[:,k]
    for k in range(1,dd+1):
        f0.bc[:,-k]=f0.bc[:,end-k]
    tt2 = time.time()
    assert f0.bc.num>0 # we have set at least some boundary conditions
    f0.bc.freeze() # takes pretty long
    tt3 = time.time()
    print('To record: t = ',tt2-tt1,', to freeze = ',tt3-tt2,flush=True)
    # zfg,rfg=np.meshgrid(f0.xe(1),f0.xe(0))
    # zc=0;
    # ar=2*R/3; az=L/4
    # f0.arr[(np.abs(gridz.r_n - zc) < az) & (np.abs(gridr.r_n)<ar)]=1.
    f0.setv = 0
    f0.setv = fini
    totals[kfig]=get_tot(f0)
    fs.append(f0)
print('totals =',totals)


#%% Discrete schemes
DISCR_SCHEMES = ['CIR_discr', 'MacCormack', 'MacCormack_Reverse']
def discr_advection_step(cyl, f, w, alg):
    wr, wz = w
    if alg=='CIR_discr':
        # First-order upstream method [Courant, Isaacson and Rees, 1952]
        fvr = fdo.c_upc(f, -wr, 0)
        fvz = fdo.c_upc(f, -wz, 1)
        df = cyl.divergence([wr*fvr, wz*fvz])
    elif alg=='MacCormack_Reverse':
        # Stable, 2nd order
        # Even more stable than Lax-Wendroff
        # Slightly better than MacCormack?? About the same
        # 1. Calculate interim value
        fi = f + cyl.divergence([wr*fdo.c_upc(f,wr,0), wz*fdo.c_upc(f,wz,1)])
        df = (-f+fi)/2 + cyl.divergence([wr*fdo.c_upc(fi,-wr,0), wz*fdo.c_upc(fi,-wz,1)])/2.
    elif alg=='MacCormack':
        # Stable, 2nd order
        # 1. Calculate interim value
        fi = f + cyl.divergence([wr*fdo.c_upc(f, -wr, 0), wz*fdo.c_upc(f,-wz,1)])
        df = (-f+fi)/2 + cyl.divergence([wr*fdo.c_upc(fi,wr,0), wz*fdo.c_upc(fi,wz,1)])/2.
    return df

#%% Automatic methods
f_indep = fdo.ExtendedArray(cyl.ncells, stags=(0, 0), nls=(1, 1), nus=(1, 1))

#%%
degree = 0
stencil = [(0,0)]
CIR_fem = AS.fem_scheme_sp(cyl, f_indep, degree, stencil)

#%%
stencil = AS.odd_order_stencil(2,1) #[(0,0), (1,0), (0,1)]
degree = 1
CIR_interp = AS.interpolation_scheme_sp(cyl, f_indep, degree, stencil)
LW_fem = AS.fem_scheme_sp(cyl, f_indep, degree, stencil)

#%% The sanity check for the new algorithm
FEM = LW_fem
geom = cyl
u = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(2,2), nus=(2,2))
u.arr = np.random.rand(*u.nts)
for p in FEM.stencil:
    Deltar1 = []
    Deltar2 = []
    for a, gr in enumerate(geom.grids):
        shift = list(p)
        shift[a] = p[a]-half
        d1 = gr.r_c.shifted(shift)-gr.r_n
        Deltar1.append(d1)
        shift[a] = p[a]+half
        d2 = gr.r_c.shifted(shift)-gr.r_n
        Deltar2.append(d2)
    # A more thorough sanity chack #2 -- integrate the neighbor cell
    D1 = tuple(d.broadcast(f_indep) for d in Deltar1)
    D2 = tuple(d.broadcast(f_indep) for d in Deltar2)
    unext = FEM.integrate(u, D1, D2)
    unext = unext/FEM.dvolume.shifted(p)
    print('Point =', p, ', method error =', np.max(np.abs(unext-u.shifted(p))))

#%%
stencil = AS.even_order_stencil(2, 1)
degree = 2
LW_interp = AS.interpolation_scheme_sp(cyl, f_indep, degree, stencil)
ORD3_fem = AS.fem_scheme_sp(cyl, f_indep, degree, stencil, True)

#%%
stencil = AS.warming_beam_stencil(2,1)
WB_interp = AS.interpolation_scheme_sp(cyl, f_indep, degree, stencil)

stencil = AS.odd_order_stencil(2,2)
degree = 3
ORD3_interp =  AS.interpolation_scheme_sp(cyl, f_indep, degree, stencil)

INTERP_SCHEMES = {'CIR_interp':CIR_interp, 'LW_interp':LW_interp,'WB_interp':WB_interp,
                  'ORD3_interp':ORD3_interp}
FEM_SCHEMES = {'CIR_fem':CIR_fem, 'LW_fem':LW_fem, 'ORD3_fem':ORD3_fem}

#%% Composite (combination) schemes
COMP_SCHEMES = ['Fromm_interp']
def comp_advection_step(cyl, f, w, alg):
    wr, wz = w
    if alg=='Fromm_interp':
        wlim = (fdo.n_mean(wr, 0).view[span(f_indep)], fdo.n_mean(wz, 1).view[span(f_indep)])
        return (LW_interp.interpolate(f, wlim) + WB_interp.interpolate(f, wlim))/2-f

def advection_step(cyl, f, w, alg):
    if alg in DISCR_SCHEMES:
        return discr_advection_step(cyl, f, w, alg)
    elif alg in INTERP_SCHEMES.keys():
        # Convert cr, cz to the correct shape
        #wr, wz = w
        #wlim = (cyl.n_aver(wr, 0).view[span(f_indep)], cyl.n_aver(wz, 1).view[span(f_indep)])
        wlim = (fdo.n_mean(w[0], 0).view[span(f_indep)], fdo.n_mean(w[1], 1).view[span(f_indep)])
        return INTERP_SCHEMES[alg].interpolate(f, wlim)-f
    elif alg in FEM_SCHEMES.keys():
        wlim = (w[0].view[-half:end+half,-1:end+1], w[1].view[-1:end+1,-half:end+half])
        return FEM_SCHEMES[alg].fem_advection_step(f, wlim)
    elif alg in COMP_SCHEMES:
        return comp_advection_step(cyl, f, w, alg)
    else:
        raise Exception('Unknown scheme: '+alg)
    
#%% Initialize figures
zmin = -0.4
zmax = 1.4
cmap = mpl.cm.jet
if multifig:
    figs = []
    for k in range(nfig):
        if savefig is None:
            savedir = None
        else:
            savedir = 'fig_'+shapetype+'/' + methods[k]
            makedir(savedir)
        fig_ = UpdateablePcolor(100+k, figsize=(6,5), zmin=zmin, zmax=zmax, cmap=cmap,
                                savedir=savedir, savefmts=savefig)
        figs.append(fig_)
else:
    nrows = np.int(np.round(np.sqrt(nfig)))
    ncols = np.int(np.ceil(nfig/nrows))
    if savefig is None:
        savedir = None
    else:
        savedir = 'fig_combined_'+shapetype
        makedir(savedir)
    the_fig = UpdateablePcolorSubplots(nrows, ncols, num=100, zmin=zmin, zmax=zmax, savedir=savedir,
        savefmts=savefig, figsize=(4*ncols+1, 3*nrows+1), cmap=cmap)
info = lambda k,f,m: '[{:.3g},{:.3g}],k={},{:s}'.format(np.min(f.arr),np.max(f.arr),k,m)
#rc,zc = cyl.xcs

#%%
dtot = np.zeros((nfig,))
if not multifig:
    kplot = 0
for kt in range(3001):
    for kfig in range(nfig):
        f = fs[kfig]
        #tmp = cyl.AdvectionStepI(f,vr*dt,vz*dt,methods[kfig])
        #print(get_tot(tmp))
        f += advection_step(cyl, f, w, methods[kfig])
        #f[f<0]=0
        f.bc.apply()
        #dtot = cyl.integrate(f,rb=np.array([0,rc[end]]),zb=np.array([zc[0],zc[end]]))-totals[kfig]
        dtot[kfig] = get_tot(f)-totals[kfig]
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        if multifig:
            for kfig in range(nfig):
                fig_ = figs[kfig]
                if fig_.started_plotting and not plt.fignum_exists(fig_.fignum):
                    print('Figure closed, exiting ...')
                    plt.close('all')
                    sys.exit()
                title = info(kt,fs[kfig],methods[kfig]+' '+str(neat_float(dtot[kfig])))
                fig_.plot(cyl, fs[kfig],title=title)
        else:
            if not plt.fignum_exists(the_fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()                
            for kfig in range(nfig):
                title = info(kt,fs[kfig],methods[kfig]+' '+str(neat_float(dtot[kfig])))
                the_fig.subplot(kfig+1, cyl, fs[kfig],title=title)
            the_fig.save(kplot)
            kplot+=1
        plt.pause(0.1)
        # fig[0].wait() # progress by clicking mouse in Figure 100
