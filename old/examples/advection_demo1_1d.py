#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 12:24:56 2020

Solution of advective equation in 1D:

df/dt + div(v*f) = 0

using various linear schemes.

f in div(v*f) is represented as a "face value" of f

Literature:

Leonard [1979] - 3rd order upwind scheme in 1D (called QUICKEST)
Leonard [1991] - the flux-limiting algorithm in 1D (called ULTIMATE)

@author: nleht
"""

#%% Preliminaries
# Imports
import sys
import numpy as np
import matplotlib.pyplot as plt

import fidelior_old as fdo
from fidelior_old import Field, ExtendedArray, end, span, \
    c_diff, n_diff, c_mean, n_mean, c_ups, n_ups, c_upc, n_upc, c_interp_ups, c_interp_upc

# Choose method_set=0 to see several linear methods, method_set=1 for flux-limited methods.
method_set = 7

#%% More preliminaries
fdo.set_global_debug_level(3) # 0 is dangerous!
fdo.set_sparse(True)
if not fdo.numpy_override.entered:
    # Override the NumPy functions to work with extended_array objects
    fdo.numpy_override.enter()

def maximum3(x, y, z):
    return np.maximum(x, np.maximum(y, z))
def minimum3(x, y, z):
    return np.minimum(x, np.minimum(y, z))
def minmax3(f1, f2, f3, s):
    return fdo.minmax(f1, fdo.minmax(f2, f3, s), s)
def minmod(f1, f2):
    s = np.sign(f1)
    s *= (np.sign(f2)==s)
    return s*np.minimum(np.abs(f1), np.abs(f2))
def harmonic_mean(f1, f2):
    "Harmonic mean; if different signs, then zero"
    s = np.sign(f1)
    s *= (np.sign(f2)==s)
    denom = np.abs(f1) + np.abs(f2)
    i = (denom!=0)
    num = 2*np.abs(f1*f2)
    # Not necessary: i.view[intersection_span(i, num, denom)]
    num[i] /= denom[i]
    return s*num
class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

#%% GeneratedScheme
class GeneratedScheme:
    """Automatically generated scheme of N-th order, with stencil shifted by M from origin.
    Arbitrary stencils are also allowed."""
    def __init__(self,N=None,M=None,mask=None,stencil=None):
        if stencil is None:
            self.name = 'auto(N='+str(N)+', M='+str(M)+')'
            self.stencil = np.array(range(M,M+N+1))
            if mask is not None:
                self.stencil = self.stencil[mask]
        else:
            self.stencil = np.unique(stencil) # also sorts
            self.name='auto, S='+str(self.stencil)
        self.order = len(self.stencil)-1
        self.polys = [self.make_poly(j) for j in self.stencil]
    def make_poly(self,k):
        def func(x):
            "Normalized so that = 1 at k"
            p = 1
            for i in self.stencil:
                if i != k:
                    p *= (x-i)/(k-i)
            return p
        return func
    def poly(self,k,x):
        return self.polys[np.searchsorted(self.stencil,k)](x)
    def step(self,f,c):
        # only scalar c
        df = -f
        for k in self.stencil:
            df += self.poly(k,-c)*f.shifted((k,))
        return df
    def get_coef(self,c):
        # Not used for calculations, only demo
        return np.array([self.poly(k, -c) for k in self.stencil])
 
#%%
def facevalue_leonard(f, v):
    "v is dimensionless"
    # The flux is fluxx=vxcx*fvx and fluxy=vycy*fvy
    # First-order face value
    if np.isscalar(v):
        fv1 = c_ups(f, v)
    elif isinstance(v, ExtendedArray):
        fv1 = c_upc(f, v)
    else:
        raise ValueError('invalid v')
    # Third order face value
    # It is only function of df, no f
    df = c_diff(f)
    # Corrected df for 3rd-order method
    nv = np.sign(v)
    if np.isscalar(v):
        dfc = c_interp_ups(df, (nv+v)/3.)
    else:
        dfc = c_interp_upc(df, (nv+v)/3.)
    dfv3 = (nv-v)/2.*dfc
    # Face value is
    # f=fv1+dfv3 at (i-1/2)
    return (fv1,dfv3)

# The flux correction algorithm of Leonard [1991]
def tvd_ultimate(f, v, fv1, dfv3, apply=True):
    "v is dimensionless"
    if not apply:
        fluxl=(fv1+dfv3)*v
        return fluxl
    # The point where face value is determined lies halfway between
    # phiC and phiD (see figure 14), phiU is further upstream
    # Leonard's [1991] phiC, phiD and phiU
    #fC = 0 # c_up(f,vcr[a],a) # same as fv1[a]
    df = c_diff(f)
    fv = dfv3 # The first-order face value is fC, which is taken zero here
    fD = np.sign(v)*df
    if np.isscalar(v):
        CURV = c_ups(n_diff(df), v)
    else:
        CURV = c_upc(n_diff(df), v)
    fU = CURV - fD
    DEL= fD - fU
    fref = fU.copy_arr()
    #fref.arr = fU.arr.copy()
    if isinstance(v, ExtendedArray):
        inz = (v!=0)
    elif fdo.co.is_real_scalar(v):
        inz = fU.copy()
        inz.arr = np.ones(inz.nts, dtype=np.bool) & (v!=0)
    else:
        raise Exception('Internal error')
    tmp = fU.copy()
    tmp.arr = np.zeros(tmp.nts)
    tmp.setv = np.abs(v)
    fref[inz] -= fU[inz]/tmp[inz] # limiting value for small r (phi = 2r/c)
    # fref = fU *(1 - 1/c)
    # Note that when velocity is zero, flux is zero and there is nothing
    # to limit.
    s=np.sign(DEL)
    # fD is limiting value for large r (phi = 2/(1-c))
    fvnew = fdo.minmax(minmax3(fv, fref, fD, s), 0, -s)
    ic = (np.abs(CURV)>=np.abs(DEL)) # region with r<0
    fvnew[ic] = 0
    fluxl = v*(fv1 + fvnew)
    return fluxl

def advection_step(geom, f, c, alg=None):
    "v is dimensionless, i.e., the CFL factor"
    if alg is None:
        raise Exception('Need an algorithm')
    if alg in SCHEMES.keys():
        return  SCHEMES[alg].step(f, c)
    #assert isinstance(f.geom, fdo.geometry_flat) and np.isscalar(v)
    #delta = f.geom.grids[0].delta
    # First-order face value
    fv1 = c_ups(f,c)
    # Third order face value
    # It is only function of df, no f
    df = c_diff(f)
    # Corrected df for 3rd-order method
    nc = np.sign(c)
    if alg=='CIR':
        flux = c*fv1
        return -n_diff(flux)
    if alg in ['QUICKEST', 'ULTIMATE']:
        fv1, dfv3 = facevalue_leonard(f, c) # in cylindrical, these are facevalue x radius
        flux = tvd_ultimate(f, c, fv1, dfv3, apply=(alg=='ULTIMATE'))
        return -n_diff(flux)
    if alg in ['Fromm','Warming-Beam','vanLeer_orig','Lax-Wendroff','Leonard']:
        # QUICKEST corresponds to s = (nv+v)/3., Lax-Wendroff to s = v*0
        if alg=='Fromm':
            s = nc/2.
        elif alg=='Warming-Beam':
            s = nc
        elif alg=='Leonard': # QUICKEST
            s = (nc + c)/3
        elif alg=='vanLeer_orig':
            # van Leer (1974)
            dfu = np.abs(n_ups(df,-1))
            dfd = np.abs(n_ups(df,1))
            denom = dfu+dfd
            S = dfu-dfd
            ii = (denom != 0) # logical extended_array
            # ii = ii.view[intersection_span(ii, S, denom)] # ii restricted to both S and denom
            # But in this case it is not nedessary because they are of the same size
            S[ii] /= denom[ii]
            s = (nc + c_upc(S, c))/2
        elif alg=='Lax-Wendroff':
            # For v=const, is the same as MacComrack (and MacCormack_reverse)
            s = c*0
        else:
            raise Exception('internal error')
        if np.isscalar(s):
            dfc = c_interp_ups(df,s)
        else:
            dfc = c_interp_upc(df,s)
        dfv3 = (nc - c)/2.*dfc
        # Face value is
        # f=fv1+dfv3 at (i-1/2)
        flux = (fv1+dfv3)*c
        return -n_diff(flux)
    elif alg in ['vanLeer', 'superbee', 'Sweby1.5', 'Koren1', 'Koren2', 'Koren-Leonard',
                 'ULTIMATE\'', 'minmod', 'LW limited', 'WB limited', 'Fromm limited']:
        # See https://en.wikipedia.org/wiki/Flux_limiter
        dfv2 = (nc - c)/2.*df
        dfu = n_ups(df,-1.)
        dfd = n_ups(df,1.)
        # r = dfd/dfu, do it carefully:
        r = dfd.copy()
        r.arr = dfd.arr.copy()
        ii = (dfu.arr != 0)
        r.arr[ii] /= dfu.arr[ii]
        r.arr[~ii] = np.inf # infinity
        if alg=='vanLeer':
            # Same as van Leer (1974), according to Sweby (1984)
            # It's a kind of magick!
            denom = np.abs(dfu)+np.abs(dfd)
            phi = np.sign(dfu)*dfd + np.abs(dfd)
            ii = (denom != 0) # logical extended_array
            # ii = ii.view[intersection_span(ii, phi, denom)] # ii restricted to both phi and denom
            # But in this case it is not nedessary because they are of the same size
            phi[ii] /= denom[ii]
        elif alg=='superbee':
            phi = maximum3(np.minimum(2*r, 1), np.minimum(r, 2), 0)
        elif alg=='Sweby1.5':
            # beta==2 is superbee, beta==1 is minmod
            beta = 1.5
            phi = maximum3(np.minimum(r*beta, 1), np.minimum(r, beta), 0)
        elif alg=='Koren1':
            # Quoted on wikipedia
            phi = np.maximum(minimum3(2*r, (1+2*r)/3, 2), 0)
        elif alg=='Koren2':
            # Quoted by Kuzmin 2006
            phi = np.maximum(minimum3(2*r, (2+r)/3, 2), 0)
        elif alg=='Koren-Leonard':
            # My own invention - make Koren really 3-rd order (Leonard) at r~1
            # Gives wikipedia Koren for c=0 and Kuzmin Koren for c=1
            phi = np.maximum(minimum3(2*r, ((1+c)*r + (2-c))/3, 2), 0)
        elif alg=='ULTIMATE\'':
            # Koren-Leonard modified to be more accurate at large and small r
            # And this way we get ULTIMATE again (Leonard, 1991)
            # Equation (13.158) in Toro (2009) book
            phi = np.maximum(minimum3(2/c*r, ((1+c)*r + (2-c))/3, 2/(1-c)), 0)
        elif alg=='minmod':
            phi = np.maximum(0, np.minimum(r, 1))
        elif alg=='LW limited':
            phi = np.maximum(minimum3(2/c*r, 1, 2/(1-c)), 0)
        elif alg=='WB limited':
            phi = np.maximum(minimum3(2/c*r, r, 2/(1-c)), 0)
        elif alg=='Fromm limited':
            phi = np.maximum(minimum3(2/c*r, (1+r)/2, 2/(1-c)), 0)
        else:
            raise Exception('Internal error')
        # The first order flux fv1*v corrected
        # by adding second order flux dfv2*v with coefficient phi
        # (shifted upwind with function 'c_upc')
        flux = (fv1 + c_ups(phi,c)*dfv2)*c
        return -n_diff(flux)
    elif alg=='ENO2':
        # Non-oscillatory, Harten and Osher [1997]
        # Second order method
        # We are on a uniform grid with step one, for all intensive purposes.
        # 1. Interpolate with parabolas for each 3 points
        # Q function from page 284
        # 
        Di = n_diff(df)
        D = minmod(Di.lo(), Di.hi()) # equation (3.2d)
        # The function on interval around i+1/2 is now given by equation (3.2a)
        # 2. Choose the slope S_i, equation 4.2b
        dQdxp = (df - D).hi()
        dQdxm = (df + D).lo()
        S = minmod(dQdxp, dQdxm)
        #S = (dQdxp+dQdxm)/2 # modification of Fromm (would be Fromm for D==0)
        #S = n_ups(df, -c) # LW
        #S = n_ups(df, c) # WB
        #S = n_mean(df) # Fromm
        # 3. Integrate the piecewise-linear in each interval [x_i-1/2, x_i+1/2] function L
        # in interval [x-1/2, x+1/2], where x = x_i-c, as given by equation (5.3a)
        # Using equation (4.5b): (dLbar = Lbar-ubar)
        flux = c_ups(f + S*(nc-c)/2, c)*c
        #deltaf = -c*(df + c_diff(S)*(1-c)/2).lo()
        deltaf = -n_diff(flux)
        return  deltaf
    elif alg=='ENO2 minmod':
        # Almost as good! But much simpler
        S = minmod(df.hi(), df.lo()) # same as "minmod", but algorithmically much more intuitive
        flux = c_ups(f + S*(nc-c)/2, c)*c
        return -n_diff(flux)
    elif alg=='ENO2 Fromm':
        # My own, sortof average between minmod and Fromm
        dfmean = n_mean(df) # if we use just this, it would be Fromm
        S1 = minmod(df.hi(), dfmean)
        S2 = minmod(df.lo(), dfmean)
        S = (S1+S2)/2
        flux = c_ups(f + S*(nc-c)/2, c)*c
        return -n_diff(flux)
    elif alg=='ENO2 bisectrix':
        # Like Fromm, except not average slope but along bisectrix
        # This is the only place the geom is used
        dx = geom.grids[0].delta # provides a scale of f, slope "large" when angle is close to pi/2
        th_wb = np.arctan2(df.lo(), dx)
        th_lw = np.arctan2(df.hi(), dx)
        th = (th_wb+th_lw)/2 # bisectrix
        S = np.tan(th)*dx # the chosen slope
        flux = c_ups(f + S*(nc-c)/2, c)*c
        return -n_diff(flux)   
    elif alg=='ENO2 harm mean':
        # van Leer method made transparent: harmonic mean of slopes
        S = harmonic_mean(df.lo(), df.hi())
        flux = c_ups(f + S*(nc-c)/2, c)*c
        return -n_diff(flux)
    elif alg=='RK2+minmod':
        # Chi-Wang Shu, page 386
        # Still worse than all the other methods.
        S = minmod(df.hi(), df.lo()) # slope for minmod
        deltaf = -n_diff(c_ups(f + S*(nc-0)/2, c)*c)
        f1 = f + deltaf  # intermediate result
        df1 = c_diff(f1)
        S1 = minmod(df1.hi(), df1.lo())
        deltaf1 = -n_diff(c_ups(f1 + S1*(nc-0)/2, c)*c)
        return (f1 + f)/2 + deltaf1/2 - f
    else:
        raise Exception('Unknown algorithm: ' + alg)

################################################################################
#%% Testing
CFL = 0.5 # Optimal value is 1, Courant–Friedrichs–Lewy condition, must be 0<CFL<1
num_smear = 0

# Setup the grid and velocity field: CCW for r<R1, CW for R1<r<R2, w=vmax/R2
# The unknown function f is calculated on a Nx x Ny array, at x,y coordinates
L = 2
N = 200
dx = L/N
grid = fdo.Grid(num_cells=N, delta=dx, start=-L/2)
geom = fdo.Geometry((grid,), nls=(5,), nus=(5,))
ncells = (N,)
x0 = grid.r_n

def waveform(x,kt,f):
    f.setv = 0
    xs = ((x - CFL*dx*kt + L/2) % L) - L/2
    ii = (xs > 0) & (xs < L/4)
    f[ii] = 2*xs.view[span(f)][ii]
    ii = np.abs(xs+L/4)<L/8
    f[ii] = np.sqrt(1-((xs[ii]+L/4)/(L/8))**2)
    #return f

################################################################################
#%% Prepare the figures

SCHEMES = {'CIR\'': GeneratedScheme(N=1,M=-1),
           'LW\'': GeneratedScheme(N=2,M=-1),
           'WB\'': GeneratedScheme(N=2,M=-2),
           'Leonard\'': GeneratedScheme(N=3,M=-2),
           '5th order': GeneratedScheme(N=5,M=-3) }

if method_set==0:
    # Methods of various orders without flux correction (linear methods)
    methods = ['CIR','Lax-Wendroff','Leonard', '5th order']
elif method_set==1:
    # Second-order methods with flux correction, compared to the third-order
    methods = ['vanLeer','superbee','Sweby1.5','minmod','Leonard','ULTIMATE\'', 'ENO2',
               'ENO2 experiment']
elif method_set==2:
    # Only second-order methods without flux correction
    methods = ['Lax-Wendroff', 'Warming-Beam', 'Fromm']
elif method_set==3:
    # Third-order methods with and without flux correction
    methods = ['Koren1','Koren2','Koren-Leonard','Leonard']
elif method_set==4:
    # Compare second- and third-order flux-limited methods
    methods = ['Koren-Leonard', 'vanLeer','minmod', 'superbee']
elif method_set==5:
    # Third-order methods
    methods = ['Leonard', 'QUICKEST', 'Koren-Leonard', 'ULTIMATE\'', 'ULTIMATE']
elif method_set==6:
    # Compare generated methods and those given by the formula
    methods = ['Leonard','Leonard\'']
elif method_set==7:
    # First- and second-order methods
    methods = ['CIR', 'Lax-Wendroff', 'Fromm', 'ENO2', 'ENO2 Fromm', 'ENO2 minmod',
               'ENO2 bisectrix', 'RK2+minmod', 'ENO2 harm mean']
else:
    raise Exception('Unknown method set')
nmethods=len(methods)
zmin,zmax = (-0.4,1.4)
fig = plt.figure(100,figsize=(10,5))

#%%#############################################################################
# Initial value
fini = ExtendedArray(ncells, stags=(0,), nls=(5,), nus=(5,))
fini.arr = np.full(fini.shape, fill_value=np.nan)
waveform(x0, 0, fini)
def smear(ea,n=0):
    for k in range(n):
        ea.setv = n_mean(c_mean(ea))
    #return ea
smear(fini,num_smear)
fexact = fini.copy_arr()
   
################################################################################
#%% The main cycle
# Initial value

farr=[]
for kmethod in range(nmethods):
    alg = methods[kmethod]
    if alg=='CIR':
        # First-order methods require fewer ghost cells
        dd = 1
    elif alg in SCHEMES.keys():
        dd = np.max(np.abs(SCHEMES[alg].stencil))
    elif alg=='ENO2':
        dd = 3 # too many intermediate computations which chop off stuff at the ends
    elif alg=='RK2+minmod':
        dd = 4
    else:
        dd = 2
    #dd = 2
    f0 = Field('f'+alg, ncells, stags=(0,), nls=(dd,), nus=(dd,))
    # The boundary conditions may set f0.bc[0]=f0.bc[end], too -- 
    # it is independent of direciton
    f0.bc[end:end+dd]=f0.bc[0:dd]
    f0.bc[-dd:-1]=f0.bc[end-dd:end-1]
    f0.bc.freeze()
    farr.append(f0)
    
for f0 in farr:
    f0.setv = 0
    f0.setv = fini

#%%
tot_turns = 10
d_n_turns = CFL*dx/L
Nt = np.int(np.ceil(tot_turns/d_n_turns))
print('Wait for',d_n_turns*Nt,'turns')
line = [None]*nmethods
for kt in range(Nt+1):
    for kmethod in range(nmethods):
        farr[kmethod] += advection_step(geom, farr[kmethod], CFL, methods[kmethod])
        farr[kmethod].bc.apply()
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0 or kt==Nt:
        #n_turns = kt*d_n_turns
        if not plt.fignum_exists(fig.number):
            print('Figure closed, exiting ...')
            sys.exit()
        plt.figure(fig.number)
        plt.clf()
        for kmethod in range(nmethods):
            alg = methods[kmethod]
            if alg in SCHEMES.keys():
                alg += ': '+SCHEMES[alg].name
            f = farr[kmethod]
            line, = plt.plot(x0[:],f[:],'.-')
            line.set_label(alg)
        # Plot the exact value
        waveform(x0, kt+1, fexact)
        plt.plot(x0[:],fexact[:],'k',alpha=0.3)
        plt.legend()
        plt.grid(True)
        plt.ylim(zmin,zmax)
        plt.xlim(-L/2,L/2)
        plt.title('n = '+str(neat_float(d_n_turns*kt)))
        plt.pause(0.1)
        # fig.wait() # progress by clicking mouse in Figure 100

plt.show()
