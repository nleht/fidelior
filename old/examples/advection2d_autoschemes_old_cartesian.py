#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 13:35:17 2020

OBSOLETE, DO NOT USE!

Test of various advection schemes in 2D non-uniform grids, with constant velocity

Periodic boundary conditions, automatic schemes with given stencil

@author: nle003
"""

#%% Mandatory imports
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import fidelior_old as fdo
from fidelior_old import end, half, span
from fidelior_old.plotting import UpdateablePcolor, ea_plot
from fidelior_old import autoschemes as AS

fdo.set_global_debug_level(0)
if not fdo.numpy_override.entered:
    fdo.numpy_override.enter()

def makedir(dirname):
    "Create directory *dirname* with subdirectories, warn if create, do nothing if exists"
    if not os.path.isdir(dirname):
        print(fdo.co.red('Warning:'),'Creating directory',dirname)
        os.makedirs(dirname)
        
class neat_float(float):
    "Short-print of floats"
    def __str__(self):
        return "%0.5g" % self.real
    def __repr__(self):
        return self.__str__()

#%% Settings
savefig = False
speedtype = 'rotation' # Alternatives: 'uniform', 'rotation', 'div-less'
shapetype = 'rectangle' # Alternatives: 'rectangle', 'full'
if shapetype=='full':
    dzlim = .2
if shapetype=='rectangle':
    nsmear = 6
if speedtype=='rotation':
    bctype='zero'
elif speedtype=='uniform' or speedtype=='div-less':
    bctype='periodic'
if speedtype=='uniform':
    V = np.array([.3, .7])
alpha = 1
Nx = 100
Ny = 100
Lx = 100
Ly = 100
#gridtypes = ['uniform', 'uniform']
gridtypes = ['random', 'random']
#gridtypes = ['random', 'random']

#methods = ['CIR','LWtail','UTr','UTl']
methods = ['CIR_fem', 'LW_fem', 'ORD3_fem']
methods = ['CIR_fem', 'CIR', 'LW_fem', 'LWtail']
methods = ['ORD3_fem', 'ORD5_fem', 'ORD7_fem']

#%% Set up the grid and geometry
def get_dr(L, N, gridtype):
    if gridtype=='uniform':
        dr = L/N
    elif gridtype=='random':
        random_coef = 0.5
        dr = fdo.random_grid_delta(L, N, random_coef)
    elif gridtype=='growing':
        refine_coef = 0.3
        dr = fdo.growing_grid_delta(L, N, L/N*refine_coef)
    else:
        raise Exception('unknown gridtype')
    return dr
dx = get_dr(Lx, Nx, gridtypes[0])
dy = get_dr(Ly, Ny, gridtypes[1])
gridx = fdo.Grid(num_cells=Nx, delta=dx, start=-Lx/2, periodic=(bctype=='periodic'))
gridy = fdo.Grid(num_cells=Ny, delta=dy, start=-Ly/2, periodic=(bctype=='periodic'))
geom = fdo.GeometryCartesian((gridx, gridy), nls=(5,5), nus=(5,5)) # ample wiggle room

#%% Extra geometrical stuff
ndim = geom.ndim

x = gridx.r_n
y = gridy.r_n
xc = gridx.r_c
yc = gridy.r_c
xe = xc[-half:end+half].flatten()
ye = yc[-half:end+half].flatten()

ex = fdo.ExtendedArray(geom.ncells, stags=(1,0), nls=(1,1), nus=(1,1))
ex.arr = np.ones(ex.nts)
ey = fdo.ExtendedArray(geom.ncells, stags=(0,1), nls=(1,1), nus=(1,1))
ey.arr = np.ones(ey.nts)

f_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(1,1), nus=(1,1))
#f_indep = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(0,0), nus=(0,0))
f_indep.arr = np.ones(f_indep.nts)

#%% Tests
if False:
    u = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(2,1), nus=(1,1))
    u.arr = np.random.rand(*u.nts)
    w0 = (0.1, 0.1)
    w = tuple(f_indep*x for x in w0)
    w1 = tuple(-f_indep*x for x in w0)
    w2 = w

#%% Setup the interpolating scheme stencil polynomial
CIR_fem = AS.make_fem_scheme_sp(geom, f_indep, 0, [(0,0)])
stencil = AS.odd_order_stencil(2,1) #[(0,0), (1,0), (0,1)]
degree = 1
CIR = AS.make_interpolation_scheme_sp(geom, f_indep, degree, stencil)
LW_fem = AS.make_fem_scheme_sp(geom, f_indep, degree, stencil)

LWtail = AS.make_interpolation_scheme_sp(geom, f_indep, 2, AS.even_order_stencil(ndim,1))
ORD3_fem = AS.make_fem_scheme_sp(geom, f_indep, 2, AS.even_order_stencil(ndim,1))
ORD5_fem = AS.make_fem_scheme_sp(geom, f_indep, 4, AS.even_order_stencil(ndim,2))
ORD7_fem = AS.make_fem_scheme_sp(geom, f_indep, 6, AS.even_order_stencil(ndim,3))
    
#%% The sanity check for the new algorithm
FEM = ORD3_fem
u = fdo.ExtendedArray(geom.ncells, stags=(0,0), nls=(2,2), nus=(2,2))
u.arr = np.random.rand(*u.nts)
for p in FEM.stencil:
    Deltar1 = []
    Deltar2 = []
    for a, gr in enumerate(geom.grids):
        shift = list(p)
        shift[a] = p[a]-half
        d1 = gr.r_c.shifted(shift)-gr.r_n
        Deltar1.append(d1)
        shift[a] = p[a]+half
        d2 = gr.r_c.shifted(shift)-gr.r_n
        Deltar2.append(d2)
    # A more thorough sanity chack #2 -- integrate the neighbor cell
    D1 = tuple(d.broadcast(f_indep) for d in Deltar1)
    D2 = tuple(d.broadcast(f_indep) for d in Deltar2)
    unext = FEM.integrate(u, D1, D2)
    unext = unext/FEM.dvolume.shifted(p)
    print('Point =', p, ', method error =', np.max(np.abs(unext-u.shifted(p))))

#%% UTOPIA (3rd-order) schemes
UTr = AS.interpolation_scheme_sp(geom, f_indep, 3, AS.odd_right_stencil(ndim,2))
UTl = AS.interpolation_scheme_sp(geom, f_indep, 3, AS.odd_left_stencil(ndim,2))
ORD5 = AS.interpolation_scheme_sp(geom, f_indep, 5, AS.odd_left_stencil(ndim,3))

#%%
INTERP_SCHEMES = {'CIR':CIR, 'LWtail':LWtail, 'UTr':UTr, 'UTl':UTl, 'ORD5':ORD5}
FEM_SCHEMES = {'CIR_fem':CIR_fem, 'LW_fem':LW_fem, 'ORD3_fem':ORD3_fem, 'ORD5_fem':ORD5_fem,
               'ORD7_fem':ORD7_fem}

def advection_step(alg, u, r, w):
    if alg in INTERP_SCHEMES.keys():
        return INTERP_SCHEMES[alg].interpolate(u, r)-u
    elif alg in FEM_SCHEMES.keys():
        return FEM_SCHEMES[alg].fem_advection_step(u, w)
    else:
        raise Exception('Unknown alg: '+alg)

#%% The initial value and velocity
if shapetype=='rectangle':
    fini = fdo.ExtendedArray(geom.ncells, stags=(0, 0), nls=(0, 0), nus=(0, 0))
    fini.arr = np.zeros(fini.nts)
    mask = (np.abs(x)<Lx/4) & (np.abs(y-Ly/6)<Ly/6)
    contour = np.array( [[Lx/4, Lx/4, -Lx/4, -Lx/4, Lx/4], [0, Ly/3, Ly/3, 0, 0]])
    fini[mask] = 1
    def smear(f,n):
        for k in range(n):
            f.setv = fdo.n_mean(fdo.c_mean(fdo.n_mean(fdo.c_mean(f,0),0),1),1)
        return f
    if nsmear>0:
        fini = smear(fini, nsmear)
    # Seed some instabilities
    noise = 0
    fini += noise*np.random.randn(*fini.nts)
elif shapetype=='full':
    fini = fdo.ExtendedArray(geom.ncells, stags=(0, 0), nls=(1, 1), nus=(1, 1))
    fini.arr = np.ones(fini.nts)

def get_total(f):
    return geom.integrate(f, [[-Lx/2, Lx/2], [-Ly/2, Ly/2]]).item()
thetotal = neat_float(get_total(fini))

#%% Velocity
drmin = min(np.min(dx),np.min(dy))
if speedtype=='uniform':
    # c = V, so this may be used to determine the stability region, which is sum(V)<1 for odd-order methods
    # or all(abs(V)<1) for UTOPIA
    dt = alpha*drmin/np.sum(np.abs(V))
    c0 = V*dt
    # f_indep is set to ones
    r = tuple(-f_indep*x for x in c0)
    vx = V[0]*ex
    vy = V[1]*ey
    w = (-vx*dt, -vy*dt)
elif speedtype=='rotation' or speedtype=='div-less':
    vangle = 1 # angular velocity
    Rmax = np.sqrt((Lx/2)**2 + (Ly/2)**2)
    vmax = vangle*Rmax # maximum linear velocity
    dt = alpha*drmin/vmax
    # Zero-divergence velocity, curl of psi*z_unit
    psi = fdo.ExtendedArray(geom.ncells, stags=(1,1), nls=(5,5), nus=(5,5))
    psi.arr = np.full(psi.nts, fill_value=np.nan)
    rc = np.sqrt(xc**2+yc**2)
    if speedtype=='rotation':
        psi.setv = -rc**2/2*vangle # at points (i-1/2,j-1/2)
    elif speedtype=='div-less':
        psi.setv = (vmax*Rmax/np.pi)*np.cos(np.pi*xc/Lx)*np.cos(np.pi*yc/Ly)
    vx =  geom.grad(psi, 1) # vx at points (i-1/2,j)
    vy = -geom.grad(psi, 0) # vy at points (i,j-1/2)
    vxc = fdo.n_mean(vx, 0)
    vyc = fdo.n_mean(vy, 1)
    vxn = LWtail.interpolate(vxc,
        (-vxc.view[-1:end+1,-1:end+1]*dt, -vyc.view[-1:end+1,-1:end+1]*dt))
    vyn = LWtail.interpolate(vyc,
        (-vxc.view[-1:end+1,-1:end+1]*dt, -vyc.view[-1:end+1,-1:end+1]*dt))
    vxndt = (vxn + vxc)*dt/2
    vyndt = (vyn + vyc)*dt/2
    r = (-vxndt, -vyndt) # Where the previous point is
    w = (-vx.view[-half:end+half,-1:end+1]*dt,
         -vy.view[-1:end+1,-half:end+half:]*dt)
    # Check the divergence
    divv = geom.divergence([w[0], w[1]])
    print('Divergence of velocity v =', np.max(np.abs(divv)))

#%% Initial values
nmethods = len(methods)
farr=[]
for kmethod, alg in enumerate(methods):
    d = 5
    f = fdo.Field('f_'+alg, geom.ncells, stags=(0,0), nls=(d,d), nus=(d,d))
    if bctype=='periodic':
        # The periodic boundary conditions may set f.bc[0]=f.bc[end], too -- 
        # it is independent of direciton
        #f.bc[-d:end+d, end:end+d] = f.bc[-d:end+d, 0:d]
        #f.bc[-d:end+d, -d:-1]     = f.bc[-d:end+d, end-d:end-1]
        f.bc[span,    end:end+d]  = f.bc[span,            0:d]
        f.bc[span,        -d:-1]  = f.bc[span,    end-d:end-1]
        f.bc[end:end+d, 0:end-1]  = f.bc[0:d,         0:end-1]
        f.bc[-d:-1,     0:end-1]  = f.bc[end-d:end-1, 0:end-1]
    elif bctype=='zero':
        f.bc[span, end:end+d] = 0
        f.bc[span, -d:0] = 0
        f.bc[end:end+d, 1:end-1] = 0
        f.bc[-d:0, 1:end-1] = 0
    else:
        raise Exception('unknown bctype')
    f.bc.freeze()
    f.setv = 0 # to clear the ghost cells
    f.setv = fini
    f.bc.apply()
    farr.append(f)

plt.close('all')
figs = []
if shapetype=='full':
    zmin = 1-dzlim
    zmax = 1+dzlim
else:
    zmin = -.5
    zmax = 1.5
for kmethod in range(nmethods):
    if savefig:
        savedir = 'fig/'+methods[kmethod]+'_'+speedtype
        makedir(savedir)
    else:
        savedir = None
    fig = UpdateablePcolor(100+kmethod, zmin=zmin, zmax=zmax, savedir=savedir, savefmts=['png'])
    figs.append(fig)

#%% Let's go!
Nt = 10000
t = 0
lines = [None]*nmethods
def rotmat(th):
    s = np.sin(th)
    c = np.cos(th)
    return np.array([[c, -s], [s, c]])
for kt in range(Nt+1):
    for kmethod, alg in enumerate(methods):
        f = farr[kmethod]
        df = advection_step(alg, f, r, w)
        f += df
        assert not np.isnan(f.arr).any()
        f.bc.apply()
        assert not np.isnan(f.arr).any()
    t += dt
    if kt<100 or (kt<500 and kt % 10==0) or kt % 100==0:
        totals = [neat_float(get_total(f) - thetotal) for f in farr]
        print('kt =',kt, ', init total =', thetotal, ', delta totals =', totals)
        for kmethod in range(nmethods):
            fig = figs[kmethod]
            f = farr[kmethod]
            alg = methods[kmethod]
            if fig.started_plotting and not plt.fignum_exists(fig.fignum):
                print('Figure closed, exiting ...')
                plt.close('all')
                sys.exit()
            the_title = alg + ' in [{:g}, {:g}], '.format(np.min(f), np.max(f))
            if speedtype=='uniform':
                the_title += 'D = [{:g}, {:g}]'.format(t*V[0], t*V[1])
            elif speedtype=='rotation':
                the_title += 'nrot = {:g}'.format(t*vangle/(2*np.pi))
            else:
                the_title += 'kt = {:d}'.format(kt)
            fig.plot(geom, f, title=the_title, save=False)
            if shapetype=='rectangle' and speedtype=='rotation':
                if lines[kmethod] is not None:
                    lines[kmethod].remove()
                crot = rotmat(t*vangle) @ contour
                lines[kmethod],=fig.ax.plot(crot[0], crot[1],'w',linewidth=0.3)
            fig.save() # after we draw the little rectangle
            #fig.fig.canvas.draw()
        plt.pause(.1)
        #fig[0].waitforbuttonpress() # progress by clicking mouse in Figure 100
